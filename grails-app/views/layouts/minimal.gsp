<%--
  Created by IntelliJ IDEA.
  User: leonil
  Date: 2017/09/11
  Time: 15:25
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=11; IE=10; IE=9; IE=8; IE=7; IE=EDGE" />
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <title>
        <g:layoutTitle default="Signio" />
    </title>
    <g:layoutHead/>
    <r:layoutResources />

</head>

<body>
<g:layoutBody />
<r:require module="signioGoogleAnalytics"/>
<r:layoutResources />
</body>
</html>
