%{--<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>--}%
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="minimal"/>
	<title>Signio - Vehicle Inbox</title>
	%{-- <r:layoutResources/> --}%
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
	%{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>--}%
	%{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>--}%
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'pagination.css')}">
</head>
<body>

<script type="text/javascript">
	if (typeof signio === 'undefined') {
		var signio = {}
	}

	signio.vehicleInbox = {
		callAjax : function(url, type, data, success){
			$.ajax({
				url: url,
				contentType: 'application/javascript; charset=utf-8',
				async : false,
				type : type,
				data: data,
				error: function(data){
				},
				success: success
			});
		}
	};

</script>

<table width="98%"  class="responsive BGColourDarkBlue">
	<tr><td colspan="100%"  class="BGColourWhite" > <g:img dir="images" file="signio_logo.png"/></td></tr>
	<tr><td colspan="100%" class="BGColoursHeading" ><h4>VEHICLE INBOX</h4></td></tr>

	%{--grails-app/assets/images/signio_logo.png--}%
	<g:render template="/vehicleInbox/view_controls" />
	<g:render template="/vehicleInbox/inbox_table"/>
</table>

<br><br>
</body>
</html>
