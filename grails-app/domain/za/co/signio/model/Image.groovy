package za.co.signio.model

class Image {

    Byte[] image

    static hasMany = [supportingdocsImages: SupportingdocsImage, supportingdocpageImage: SupportingdocpageImage]

    static constraints = {
        image(nullable: false)
    }
    static mapping = {
        version(false)
    }
}
