package za.co.signio.model

class CrmDocumenttype {

    String type

    static hasMany = [crmDocument:CrmDocument]

    static constraints = {
        type(nullable: false, maxSize: 255)
    }

    static mapping = {
        version false
    }
}
