package za.co.signio.model

class Auditlog {

    String description;
    Date dateCreated;
    Long memberId, affectedMemberId, affectedInstitutionId

    static belongsTo = [Application, Auditlogaction]

    static hasOne = [
            application: Application,
            auditlogaction: Auditlogaction,
    ]

    static hasMany = [auditlogspecificfields: Auditlogspecificfield]

    static mapping = {
        version false
    }

    static constraints = {
        description maxSize: 255, nullable: false;
        memberId nullable: false;
        affectedInstitutionId nullable: true;
        affectedMemberId nullable: true;
        dateCreated nullable: false;
    }

    String toString() {
        return description?.substring(0, 15);
    }
}
