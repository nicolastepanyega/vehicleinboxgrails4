package za.co.signio.model

class Relationshipmoduleconfig {

    Date dateCreated
    Date dateUpdated

    static hasOne = [relationship:Relationship,member:Member,module:Module]
    static hasMany = [relationshipmoduleconfigModuleactions:RelationshipmoduleconfigModuleaction,relationshipmoduleconfigModuletemplates:RelationshipmoduleconfigModuletemplate]
    static belongsTo = [Relationship,Member,Module]

    static mapping = {
        version false
    }

    static constraints = {
        dateCreated nullable: false
        dateUpdated nullable: false
    }
}
