package za.co.signio.model

import za.co.signio.model.Certificatetype

class Certificate {

    String serialNumber
    String passphrase
    Date startDate
    Date endDate
    Date dateCreated
    Boolean expired

    static hasOne = [certificatetype: Certificatetype,
                     member: Member]

    static belongsTo = [Certificatetype, Member]
    static auditable = true

    static mapping = {
        version false
    }

    static constraints = {
        serialNumber maxSize: 45, nullable: false
        startDate nullable: false
        endDate nullable: false
        dateCreated nullable: false
        passphrase(maxSize: 7, nullable: false)
        expired(nullable: false)
    }

    String toString() {
        return "${serialNumber} - ${member.toString()}"
    }
}
