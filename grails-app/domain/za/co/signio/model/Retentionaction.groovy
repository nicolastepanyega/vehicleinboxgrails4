package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 5/17/13
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
class Retentionaction {
    
    Integer lostToInstitution
    Double lostRandValue
    Date dateUpdated
    Date followupDate
    String comment
    String bdoName
    Boolean resultInApplication
    BigInteger applicationReferenceNumber
    String newAccountNumber
    Retention retention
    Reason reason
    Dealstate dealstate

    static belongsTo = [Retention, Reason]

    static constraints = {
        lostToInstitution nullable: true
        lostRandValue nullable: true
        dateUpdated( nullable: true)
        followupDate(nullable: true)
        comment(nullable: true)
        bdoName nullable: true, maxSize: 45
        resultInApplication nullable: true
        applicationReferenceNumber(nullable: true)
        newAccountNumber nullable: true, maxSize: 30
        retention(nullable: false)
        reason(nullable: true)
        dealstate nullable: false

    }

    static mapping = {
        version(false)
    }
}
