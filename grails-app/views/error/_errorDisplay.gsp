<tr class="BGColourWhite">
    <td colspan="4">
        <span style="font-weight: bold;">Error Details</span>
    </td>
</tr>
<tr>
    <td colspan="4">
        <span style="font-weight: bold;">
            Member id: "${session["memberID"]}"<br>
            Institution id: "${session["institutionID"]}"<br>
            <g:if test="${fullMessage != null && fullMessage}">
                ------------<br>
                Message: "${fullMessage}"<br>
            </g:if>
            <g:if test="${cause != null && cause != ''}">
                ------------<br>
                Probable Cause: "${cause}"
            </g:if>
        </span>
    </td>
</tr>