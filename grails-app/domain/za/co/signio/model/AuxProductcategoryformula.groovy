package za.co.signio.model

class AuxProductcategoryformula {

    String formula
    String label

    static hasOne = [auxProductcategory:AuxProductcategory, auxProductcategoryformulatype: AuxProductcategoryformulatype]

    static belongsTo = [AuxProductcategory, AuxProductcategoryformulatype]
    static hasMany = [auxCalculateFormulas:AuxCalculateformula, totalPremiumProductcategoryformulas: AuxCalculateformula, auxProductoptioninstitutionfspconfigs:AuxProductoptioninstitutionfspconfig]



    static constraints = {
        auxProductcategory(nullable: false)
        formula(nullable: false)
        label(nullable: false, maxSize: 255)
        auxProductcategoryformulatype(nullable: false)
    }

    static mappedBy = [
            auxCalculateFormulas: 'auxProductcategoryformula',
            totalPremiumProductcategoryformulas: 'totalPremiumProductcategoryformula']


    static mapping = {
        version(false)

    }


}
