package za.co.signio.model

class Message {

    String title;
    Integer duration;
    Date dateScheduled;
    Date dateCreated;
    Date dateEnded;
    Boolean isSendable;

    static belongsTo = [Messagetype, Messagerecipient, Member]

    static hasOne = [consultant: Member, createdByMember: Member, endedByMember: Member, messagetype: Messagetype, messagerecipient: Messagerecipient]

    static hasMany = [messagecontents: Messagecontent, messageRelationships: MessageRelationship]

    static mapping = {
        version false;
    }

    static constraints = {
        title maxSize: 255, nullable: false;
        duration nullable: true;
        dateScheduled nullable: true;
        dateCreated nullable: false;
        dateEnded nullable: true;
        isSendable nullable: false;
        consultant nullable: true;
        endedByMember nullable: true;
    }

    String toString() {
        return title;
    }
}
