package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/16/12
 * Time: 3:58 PM
 * To change this template use File | Settings | File Templates.
 */
class Documenttemplatetype {
    
    String type

    static hasMany = [documentTemplateGroups:Documenttemplategroup]

    static constraints = {
        type(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
