package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 5/3/12
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
class Documenttemplategroup {
    
    String name
    Documenttemplatetype documenttemplatetype
    String displayName

    static belongsTo = [Documenttemplatetype, Institution, AuxProductoption]
    static hasMany = [documentTemplates: Documenttemplate, documentTemplateScripts: Documenttemplatescript, dealDocuments: Dealdocument,
                      institutions:Institution, auxProductOptions:AuxProductoption, documenttemplategroupInstitution: DocumenttemplategroupInstitution]

    static constraints = {
        name(nullable: false)
        documenttemplatetype nullable: false
    }

    static mapping = {
        version false
        documentTemplateScripts joinTable: 'documenttemplategroup_documenttemplatescript'
        documentTemplates joinTable: 'documenttemplategroup_documenttemplate'
        institutions joinTable: 'documenttemplategroup_institution'
        auxProductOptions joinTable: 'documenttemplategroup_aux_productoption'
    }
}
