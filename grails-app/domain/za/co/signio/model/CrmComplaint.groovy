package za.co.signio.model

class CrmComplaint {

    String heading
    String note

    static hasMany = [crmSharedRelationships:CrmSharedrelationship]

    static mapping = {
        version false
    }

    static constraints = {
        heading nullable: false
        note nullable: false
    }
}
