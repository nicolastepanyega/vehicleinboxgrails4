package za.co.signio.vehicleinbox.service

import com.lowagie.text.pdf.PdfCopyFields
import com.lowagie.text.pdf.PdfReader
import grails.converters.JSON
import grails.util.Holders
import groovy.util.logging.Slf4j
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder
import org.apache.commons.codec.binary.Base64

import org.springframework.web.context.request.RequestContextHolder
import za.co.signio.dataModel.SignioDataModel
import za.co.signio.model.*
import za.co.signio.vehicleinbox.helper.RegTrackHelper
import za.co.signio.webservice.*
@Slf4j
class RegtrackService {

    def thirdPartyWebserviceRequestService
    //def signioWebserviceRequestService

    def sessionFactory

    /*private static final log log = log.getlog(this);*/

    def sendLogTransactionRequest() throws Exception{
        def returnMap = [
            success:false,
            message: "RegTrack failed."
        ]

        def params = RequestContextHolder.requestAttributes.params
        final RegTrackHelper helper = new RegTrackHelper(params)

        if (!helper.isReadyForRegTrack()) {
            log.error(helper.getRejectionMessage())
            returnMap.message = helper.getRejectionMessage()
        } else {
            log.info("Log Transaction to regtrack called for reference: ${helper.regTrackDeal.batchRef}")

            WebServiceResponse wsResponse = thirdPartyWebserviceRequestService.sendRequest(
                    (Class<WebServiceProvider>) ThirdParty.REGTRACK, ThirdParty.REGTRACK.LOG_TRANSACTION,
                    helper.getRegTrackXML());
            StringBuilder fullResponseMessage = new StringBuilder()
            String message = wsResponse.getMessage()
            switch(wsResponse.getState()) {
                case WebServiceResponseState.SUCCESS :
                    def xmlResponse = new XmlSlurper().parseText(wsResponse.getResult())
                    String regTrackRefNo = xmlResponse.success.requestReference
                    fullResponseMessage.append("Log Transaction Request : Successful.\nReference No = $regTrackRefNo\n")
                    helper.setReferenceNoInDeals(regTrackRefNo)
                    helper.updateRegTrackDealState("RegTrack - Requested", "RegTrack Request Logged.\nReference: " + regTrackRefNo, false)
                    returnMap.success = true
                    break
                case WebServiceResponseState.FAILED :
                    String result = wsResponse.getResult()
                    def logTransactionResponse = new XmlSlurper().parseText(result)
                    def globalError = logTransactionResponse?.error?.globalError
                    def fieldErrors = logTransactionResponse?.error?.fieldErrors?.children()

                    String errorString = "Error: " + globalError + "\n"
                    fieldErrors.each {
                        errorString = errorString + it + "\n"
                    }

                    if ("".equals(errorString)) {
                        errorString = message
                    }
                    fullResponseMessage.append("Log Transaction Request : Failed, Message = $errorString\n")
                    helper.updateRegTrackDealState("RegTrack - Failed", errorString, false)
                    break
                case WebServiceResponseState.ERROR :
                    helper.updateRegTrackDealState("RegTrack - Failed", wsResponse.getMessage(), false)
                    fullResponseMessage.append("Log Transaction Request : Failed, Message = $message\n")
                    break
                default :
                    helper.updateRegTrackDealState("RegTrack - Failed", message, false)
                    fullResponseMessage.append("Log Transaction Request : Failed, Unknown state\n")
                    break
            }
            returnMap.message = fullResponseMessage.toString()
        }
        return returnMap as JSON
    }

    public def uploadSupportingDocs (String originID, String batchRefNo, String requestRef, boolean isJuristic,
                                     String bank, String newUsed, Institution institution)
            throws Exception {
        StringBuilder retMessage = new StringBuilder();

        def ppoList = Formfieldcode.createCriteria().list{
            formfieldvalue {
                formfield {
                    eq "name", "regtrackSupportingDocs"
                }
                if (!isJuristic) {
                    eq "value", "IDBOOK"
                }
            }
            eq "code", "PPO"
            projections {
                formfieldvalue {
                    property "value"
                }
            }
        } as List<String>

        def mibList = Formfieldcode.createCriteria().list{
            formfieldvalue {
                formfield {
                    eq "name", "regtrackSupportingDocs"
                }
                if ("New".equals(newUsed)) {
                    eq "value", "MANUFACTURER CERTIFICATE"
                } else {
                    eq "value", "COPY OF NATIS"
                }

            }
            eq "code", "MIB/RC"
            projections {
                formfieldvalue {
                    property "value"
                }
            }
        } as List<String>

        def poaList = Formfieldcode.createCriteria().list{
            formfieldvalue {
                formfield {
                    eq "name", "regtrackSupportingDocs"
                }
            }
            eq "code", "KYC"
            projections {
                formfieldvalue {
                    property "value"
                }
            }
        } as List<String>

        StringBuilder ppoContent = new StringBuilder()
        StringBuilder mibContent = new StringBuilder()
        StringBuilder poaContent = new StringBuilder()

        boolean isPPOAvailable = buildCombinedDocument(ppoList, ppoContent, originID, batchRefNo, institution)
        boolean isMIBAvailable = buildSingleDocument(mibList, mibContent, originID, batchRefNo, institution)
        boolean isDOCAvailable = buildSingleDocument(poaList, poaContent, originID, batchRefNo, institution)

        if (isPPOAvailable && isMIBAvailable && isDOCAvailable) {
            // Upload ppo
            uploadSupportingDocsToRegTrack(requestRef, "PPO", "application/pdf", "OwnerProxyPack.pdf", ppoContent.toString())
            // Upload MIB
            String docName = "New".equals(newUsed)? "ManufacturerCertificate.pdf" : "CopyOfNatis.pdf"

            uploadSupportingDocsToRegTrack(requestRef, "MIB/RC", "application/pdf", docName, mibContent.toString())
            // Upload Proof of address
            uploadSupportingDocsToRegTrack(requestRef, "KYC", "application/pdf", "ProofOfAddress.pdf", poaContent.toString())

        } else {
            String message = "The following document(s) is/are not uploaded for the client:"
            if(!isPPOAvailable) {message = message + "\n - Owner Proxy Pack (ID Document and/or Business Register Number Certificate)."}
            if(!isMIBAvailable) {
                message = message + ("New".equals(newUsed)? "\n - Manufacturer's Certificate." : "\n - Copy Of Natis.")
            }
            if(!isDOCAvailable) {message = message + "\n - Proof of Residence."}

            Deal regTrackDeal = getDealOnRequestRefAndState(requestRef, Dealstate.findByState("RegTrack - Requested"))
            message = "RegTrack Request Logged. Reference No: " + requestRef + ".\n" + message
            if (!message.equals(regTrackDeal.getComment())) {
                updateDealState(regTrackDeal, null , message, false)
            }

            return message
        }
        RegTrackHelper.Bank requestedBank = RegTrackHelper.Bank.getBankByName(bank)
        log.info("Transferring Request to bank: ${bank}, code: ${requestedBank.getBankCode()}")
        retMessage.append("Transfer Request to bank: ${bank}, code: ${requestedBank.getBankCode()}.")
        transferRequestToBank(requestedBank.getBankCode(), requestRef, retMessage)
        return retMessage.toString()
    }

    def transferRequestToBank(String code, String requestRef, StringBuilder retMessage) throws Exception{

        StringWriter writer = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(writer)

        builder.transferRequestToBank() {
            bankCode(code)
            requestReference(requestRef)
        }
        WebServiceResponse wsResponse = thirdPartyWebserviceRequestService
                .sendRequest((Class<WebServiceProvider>)ThirdParty.REGTRACK, ThirdParty.REGTRACK.TRANSFER_REQUEST_TO_BANK, writer.toString());
        String result = wsResponse.getResult()
        def transferRequestToBankResponse = new XmlSlurper().parseText(result)

        String success = transferRequestToBankResponse.success?.text()

        if (success == null || "".equals(success)) {
            retMessage.append("\nTransfer Request to Bank Failed!\n")
            retMessage.append("Message: " + transferRequestToBankResponse.error?.text())
        } else {
            retMessage.append("SUCCESS")
        }
        return retMessage.toString()
    }

    private boolean buildCombinedDocument(List<String> docList, StringBuilder document,String originID, String batchRefNo,
                                          Institution institution) {
        StringWriter writer = new StringWriter()
        MarkupBuilder markupBuilder = new MarkupBuilder(writer)

        markupBuilder.supportingDocs(){
            origId(originID)
            batchRef(batchRefNo)
            docs(){
                docList.each {doc(name: "$it", available: "")}
            }
            error("false")
        }

        // LP-339
        // Fix warning message about retrieving V2 supporting docs in VehicleInbox
        // Check for documents on V2 side...
/*
        WebServiceResponse supportingDocsResponse = signioWebserviceRequestService
                .sendRequest((Class<WebServiceProvider>) Signio.SUPPORTING_DOCS, Signio.SUPPORTING_DOCS.CREATEBATCH, writer.toString());

        log.info("SIGNIO WEBSERVICE RESPONSE = " + supportingDocsResponse.getMessage())

        if (supportingDocsResponse.getState() == WebServiceResponseState.ERROR
                || supportingDocsResponse.getState() == WebServiceResponseState.FAILED) {
            String errorMessage = supportingDocsResponse.getMessage()
            log.error("Failed to retrieve supporting docs from v2 for regTrack!\n Message = " + errorMessage + "\nChecking V5...")
            // Error occurred check V5
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution, true)
        }
*/
        // Check for documents on V5 side...
        log.info("Checking V5...")
        return retrieveDocumentsFromV5(docList, document, batchRefNo, institution, true)

        def supportingDocs = new XmlSlurper().parseText(supportingDocsResponse.getResult())
        def notAvailable = supportingDocs.depthFirst().findAll { it.name() == 'doc' && it.@available == 'false'}
        def available = supportingDocs.depthFirst().findAll { it.name() == 'doc' && it.@available == 'true'}
        StringBuilder retMessage = new StringBuilder()
        if (notAvailable != null && !notAvailable.isEmpty()) {
            retMessage.append("The following document(s) is/are not available:\n")
            notAvailable.each {
                retMessage.append("- ").append(it.@name).append("\n")
            }
            log.warn("Not all documents are available for regTrack on V2!\n Message = " + retMessage.toString() + "\nChecking V5...")
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution, true)
        } else if (available != null && !available.isEmpty() && (notAvailable == null || notAvailable.isEmpty())) {
            List<String> docContent = new ArrayList<String>();
            for (def doc : available) {
                docContent.add(doc.text())
            }
            document.append(mergePDF(docContent))
            return true
        } else {
            log.error("Failed to retrieve supporting docs for regTrack!\n Message = Unknown Error.\nChecking V5...")
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution, true)
        }
    }

    private boolean buildSingleDocument(List<String> docList, StringBuilder document,String originID, String batchRefNo,
                                        Institution institution) {
        StringWriter writer = new StringWriter()
        MarkupBuilder markupBuilder = new MarkupBuilder(writer)

        markupBuilder.supportingDocs(){
            origId(originID)
            batchRef(batchRefNo)
            docs(){
                docList.each {doc(name: "$it", available: "")}
            }
            error("false")
        }

        // LP-339
        // Fix warning message about retrieving V2 supporting docs in VehicleInbox
        // Check for documents on V2 side...
/*
        WebServiceResponse supportingDocsResponse = signioWebserviceRequestService
                .sendRequest((Class<WebServiceProvider>)Signio.SUPPORTING_DOCS, Signio.SUPPORTING_DOCS.CREATEBATCH, writer.toString());

        if (supportingDocsResponse.getState() == WebServiceResponseState.ERROR
                || supportingDocsResponse.getState() == WebServiceResponseState.FAILED) {
            String errorMessage = supportingDocsResponse.getMessage()
            log.error("Failed to retrieve supporting docs from V2 for regTrack!\n Message = " + errorMessage + "\nChecking V5...")
            // Check V5
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution);
        }

*/
        // Check for documents on V5 side...
        log.info("Checking V5...")
        return retrieveDocumentsFromV5(docList, document, batchRefNo, institution);

        def supportingDocs = new XmlSlurper().parseText(supportingDocsResponse.getResult())
        def notAvailable = supportingDocs.depthFirst().findAll { it.name() == 'doc' && it.@available == 'false'}
        def available = supportingDocs.depthFirst().findAll { it.name() == 'doc' && it.@available == 'true'}
        StringBuilder retMessage = new StringBuilder()
        if (notAvailable != null && !notAvailable.isEmpty()) {
            retMessage.append("The following document(s) is/are not available in V2:\n")
            notAvailable.each {
                retMessage.append("- ").append(it.@name).append("\n")
            }
            log.warn("Not all documents are available on V2 !\n Message = " + retMessage.toString() + "\nChecking V5...")
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution);
        } else if (available != null && !available.isEmpty() && (notAvailable == null || notAvailable.isEmpty())) {
            for (def doc : available) {
                document.append(doc.text())
            }
            return true
        } else {
            log.error("Failed to retrieve supporting docs for regTrack on V2!\n Message = Unknown Error.\nChecking V5...")
            return retrieveDocumentsFromV5(docList, document, batchRefNo, institution);
        }
    }

    private boolean retrieveDocumentsFromV5(List<String> docList, StringBuilder document, String batchRefNo, Institution institution, boolean mergeDocuments = false) {
        log.info("Checking V5 for supporting docs on reference: ${batchRefNo}.")
        Deal deal = (Deal)Deal.findByBatchRefAndDealtypeInListAndInstitution(batchRefNo, [Dealtype.findByType("VAF"), Dealtype.findByType("JuristicVAF")], institution);
        if (deal == null) {
            log.warn("VAF Deal not found for batchRef: ${batchRefNo}")
            return false;
        }

        List<String> docContent = new ArrayList<String>();

        for (String docName : docList) {
            List<Dealdocument> documents;
            Dealdocumenttype type = Dealdocumenttype.findByType(docName.replaceAll(" ", ""))
            documents = Dealdocument.findAllByDealAndDealdocumenttype(deal, type);

            if (documents == null || documents.isEmpty()) {
                log.info("Dealdocument not found for deal_id: ${deal.id} and type: ${type.type}")
                return false;
            } else {
                try {
                    if (!mergeDocuments) {
                        document.append(getDocumentBase64(documents.get(0)))
                    } else {
                        docContent.add(getDocumentBase64(documents.get(0)));
                    }
                } catch (all) {
                    log.error("An error occurred while retreiving the file from the repo: ${all.getMessage()}!")
                    return false;
                }
            }
        }

        if (mergeDocuments) {document.append(mergePDF(docContent))}
        return true
    }

    private String getDocumentBase64(Dealdocument dealDocument) throws Exception {

        // Hack build in to cater for SA-Taxi which saves a preceding / in the uri, (╯°□°）╯︵ ┻━┻
        String uri = dealDocument.uri

        if (uri.startsWith("/")) {
            uri = uri.substring(1);
        }

        String path = "${Holders.config.documentRepo.v5Store}/${uri}/${dealDocument.filename}"
        File file = new File(path);

        if (!file.exists()) {
            path = "/opt/repo/v5store/${uri}/${dealDocument.filename}"
        }

        file = new File(path);

        if (file.exists()) {
            return Base64.encodeBase64String(file.readBytes());
        } else {
            throw new RuntimeException("File not found on path: ${path}")
        }
    }

    String statusUpdates() {
        String result = null
        try {
            log.info("Fetch RegTrack Updates")
            WebServiceResponse response = thirdPartyWebserviceRequestService
                    .sendRequest((Class<WebServiceProvider>)ThirdParty.REGTRACK, ThirdParty.REGTRACK.FETCH_STATUS_UPDATES, "<xml></xml>")
            log.info("Message : " + response.getMessage())
            result = response.getResult()
        } catch (Exception ex) {
            log.error("Failed to get RegTrack updates.", ex)
        }
        return result;
    }



    def fetchALVForm(String requestRef) throws Exception{
        StringWriter writer = new StringWriter()
        MarkupBuilder  builder = new MarkupBuilder(writer)

        builder.fetchALVForm() {
            requestReference(requestRef)
        }
        WebServiceResponse response = thirdPartyWebserviceRequestService
                .sendRequest((Class<WebServiceProvider>)ThirdParty.REGTRACK, ThirdParty.REGTRACK.FETCH_ALV_FORM, writer.toString())

        if (WebServiceResponseState.FAILED == response.getState() || WebServiceResponseState.ERROR == response.getState()) {
            log.error("Failed fetching ALV form!\nMessage = " + response.getMessage())
            log.error("Result = " + response.getResult())
            return null
        }

        def fetchALVFormResponse = new XmlSlurper().parseText(response.getResult())
        String documentContent = fetchALVFormResponse?.success?.alvForm?.documentContent
        return documentContent
    }


    private boolean uploadSupportingDocsToRegTrack(String refNo, String typeCode, String mime, String fileName, String content)
            throws Exception{

        StringWriter writer = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(writer)

        builder.uploadDocument {
            requestReference(refNo)
            documentTypeCode(typeCode)
            mimeType(mime)
            originalFileName(fileName)
            documentContent(content)
        }

        WebServiceResponse response = thirdPartyWebserviceRequestService
                .sendRequest((Class<WebServiceProvider>)ThirdParty.REGTRACK, ThirdParty.REGTRACK.UPLOAD_DOCUMENT, writer.toString());
        log.info("RESULT" + response.getResult())
        log.info("MESSAGE = " + response.getMessage())

        if ("SUCCESS".equals(response.getState().toString())) {
            return true
        }
        return false
    }

	private String mergePDF(List<String> files){
		ByteArrayOutputStream pdfData = new ByteArrayOutputStream();
		PdfCopyFields currentDocument = new PdfCopyFields(pdfData);
		currentDocument.open();
		for(String file : files){
			// convert file to binary from base64, read it in as a PDF and then add it to the document.
			currentDocument.addDocument(new PdfReader(Base64.decodeBase64(file.getBytes())));
		}
		currentDocument.close();
		return new String(Base64.encodeBase64(pdfData.toByteArray()));
	}

    public void uploadSupportingDocs() {
        log.info("Upload Supporting Docs Called...")
        List<Deal> deals = getDeals()
        deals.each {
            String br = it.batchRef
            SignioDataModel sdm = (SignioDataModel) it.getSignioDataModel("originalXml");
            String regTrackRef = sdm.fields.regTrackReferenceNo?.value
            String originatorID = sdm.fields.originatorID?.value
            String natureOfOwnership = sdm.fields.regtrackNatureOfOwnership?.value
            String bank = sdm.fields.bank?.value
            String newUsed = sdm.fields.newOrUsed?.value
            boolean isJuristic = false

            if (!isNull(sdm.fields.companyName?.value)) {
                isJuristic = true
            }

            if (!isNull(br) && !isNull(regTrackRef) && !isNull(bank)
                    && !isNull(originatorID) && !isNull(natureOfOwnership) && !isNull(newUsed)) {
                def session = sessionFactory.openSession()
                def transaction = session.beginTransaction()
                try {
                    String message = uploadSupportingDocs(originatorID, br, regTrackRef, isJuristic, bank, newUsed, it.institution)

                    log.info("MESSAGE = " + message)
                    if (message.contains("SUCCESS")) {
                        it.setIsResendable(false)
                        it.setComment("Request Reference: "+ regTrackRef +".\nRequest Transferred to Bank.")
                        it.save(flush: true, failOnError: true)
                        //RegTrackHelper.insertDealData(it)
                    } else if (message.contains("Transfer Request to Bank Failed")){
                        updateDealState(it ,"RegTrack - Failed" , "Request Reference: "+ regTrackRef +".\n${message}", false)
                    }
                    transaction.commit()
                } catch (e) {
                    log.error("Upload supporting docs failed: ${e.message}, batchRef: ${br}, regtrack reference: ${regTrackRef}")
                    transaction.rollback()
                    session.clear()
                    updateDealState(it ,"RegTrack - Failed" , "Request Reference: "+ regTrackRef +".\n${e.message}", false)
                } finally {
                    session.close()
                }
            }
        }
    }

    private List<Deal> getDeals() {
        Dealstate state = Dealstate.findByState("RegTrack - Requested");
        List<Deal> dealList;
        dealList = Deal.findAll {
            eq "dealstate", state
            eq "isResendable", true
            // TODO set date between
        }
        return dealList
    }

    private boolean isNull(String theString) {
        if (theString == null) {
            return true
        }

        if ("".equals(theString.trim()) || "null".equals(theString.trim())) {
            return true
        }

        return false
    }

    public void requestStatusUpdate() {
        log.info("**** fetchStatusUpdates *****")
        String updateXML = statusUpdates()
//        println updateXML
        log.info("**** After Webservice call *****")
        if (!isNull(updateXML)) {
            def fetchStatusUpdates = new XmlSlurper().parseText(updateXML.replaceAll("&", "and"))
            def statusUpdates = fetchStatusUpdates?.statusUpdates?.children()
            if (statusUpdates != null && !statusUpdates.isEmpty()) {
                statusUpdates.each{
                    String regTrackRequestRef = "Unknown"
                    try {
                        String uniqueID = it.uniqueID?.text()
                        regTrackRequestRef = it.requestReference?.text()
                        String alvFormAvailable = it.alvFormAvailable?.text()
                        String updateType = it.updateType?.text()
                        String updateComment = it.comment?.text()
                        String currentStatus = it.currentStatus?.text()
                        String originatorReference = it.originatorReference?.text()
                        Deal deal = Deal.findByBatchRefAndDealtype(originatorReference,Dealtype.findByType("RegTrack"))

                        if (deal==null) {
                            deal =  getDealOnRequestRefAndType(regTrackRequestRef, Dealtype.findByType("RegTrack"))
                        }

                        boolean updated;

                        // New Code created with Neil, Elias and Chris to cater for all states and removing all status updates where needed.
                        if(deal != null) {
                            Dealstate ds = deal.getDealstate()
                            if("Rejected".equalsIgnoreCase(updateType)){
                                ds = (deal.getDealstate().getState().equalsIgnoreCase("RegTrack - Requested"))?Dealstate.findByState("RegTrack - Failed"):deal.getDealstate()
                                String comment = buildComment(ds, currentStatus,updateComment,regTrackRequestRef)
                                updated = updateCommentStateALVInDeal(deal,ds,comment)
                            }
                            else if(Boolean.valueOf(alvFormAvailable)){
                                if(deal.getDealstate().getState().equalsIgnoreCase("RegTrack - Requested") || deal.getDealstate().getState().equalsIgnoreCase("RegTrack - Failed")) {
                                    ds = Dealstate.findByState("RegTrack - Received")
                                    deal = setDocumentInDeal(regTrackRequestRef,deal)
                                    String comment = buildComment(ds, currentStatus,updateComment,regTrackRequestRef)
                                    updated = updateCommentStateALVInDeal(deal,ds,comment)
                                }else{
                                    String comment = buildComment(ds, currentStatus,updateComment,regTrackRequestRef)
                                    updated = updateCommentStateALVInDeal(deal,ds,comment)
                                }
                            }
                            else{
                                String comment = buildComment(ds, currentStatus,updateComment,regTrackRequestRef)
                                updated = updateCommentStateALVInDeal(deal,ds,comment)
                            }
                            if(updated)
                               acknowledgeStatusUpdates(uniqueID)
                        }else{
                            log.error("Deal Does not Exsist on batchref: ${originatorReference} for reference: ${regTrackRequestRef}")
                            acknowledgeStatusUpdates(uniqueID)
                        }
                    }catch (e) {
                        log.info("Status update failed for Ref: ${regTrackRequestRef}, Reason: ${e.getMessage()} \nStackTrace to follow:\n", e)
                    }
                }
            }
        }
    }

    private String buildComment(Dealstate state, String status,String comment,String regTrackRequestRef){

        String newStatus = "${state.getState()} Request Reference: " + regTrackRequestRef + ".";
        if (!isNull(status)) {
            newStatus = newStatus + "\n\nCurrent status: " +  status + "."
        }
        if (!isNull(comment)) {
            newStatus = newStatus + "\n\nComment: " + comment + "."
        }
        return newStatus
    }

    private Deal setDocumentInDeal(String requestRef, Deal deal) {
        boolean updated;

        String document = fetchALVForm(requestRef)

        def signioPackage = new XmlSlurper().parseText(deal.getOriginalXml())
            signioPackage.dataBundle.appendNode {
                field(formfieldName:"documents", mapping:"1273", collectionType:"documents") {
                    subField("ALVForm.pdf",formFieldName:"fileNameOfDocument", mapping:"1274", originalName:"ALVForm", formSection:"")
                    subField(document, formFieldName:"documentContents", mapping:"0000", originalName:"ALVForm", formSection:"documents")
                }
            }
            def outputBuilder = new StreamingMarkupBuilder()
            String result = outputBuilder.bind{mkp.yield signioPackage}
            deal.setOriginalXml(result)

        return deal;
    }

    private boolean updateCommentStateALVInDeal(Deal deal, Dealstate ds, String comment) {
        boolean updated = false;

        try{
            if(comment != null)
                deal.setComment(comment)
            if(ds != null)
                deal.setDealstate(ds)
            deal.save(flush: true, failOnError: true)
            updated = true;
        }catch(Exception e){
            //We need to do this properly where do we report these errors or add code to handle this then.
            log.error(e.getMessage(),e)
        }
        return updated;
    }


    def acknowledgeStatusUpdates(String uID) {
        StringWriter writer = new StringWriter()
        MarkupBuilder  builder = new MarkupBuilder(writer)

        builder.acknowledgeStatusUpdate() {
            uniqueID(uID)
        }
        WebServiceResponse response = thirdPartyWebserviceRequestService.sendRequest((Class<WebServiceProvider>)ThirdParty.REGTRACK, ThirdParty.REGTRACK.ACKNOWLEDGE_STATUS_UPDATE, writer.toString())

        if (WebServiceResponseState.FAILED == response.getState() || WebServiceResponseState.ERROR == response.getState()) {
            log.error("Error while Acknowledging status update for regTrack!\nMessage = " + response.getMessage())
        }
    }

    private void updateDealState(Deal regTrackDeal, String dealStateDesc, String message, boolean append) throws Exception {
        if (dealStateDesc != null) {
            Dealstate state = Dealstate.findByState(dealStateDesc)
            regTrackDeal.setDealstate(state)
        }

        if (message != null) {
            String comment = regTrackDeal.getComment()
            if (comment == null) {comment = ""}
            if (append) {
                comment = comment + "\n\n" + message
            } else {
                comment = message
            }
            regTrackDeal.setComment(comment)
        }
        regTrackDeal.save(failOnError: true, flush: true)
    }


    private Deal getDealOnRequestRefAndState(String regTrackRequestReference, Dealstate dealstate) {

        List<Deal> dealList;
        dealList = Deal.findAllByDealstate(dealstate)
        for (Deal deal : dealList) {
            SignioDataModel sdm = (SignioDataModel) deal.getSignioDataModel("originalXml")
            String regTrackRef = sdm?.fields?.regTrackReferenceNo?.value
            if (regTrackRef != null && regTrackRequestReference.equals(regTrackRef)) {
                return deal;
            }
        }
        return null;
    }

    private Deal getDealOnRequestRefAndType(String regTrackRequestReference, Dealtype dealtype) {

        List<Deal> dealList;
        dealList = Deal.findAllByDealtype(dealtype)

        for (Deal deal : dealList) {
            log.info("Find Broken Deal: ${deal.getId()}")
            SignioDataModel sdm = (SignioDataModel) deal.getSignioDataModel("originalXml")
            String regTrackRef = sdm?.fields?.regTrackReferenceNo?.value
            if (regTrackRef != null && regTrackRequestReference.equals(regTrackRef)) {
                return deal;
            }
        }
        return null;
    }
}
