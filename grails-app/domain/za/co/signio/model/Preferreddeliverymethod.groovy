package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 2/25/13
 * Time: 3:50 PM
 * To change this template use File | Settings | File Templates.
 */
class Preferreddeliverymethod {

    String name

    static hasMany = [requests:Request]

    static constraints = {
        name(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
