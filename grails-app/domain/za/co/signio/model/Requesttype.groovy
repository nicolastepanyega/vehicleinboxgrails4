package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 2/13/13
 * Time: 9:42 AM
 * To change this template use File | Settings | File Templates.
 */
class Requesttype {

    String type

    static hasMany = [requests:Request]

    static constraints = {
        type(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
