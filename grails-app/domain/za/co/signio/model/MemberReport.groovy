package za.co.signio.model

class MemberReport implements Serializable{

    Member member;
    Report report;
    Qvlicense qvlicense;
    Frequency frequency;
    Boolean active;
    static belongsTo = [Report, Member, Qvlicense]

    static constraints = {
        member(nullable: false)
        report(nullable: false)
        qvlicense(nullable: true)
        frequency(nullable: true)
        active(nullable: false)
    }

    static mapping = {
        frequency enumType:"ordinal"
        id composite: ['member', 'report']
        version false
    }

    /**
     * D = Daily, W = Weekly, M = Monthly, Q = Quarterly, Y = Annually/Yearly
     */
    enum Frequency {
        D("D"), W("W"), M("M"), Q("Q"), Y("Y")
        final String value
        Frequency(String value) {this.value = value}
        String toString() {value}
        String getKey() {name()}
    }

}
