package za.co.signio.vehicleinbox.service

import groovy.util.ConfigObject
import groovy.util.logging.Slf4j;
import groovy.util.slurpersupport.GPathResult
import groovy.util.slurpersupport.NodeChild;
import grails.util.Holders;


import za.co.signio.webservice.WebServiceAction;
import za.co.signio.webservice.WebServiceProvider;
import za.co.signio.webservice.WebServiceResponse;
import za.co.signio.webservice.WebServiceResponseState;
import grails.plugins.rest.client.RestBuilder;
import org.springframework.web.client.ResourceAccessException;
@Slf4j
class ThirdPartyWebserviceRequestService {
	
	private ConfigObject config = Holders.config;
	/*private static final Logger LOGGER = Logger.getLogger(this);*/

	public WebServiceResponse sendRequest(Class<WebServiceProvider> provider, WebServiceAction action, String xmlContent) {
		try{
			def resp = new RestBuilder().post(config.webService.thirdparty + "/json"){

				json {
					institution = provider.getSimpleName()
					institutionAction = action.name()
					xml = xmlContent
				}
			}.xml;
			return new WebServiceResponse(WebServiceResponseState.valueOf(resp.state.toString().trim()), resp.message.toString().trim(), resp.result.toString().trim());
		}
		catch(ResourceAccessException e){
            log.error("Call to SharedWebService Failed1", e)
			// NOTE! Do not change the Message value! it is used elsewhere for error handling!
			return new WebServiceResponse(WebServiceResponseState.ERROR, "System Currently Offline", null);
		} catch (Exception e) {
            log.error("Call to SharedWebService Failed1", e)
            return new WebServiceResponse(WebServiceResponseState.ERROR, "System Currently Offline", null);
        }
	}
}
