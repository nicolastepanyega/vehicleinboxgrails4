package za.co.signio.security.utils;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.apache.commons.codec.binary.Base64;

public class RijndaelEncrypt {
  private final byte[] skey;
  private final byte[] iv;

  private char encodePlusCharacter = '+';

  public RijndaelEncrypt(byte[] key, byte[] iv) {
    this.skey = key;
    this.iv = iv;
  }

  public RijndaelEncrypt(byte[] key, byte[] iv, char encodePlusCharacter) {
    this.skey = key;
    this.iv = iv;
    this.encodePlusCharacter = '|';
  }

  public String encrypt(String inputText) {
    BlockCipher rijndaelCipher = new RijndaelEngine(192);
    BlockCipher cbcCipher = new CBCBlockCipher(rijndaelCipher);
    PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(cbcCipher, new PKCS7Padding());

    KeyParameter keyParameter = new KeyParameter(skey);
    ParametersWithIV parameters = new ParametersWithIV(keyParameter, iv);
    cipher.init(true, parameters);

    byte[] inputBytes = inputText.getBytes();
    byte[] cipherText = new byte[cipher.getOutputSize(inputBytes.length)];

    int processLength = cipher.processBytes(inputBytes, 0, inputBytes.length, cipherText, 0);

    try {
      cipher.doFinal(cipherText, processLength);
    }
    catch (CryptoException e) {
      e.printStackTrace();
      return null;
    }

    return asOutputString(cipherText);
  }

  public String decrypt(String inputText) {
    BlockCipher rijndaelCipher = new RijndaelEngine(192);
    BlockCipher cbcCipher = new CBCBlockCipher(rijndaelCipher);
    PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(cbcCipher, new PKCS7Padding());

    KeyParameter keyParameter = new KeyParameter(skey);
    ParametersWithIV parameters = new ParametersWithIV(keyParameter, iv);
    cipher.init(false, parameters);

    byte[] inputBytes = fromInputString(inputText);
    byte[] plainText = new byte[cipher.getOutputSize(inputBytes.length)];

    int processLength = cipher.processBytes(inputBytes, 0, inputBytes.length, plainText, 0);

    int resultLength;
    try {
      resultLength = cipher.doFinal(plainText, processLength);
    }
    catch (CryptoException e) {
      e.printStackTrace();
      return null;
    }

    return new String(plainText, 0, resultLength);
  }

  private String asOutputString(byte[] cipherText) {
    return new String(new Base64().encode(cipherText)).replace('+', encodePlusCharacter);
  }

  private byte[] fromInputString(String cipherText) {
    return new Base64().decode(cipherText.replace(encodePlusCharacter, '+').getBytes());
  }

  public char getEncodePlusCharacter() {
    return encodePlusCharacter;
  }

  public void setEncodePlusCharacter(char encodePlusCharacter) {
    this.encodePlusCharacter = encodePlusCharacter;
  }
}