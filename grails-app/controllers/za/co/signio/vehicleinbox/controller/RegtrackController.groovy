package za.co.signio.vehicleinbox.controller

import grails.converters.JSON
import groovy.util.logging.Slf4j
import org.apache.commons.codec.binary.Base64

import za.co.signio.dataModel.SignioDataModel
import za.co.signio.security.utils.StringEncryption
import za.co.signio.model.*
import grails.util.Environment
@Slf4j

class RegtrackController {
    def regtrackService

    def beforeInterceptor = [action : this.&setBrowserCompatibility]

    private setBrowserCompatibility(){
        response.addHeader("X-UA-Compatible", "IE=8")
    }

   /* private static final log log = log.getlog(this);*/

    def index = {
        if (params.authNr == null) {
            log.error("${new Date()}.User not authenticated:\nPARAMS: ${params}.")
            error("Vehicle Inbox - Error", "The user was not authorised to access RegTrack, please contact support.",
                    "The User was not authorised on the following parameters: \n${params}", null)
            return
        }

        // get the authentication
        Singlesignon singleSignOn = Singlesignon.findByAuthKey(StringEncryption.getDecryptedString(URLDecoder.decode(params.authNr, "utf-8")))

        // if the user is authenticated.
        if(singleSignOn != null){
            String parameterXML = singleSignOn.getXml()
            if (parameterXML != null) {
                def params = new XmlSlurper().parseText(parameterXML).children()
                Map parameterMap = new HashMap<String, String>()
                params.each {
                    parameterMap.put((String) it.@name, (String) it)
                }

                Member member = Member.findByIdNumber(parameterMap.get("memberIDNo"));
                Institution institution1 = (Institution) Institution.get(parameterMap.get("institutionID"))
                String dealerCode = parameterMap.get("dealerCode")
                String thirdPartyID = parameterMap.get("thirdPartyID")
                log.info("dealerCode: ${dealerCode}*****thirdPartyID:${thirdPartyID}****institution1:${institution1}")
                singleSignOn.delete();

                if (institution1 != null && dealerCode != null && thirdPartyID != null && member != null) {
                    InstitutionInstitution ii = InstitutionInstitution.findByMerchantIdAndInstitution1AndThirdPartyId(dealerCode, institution1,thirdPartyID)
                    if (ii == null) {
                        log.error("${new Date()}.InstitutionInstitution not found on:\nInstitutionID: ${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}, MemberIDNo: ${member?.getIdNumber()}.")
                        error("Vehicle Inbox - Authentication Error (RegTrack)", "Financial Institution and dealership not linked! Please contact support.",
                                "Failed to retrieve Institution_Institution from:\n" +
                                        "InstitutionID:${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}, MemberIDNo: ${parameterMap.get("memberIDNo")}.", null)
                    } else {
                        session["memberID"] = member.getId()
                        session["institutionID"] = ii.getInstitution2().getId()
                        log.info("user: " + session["memberID"] + " (institution:'" + session["institutionID"] + "') authenticated");

                        String clientOrJuristic = parameterMap.get("clientJuristic")

                        if (clientOrJuristic == null || "".equals(clientOrJuristic)) {
                            if(((String) parameterMap.get("idNo"))?.contains('/')) {
                                clientOrJuristic = "Juristic"
                            } else {
                                if (((String) parameterMap.get("idNo"))?.length() == 13) {
                                    clientOrJuristic = "Client"
                                } else {
                                    clientOrJuristic = "Foreigner"
                                }

                            }
                        }

                        [
                                // Vehicle Details
                                vinNumber: parameterMap.get("vinNo"), registrationNumber: parameterMap.get("regNo"), mmCode: parameterMap.get("mmCode"),
                                yearOfFirstRegistration: parameterMap.get("year"), newOrUsed: parameterMap.get("newUsed"), clientOrJuristic: clientOrJuristic,
                                // Customer Details
                                customerFirstName: parameterMap.get("firstName"),  customerSurname: parameterMap.get("surname"), customerIdNumber: parameterMap.get("idNo"),
                                customerEmail: parameterMap.get("email"),customerDateOfBirth: parameterMap.get("dateOfBirth"), customerPhoneNumber: parameterMap.get("telephone"),
                                customerResidentialAddress: parameterMap.get("physicalAddress"), customerTitle: parameterMap.get("title"),
                                customerPostalAddress: parameterMap.get("postalAddress"),
                                customerBankAccountNumber: parameterMap.get("accountNo"),
                                // Company Details
                                companyName: parameterMap.get("firstName"), businessRegistrationNumber: parameterMap.get("idNo"),
                                companyEmail: parameterMap.get("email"), companyPhoneNumber: parameterMap.get("telephone"),
                                companyResidentialAddress: parameterMap.get("physicalAddress"),
                                companyPostalAddress: parameterMap.get("postalAddress"),
                                companyBankAccountNumber: parameterMap.get("accountNo"),
                                // Dealer Details
                                memberId: parameterMap.get("memberIDNo"), institutionID: parameterMap.get("institutionID"),
                                dealerCode: parameterMap.get("dealerCode"), batchRef: parameterMap.get("batchRef"),
                                thirdPartyID: parameterMap.get("thirdPartyID"), bank:institution1.getName()
                        ];
                   }
                } else {

                    if (member == null) {
                        log.error("${new Date()}.Member does not exists in VAF5 DB on id Number: ${parameterMap.get("memberIDNo")}\nOther Parameters: " +
                                "InstitutionID:${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}.")
                        error("Vehicle Inbox - Authentication Error (RegTrack)", "User not found on id number: ${parameterMap.get("memberIDNo")}! Please contact support.",
                                "Member not found on id number: ${parameterMap.get("memberIDNo")}\n\nThe following parameters where sent through from the front-end:\n" +
                                        "InstitutionID:${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}, MemberIDNo: ${parameterMap.get("memberIDNo")}.", null)
                    } else {
                        log.error("${new Date()}.Not all params where sent through:\n" +
                                "InstitutionID:${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}, MemberIDNo: ${parameterMap.get("memberIDNo")}.")
                        error("Vehicle Inbox - Authentication Error (RegTrack)", "Not all parameters where passed from the Signing-Boardroom front-end!. Please contact support.",
                                "Initialisation failed on the following parameters:\n" +
                                        "InstitutionID:${institution1?.id}, DealerCode: ${dealerCode}, ThirdPartyID: ${thirdPartyID}, MemberIDNo: ${parameterMap.get("memberIDNo")}.", null)
                    }
                }
            }
        } else{
            log.error("${new Date()}.User not authenticated:\nPARAMS: ${params}.")
            error("Vehicle Inbox - Error", "The user was not authorised to access RegTrack, please contact support.",
                    "The User was not authorised on the following parameters: \n${params}", null)
        }
    }

	def view(){
		// check that we at least got an ID.
		if(params.id == null || params.id.isEmpty()){
			render ([error: "no ID was specified"]) as JSON;
			return;
		}

		// get a deal and check that it is not null
		Deal deal = (Deal) Deal.get(params.id);
		if(deal == null){
            log.error("${new Date()}. Error retrieving ALV form! No Deal exists with ID: ${params.id}")
            error("Vehicle Inbox - ALV Form Error", "Error retrieving ALV form!",
                    "No Deal exists with ID: ${params.id}", null)
			return;
		}

		SignioDataModel sdm = (SignioDataModel) deal.getSignioDataModel("originalXml");
        String pdfContent = null;
        sdm.getFields().get("").get("list").each {
            pdfContent = it.get("documentContents")?.get("value")
        }

        if (pdfContent == null){
            log.error("${new Date()}. An error occurred while fetching the ALV-form from the Database on Deal:\n" +
                    "ID: ${deal.getId()}, Batch Ref: ${deal.getBatchRef()}")
            error("Vehicle Inbox - ALV Form error", "Error retreiving ALV form!",
                    "An error occurred while fetching the ALV-form from the Database on Deal:\n" +
                    "ID: ${deal.getId()}, Batch Ref: ${deal.getBatchRef()}", null)
        }
        else {
            response.contentType = 'application/pdf'
            byte[] pdfData = pdfContent.getBytes()
            def out = response.getOutputStream()
            out.write(Base64.decodeBase64(pdfData))
            out.flush()
        }
	}

    def createRegTrackRequestFromPOST() {
        try {
            def result = regtrackService.sendLogTransactionRequest()
            render result
        } catch (Exception ex) {
            log.error("RegTrack Request Failed!", ex)
            def returnMessage = [
                    success:false,
                    message: "RegTrack Request Failed!\n Error: ${ex.getMessage()}"
                ]
            render returnMessage as JSON
        }
    }

    /**
     * VAF-8372 PdvW
     * This function will get the deal data for the deal.
     * This is simply to display a audit trail for this deal in a popup.
     * Uses the parameter deal id, and sends a formatted string back.
     * It will check if it must take the comment in the entry, or get
     * the actual state from the database.
     * Will handle the error states too, and replaces the
     * getRequestErrors function.
     */
    def getDealdataForRegtrackDeal(){
        try{
            def renderReturnString = ""

            def deal_ID = params.id

            //Get the relevant deal.
            Deal regTrackDeal = (Deal) Deal.get(deal_ID)

            if (regTrackDeal==null) {
                log.error("Deal could not be found on ID: ${deal_ID}.")
                render  "Deal could not be found on ID: ${deal_ID}. RegTrack Status Unknown."
            }

            //Get its history
            def dealHistory = Dealdata.findAllByDeal(regTrackDeal);

            //Sort it by date
            dealHistory = dealHistory.sort{it.dateCreated}
            Collections.reverse(dealHistory)

            //Get every history entry and format it.
            int count = 0;
            for (deal in dealHistory)   {
                if(deal.getComment() !=  null){
                    renderReturnString += "${++count}.) " + deal?.getComment()?.trim()  + "\nDate: " + deal.getDateCreated().toString() + "\n\n"
                } else{
                    renderReturnString += "${++count}.) " + Dealstate.findById(deal.getDealstateId()).getDescription()?.trim() + "\nDate: " +deal.getDateCreated().toString() + "\n\n"
                }
            }

            def currentResult;

            //Final history entry is the deal itself.
            if(regTrackDeal.getComment() != null) {
                currentResult = "<b>Current Status:</b>\n" + regTrackDeal?.getComment()?.trim() + "\nDate: " +  regTrackDeal.getDateUpdated().toString()
            }else{
                currentResult = "<b>Current Status:</b>\n" + Dealstate.findById(regTrackDeal.getDealstateId()).getDescription()?.trim() + "\nDate: " + regTrackDeal.getDateUpdated().toString()
            }

            //Make sure that the history must be rendered.
            if(dealHistory.size() > 0){
                renderReturnString = currentResult   +  "\n\n<b>History:</b>\n" + renderReturnString
            }else{
                renderReturnString = currentResult
            }

            render renderReturnString
        }catch(Exception e){
            log.error("RegtrackController:getDealdataForRegtrackDeal: " + e.printStackTrace())
            return  "An deal history error occurred. RegTrack Status Unknown."
        }
    }

    def getRequestErrors() {
        String retMessage;
        def dealID = params.id
        if (dealID==null || dealID=="") {
            retMessage =  "An unknown error occurred. RegTrack Status Unknown."
        } else {
            Deal regTrackDeal = (Deal) Deal.get(dealID)
            retMessage = regTrackDeal?.getComment()
        }

        if (retMessage == null) {
            retMessage =  "An unknown error occurred. RegTrack Status Unknown."
        }
        render retMessage
    }

    def error(String errorTitle, String shortMessage, String fullMessage, String cause) {
        chain(
                controller: "error",
                action: "viError",
                model:[
                        title: errorTitle,
                        shortMessage: shortMessage,
                        fullMessage: fullMessage,
                        cause:cause
                ]
        )
    }

    // THIS FUNCTION IS FOR TESTING PURPOSES ONLY!
    def test() {
        if ('live'.equals(Environment.currentEnvironment.name)){
            chain(controller: "error",
                    action: "viError",
                    model: [
                            shortMessage: "This URL only works in the test environments!"
                    ]
            )
        } else {
            Member member = Member.findByIdNumber('8904305754082'); //VAP Training
            Institution institution1 = (Institution) Institution.get(8352L);
            String dealerCode = '00013719' //'00009545'
            String thirdPartyID = '3RD00004'

            InstitutionInstitution ii = InstitutionInstitution.findByMerchantIdAndInstitution1AndThirdPartyId(dealerCode, institution1, thirdPartyID)

            session["memberID"] = member.getId()
            session["institutionID"] = ii.getInstitution2().getId()
            log.info("user: " + session["memberID"] + " (institution:'" + session["institutionID"] + "') authenticated");

            String clientOrJuristic = 'Client'

            def parameterMap = [
                    // Vehicle Details
                    vinNumber: 'VIN12365845683', registrationNumber: 'ABC 123 MP', mmCode: '60079403',
                    yearOfFirstRegistration: '2014', newOrUsed: 'New', clientOrJuristic: clientOrJuristic,
                    // Customer Details
                    customerFirstName: 'Jimmy',  customerSurname: 'Buffet', customerIdNumber: '8904305754082',
                    customerEmail: 'jb@fakemail.nz',customerDateOfBirth: '19611010', customerPhoneNumber: '0123456789',
                    customerResidentialAddress: '16 Zoete Waters, 123 ABC Lane, Faerie Glen, Pretoria, 0081', customerTitle: 'Mr',
                    customerPostalAddress: '16 Zoete Waters, 123 ABC Lane, Faerie Glen, Pretoria, 0081',
                    customerBankAccountNumber: '86123627389',
                    // Company Details
                    companyName: 'TEST ICICLES CC', businessRegistrationNumber: '2014/3723/2323',
                    companyEmail: 'test@icicles.uk', companyPhoneNumber: '0123456789',
                    companyResidentialAddress: '16 Zoete Waters, 123 ABC Lane, Faerie Glen, Pretoria, 0081',
                    companyPostalAddress: '16 Zoete Waters, 123 ABC Lane, Faerie Glen, Pretoria, 0081',
                    companyBankAccountNumber: '86123627389',
                    // Dealer Details
                    memberId: '8904305754082', institutionID: '8352',
                    dealerCode: '00013719', batchRef: '86123627389',
                    thirdPartyID: '3RD00004', bank:institution1.getName()
            ];

            render(view: "index", model: parameterMap)

        }
    }
}
