package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 5/17/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
class Reason {

    String description

    static hasMany = [retentionActions:Retentionaction]

    static constraints = {
        description(nullable: false, maxSize: 255)
    }

    static mapping = {
        version(false)
    }
}
