if (typeof signio === 'undefined') {
	var signio = {}
}

signio.vehicleInbox = {
	callAjax : function(url, type, data, success){
		var response = null;
		$.ajax({
			url: url,
			async : false,
			contentType: 'application/json; charset=utf-8',
			type : (type == null) ? "GET" : type,
			data: (data == null) ? { _ : $.now()} : JSON.stringify(data),
			error: function(data){
				//console.log("could not retrieve data"); 
			},
			success: (success != null) ? success : function(data){ response = data; }
		});
		return response;
	},

    callAjaxAsync : function(url, type, data, success){
        $.ajax({
            url: url,
            async : true,
            contentType: 'application/json; charset=utf-8',
            type : (type == null) ? "GET" : type,
            data: (data == null) ? { _ : $.now()} : JSON.stringify(data),
            error: function(data){
                //console.log("could not retrieve data");
            },
            success: success
        });
    }
};