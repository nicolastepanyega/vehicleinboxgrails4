package za.co.signio.model

class Rolesetup {

    String authority
    String url

    static belongsTo = [Relationshiptype]
    static hasMany = [relationshipTypes:Relationshiptype]


    static mapping = {
        version false
        relationshipTypes joinTable: 'relationshiptype_rolesetup'
    }

    static constraints = {
        authority(maxSize: 45, nullable: false, blank: false, unique: true)
        url(maxSize: 255, nullable: true)
    }

    String toString() {
//        return name
        return authority
    }

}
