package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/08/26
 * Time: 8:51 AM
 * To change this template use File | Settings | File Templates.
 */
class Vehicleoptionalextra {

    Integer id
    String name
    String code

    static hasMany = [vehicles:Vehicle, vehicleoptionalextraInstitutions: VehicleoptionalextraInstitution]

    static constraints = {
        name(maxSize: 100, nullable: false)
        code(maxSize: 45, nullable: false)
    }

    static mapping = {
        version false
        vehicles joinTable: 'vehicle_vehicleoptionalextra'
    }
}
