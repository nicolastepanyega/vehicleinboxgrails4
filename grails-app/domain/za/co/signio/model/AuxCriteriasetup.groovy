package za.co.signio.model

class AuxCriteriasetup {

    String operator
    String value

    static hasOne = [auxValueindicator:AuxValueindicator,
                     auxCriteriafield:AuxCriteriafield,
                     auxProductoption:AuxProductoption]

    static belongsTo = [AuxCriteriafield, AuxValueindicator, AuxProductoption]


    static constraints = {
        auxProductoption(nullable: false)
        auxCriteriafield(nullable: false)
        operator(nullable: false, maxSize: 45)
        value(nullable: true)
        auxValueindicator(nullable: true)
    }

    static mapping = {
        version false;
    }

    public String toString(){
        "${auxProductoption?.productOptionName} >> ${auxCriteriafield?.formfield?.name} ${operator} ${value} ${auxValueindicator?:''}"
    }
}
