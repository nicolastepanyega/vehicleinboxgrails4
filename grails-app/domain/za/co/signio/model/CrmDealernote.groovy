package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmDealernote {

    String note

    static hasMany = [crmSharedRelationships:CrmSharedrelationship]

    static constraints = {
        note(nullable: false, maxSize: 255)
    }

    static mapping = {
        version(false)
    }
}
