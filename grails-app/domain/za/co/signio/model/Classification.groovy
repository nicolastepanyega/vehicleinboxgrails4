package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/07/11
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
class Classification {

    String name

    static hasMany = [institutionInstitutions:InstitutionInstitution]

    static constraints = {
        name nullable: false, maxSize: 45

    }

    static mapping = {
        version(false)
    }
}
