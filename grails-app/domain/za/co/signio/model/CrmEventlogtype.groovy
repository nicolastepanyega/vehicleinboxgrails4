package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:01 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmEventlogtype {

    String type
    String system

    static hasMany = [crmEventLogs:CrmEventlog]

    static constraints = {
        type(nullable: false, maxSize: 45)
        system(nullable: true)
    }

    static mapping = {
        version(false)
    }

}
