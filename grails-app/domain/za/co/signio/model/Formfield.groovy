package za.co.signio.model

class Formfield {

    String name
    String label
    String description
    Integer parentId
    Boolean isDocField
    Boolean isEditableDoc
    Integer docOrder

    static hasMany = [  formfieldvalues: Formfieldvalue,
                        mappings: Mapping,
                        docs: Doc,
                        members:Member,
                        auxCriteriaFields:AuxCriteriafield,
                        bankvapsCriteriafields:BankvapsCriteriafield
        
    ]

    static belongsTo = [Member]

    static mapping = {
        version false
        table('formfield')
        mappings joinTable: 'mapping_formfield'
        members joinTable: 'member_formfield'
        formfieldvalues sort: 'id'
    }

    static constraints = {
        name nullable: false, maxSize: 255
        label nullable: false, maxSize: 300
        description nullable: true, maxSize: 300
        parentId nullable: true
        isDocField nullable: true
        isEditableDoc nullable: true
        docOrder nullable: true
    }

    String toString() {
        return name
    }
}
