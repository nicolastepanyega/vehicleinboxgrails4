<%--
  Vehicle details main page
  User: chris.ferreira@signio.co.za
  Date: 2013/07/26
  Time: 11:29 AM
  --%>
<g:javascript library="jquery" plugin="jquery"/>
<r:require modules="jquery-validate, jquery-ui" />
<!DOCTYPE html>
<html>
<head>
    %{--<meta http-equiv="X-UA-Compatible" content="IE=8"/>--}%
    <meta name="layout" content="minimal"/>
    <title>Vehicle Inbox - V5 - Add Vehicle</title>
    %{-- <r:layoutResources/> --}%
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'json.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/businessRules', file: 'vehicleinbox.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.migrate.js')}"></script>
    <style type="text/css">
    label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
    </style>
</head>
<body>
<table width="98%"  class="responsive BGColourDarkBlue">
    <tr><td class="BGColourWhite" > <g:img dir="images" file="signio_logo.png"/></td></tr>
    <tr><td class="BGColoursHeading" ><h4>New Vehicle</h4></td></tr>
    <g:render template="/vehicledetail/viewControls" />
    <g:render template="/vehicledetail/newVehicleDetails" />
</table>
%{-- <r:layoutResources/> --}%

<div id="blockModal" style="display:none"  title="Basic dialog">Please Wait</div>
</body>
</html>
