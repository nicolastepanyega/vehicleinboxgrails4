package za.co.signio.webservice

public enum WebServiceResponseState{
	ERROR,		// Used when something goes wrong with the connection- timeout, service unavailable, connection refused, etc.
	FAILED,		// Used when there was an error from the webservice- missing fields, invalid data, whatever.
	SUCCESS		// Used to indicate that a successful response was received.
}