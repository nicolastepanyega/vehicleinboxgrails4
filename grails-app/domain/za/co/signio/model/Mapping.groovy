package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Monica Longhurst
 * Date: 2011/08/10
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
class Mapping {
    String name
    Institution institution

    static belongsTo = [Institution, Formfield]
    static hasMany = [formFields:Formfield]

    static mapping = {
        version false
        table('mapping')
        formFields(joinTable: 'mapping_formfield')
    }

    static constraints = {
        name nullable: false, maxSize: 45
        institution nullable: false
    }

    String toString() {
        return name
    }

}
