package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 1/25/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
class Memberinterest {

    String description
    Member member
    Status status

    static belongsTo = [Member, Status]

    static constraints = {
        description(nullable: false, maxSize: 255)
        member(nullable: false)
        status(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
