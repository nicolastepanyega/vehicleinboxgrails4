package za.co.signio.model

class AuxProductoptioninstitutionfspconfig {

    Double factoredCommissionMonths
    Double factoredPercentageOfExtrafee
    Double factoredExtrafeeMonths

	static belongsTo = [AuxProductoption, AuxInstitutionfspconfig, AuxProductcategoryformula]
    static hasOne = [auxInstitutionfspconfig:AuxInstitutionfspconfig,
                     auxProductoption:AuxProductoption,
                     auxProductcategoryformula:AuxProductcategoryformula]


    static constraints = {
        auxInstitutionfspconfig(nullable: false)
        auxProductoption(nullable: false)
        auxProductcategoryformula(nullable: false)
        factoredCommissionMonths(nullable: true)
        factoredPercentageOfExtrafee(nullable: true)
        factoredExtrafeeMonths(nullable: true)

	}

    static mapping = {
        version false
    }

    public String toString(){
        "${auxInstitutionfspconfig?.institution1?.name} >> ${auxInstitutionfspconfig?.institution2?.name} >> ${auxProductoption?.productOptionName}"
    }
}
