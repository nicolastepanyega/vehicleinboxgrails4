package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/02/12
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxDeliverymethod {

    String label
    String value


    static hasOne = [auxDeliverymethodtype: AuxDeliverymethodtype]
    static belongsTo = [AuxDeliverymethodtype]
    static hasMany = [auxProductAuxDeliverymethods: AuxProductAuxDeliverymethod]

    static constraints = {
        label maxSize: 255, nullable: false
        value maxSize: 255, nullable: false
        auxDeliverymethodtype nullable: false

    }

    static mapping = {
        version false
        auxProductAuxDeliverymethods joinTable: 'aux_product_aux_deliverymethod'
    }
}
