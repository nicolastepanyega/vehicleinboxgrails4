package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/07/12
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
class Fieldvaluemappingfilter {
    Formfield formfield
    Formfieldvalue formfieldvalue1 //value
    Formfieldvalue formfieldvalue2 //valueMapping

    static belongsTo = [Formfield, Formfieldvalue]

    static constraints = {
        formfield(nullable: false)
        formfieldvalue1(nullable: false)
        formfieldvalue2(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
