package za.co.signio.model

class Messagecontent {

    String messageText;
    Byte[] image;
    Date dateCreated;

    static belongsTo = [Message, Member]

    static hasOne = [message: Message, member: Member]

    static mapping = {
        version false;
    }

    static constraints = {
        messageText maxSize: 8000, nullable: true;
        image nullable: true;
        dateCreated nullable: false;
        member nullable: false;
        message nullable: false;
    }

    String toString() {
        return member.firstName + " " + member.lastName + ":" + dateCreated.toString();
    }
}
