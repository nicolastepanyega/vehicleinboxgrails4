<%-- <g:javascript library="jquery" plugin="jquery"/> --%>
<%-- <r:require modules="jquery-validate, jquery-ui" /> --%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="minimal"/>
	<title>Vehicle Inbox - V5 - Request Natis</title>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.loadmask.css')}" />
	<link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'vi-jquery.css')}">
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'json.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.migrate.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.ui.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.validate.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'upclick-min.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/libs', file: 'jquery.loadmask.min.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/businessRules', file: 'vehicleinbox.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/businessRules/natis', file: 'validation.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js/businessRules/natis', file: 'fieldactions.js')}"></script>
	%{-- <r:layoutResources/> --}%
    
    <style type="text/css">
		label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
		div.error { color: red; padding-left: .5em; vertical-align: top; }
	</style>
</head>
<body>
    <form id="natisForm" action="">
	<table id="natisRequestTable" width="98%"  class="responsive BGColourDarkBlue">
		<tr><td class="BGColourWhite" ><img alt="SignioLogo" src="${resource(dir: 'images', file: 'signio_logo.png')}"></td></tr>
		<tr><td class="BGColoursHeading" >
			<h4>Request Natis</h4>
			<div style="display:inline" class="originalNatis">Please note that original natis will only be supplied once the account has been settled in full.</div>
		</td></tr>

		<script type="text/javascript">
			signio.vehicleInbox.dealID=${params.id};
			signio.vehicleInbox.natis = {};
		</script>

	    <g:render template="/natis/natis_type" />
	    <g:render template="/natis/natis_form" />
    </table>
    %{-- <r:layoutResources/> --}%
    </form>
    %{--<div id="blockModal" style="display:none"  title="Basic dialog">Please Wait</div>--}%
</body>
</html>
