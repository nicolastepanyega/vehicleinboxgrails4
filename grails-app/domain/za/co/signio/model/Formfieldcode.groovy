package za.co.signio.model


class Formfieldcode {

    String code
    String incomingCode
    Boolean includeVatIndicator
    Formfieldvalue formfieldvalue
    Institution institution
    FormfieldcodeType formfieldcodeType
    static belongsTo = [Formfieldvalue, Institution, FormfieldcodeType]

    static mapping = {
        version false
    }

    static constraints = {
        code nullable: false, maxSize: 45
        incomingCode nullable: true, maxSize: 45
        includeVatIndicator nullable: true
        formfieldvalue(nullable: false)
        institution(nullable: false)
        formfieldcodeType(nullable: true)
    }

    String toString() {
        return code
    }
}
