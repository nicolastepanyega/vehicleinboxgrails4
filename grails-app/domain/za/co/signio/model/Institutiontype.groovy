package za.co.signio.model

class Institutiontype {

    String type

    static hasMany = [institutions:Institution, institutionInstitutions:InstitutionInstitution, relationshipTypes:Relationshiptype, auxReferenceCodeConfigs: AuxReferencecodeconfig]
    static auditable = true

    static mapping = {
        version false
        relationshipTypes joinTable: 'institutiontype_relationshiptype'
    }

    static constraints = {
        type maxSize: 45, nullable: false
    }

    String toString() {
        return type
    }

    def onLoad = {
        //println("GORM(Institutiontype) Result: ${this.type}");
    }
}
