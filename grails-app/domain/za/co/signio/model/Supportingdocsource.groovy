package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 4/5/12
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
class Supportingdocsource {
    
    String source

    static hasMany = [supportingDocs:Supportingdocs]

    static constraints = {
        source(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
