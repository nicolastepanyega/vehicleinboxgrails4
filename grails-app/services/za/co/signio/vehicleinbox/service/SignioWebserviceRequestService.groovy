package za.co.signio.vehicleinbox.service

import groovy.util.ConfigObject;
import groovy.util.slurpersupport.GPathResult
import groovy.util.slurpersupport.NodeChild;
import grails.util.Holders;



import za.co.signio.webservice.WebServiceAction;
import za.co.signio.webservice.WebServiceProvider;
import za.co.signio.webservice.WebServiceResponse;
import za.co.signio.webservice.WebServiceResponseState;
import grails.plugins.rest.client.RestBuilder;
import org.springframework.web.client.ResourceAccessException;

class SignioWebserviceRequestService {
	
	private ConfigObject config = Holders.config;
	/*private static final Logger LOGGER = Logger.getLogger(this);*/

	public WebServiceResponse sendRequest(Class<WebServiceProvider> provider, WebServiceAction action, String xmlContent) {
		try{
			def resp = new RestBuilder().post(config.webService.signio + "/json"){
				json {
					institution = provider.getSimpleName()
					institutionAction = action.name()
					xml = xmlContent
				}
			}.xml;
			return new WebServiceResponse(WebServiceResponseState.valueOf(resp.state.toString().trim()), resp.message.toString().trim(), resp.result.toString().trim());
		}
		catch(ResourceAccessException e){
			return new WebServiceResponse(WebServiceResponseState.ERROR, "Could not connect to SharedWebservices", null);
		}
		
	}
}
