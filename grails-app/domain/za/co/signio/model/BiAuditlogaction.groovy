package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/15/12
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
class BiAuditlogaction {

    String name

    static hasMany = [biAuditlogs:BiAuditlog]

    static constraints = {
        name(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
