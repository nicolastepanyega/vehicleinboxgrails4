package za.co.signio.model

class AuxProduct {

    String productName
    String supplierReferenceCode
    String productPrefix
    String pasaAbbreviation
    Date productStartDate
    Date productEndDate
    Boolean active
    Boolean isRegulated
    Boolean useBest
    String collectionAgency
	String productSpecificQuestionClasses
	Boolean showValidOptions
    Boolean showBestOption
    Servicehandler servicehandler

    static hasOne = [auxProductcategory:AuxProductcategory,
                     institution:Institution]

    static hasMany = [auxProductOptions: AuxProductoption, auxProductAuxDeliverymethods: AuxProductAuxDeliverymethod,
                      auxComments: AuxComment, auxProductAuxLogos:AuxProductAuxLogo,
                      serviceconfigurations: Serviceconfiguration, dealdocumenttypes: Dealdocumenttype]
    static belongsTo = [Institution, AuxProductcategory, Dealdocumenttype]


    static mapping = {
        version false
        auxProductAuxDeliverymethods joinTable: 'aux_product_aux_deliverymethod'
        serviceconfigurations joinTable: 'aux_product_serviceconfiguration'
        dealdocumenttypes joinTable: 'aux_product_dealdocumenttype'
    }

    static constraints = {
        institution(nullable: false)
        institution2(nullable: true)
        auxProductcategory(nullable: false)
        productName(nullable: false, maxSize: 255)
        supplierReferenceCode (nullable: true, maxSize: 45)
        productPrefix( nullable: true, maxSize: 20)
        pasaAbbreviation(nullable: true, maxsize: 45)
        productStartDate(nullable: true)
        productEndDate(nullable: true)
        active(nullable: false)
        isRegulated(nullable: false)
        useBest(nullable: true)
        collectionAgency(nullable: true, maxSize: 45)
		productSpecificQuestionClasses(nullable: true, maxSize: 255)
		showValidOptions(nullable: false, default: false)
        showBestOption(nullable: true, default: false)
        servicehandler(nullable: true)
    }

    def String toString() {
        return productName
    }

}


