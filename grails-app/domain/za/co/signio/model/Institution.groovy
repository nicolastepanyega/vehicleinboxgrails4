package za.co.signio.model

class Institution {

    String name
    String originatorId
    String branchCode
    String vatNumber
    Date contractRenewelDate
    Integer billingOption
    BigDecimal invoiceRate
    String legalName
    String registrationNumber
    String ncrcpNumber
    String thirdPartyId
    Long corporate
    Long lft
    Long rgt
    byte[] logo
    String logoMimeType
    Date dateCreated
    Date dateUpdated
    Integer createdByMember
    Boolean active

    static belongsTo = [Suburb, Contactinfo, Institutiontype, Communicationtype, Status, Theme, IndemnityFedility, Bankingdetail]
    static hasMany = [  children: Institution,
                        relationships: Relationship,
                        banks: InstitutionInstitution,
                        dealers: InstitutionInstitution,
                        reports: Report,
                        dealgroups:Dealgroup,
                        documentTemplates:Documenttemplate,
                        reportInstitutions:ReportInstitution,
                        consultantLogs:Consultantlog,
                        vehicleDetails:Vehicledetail,
                        vehicles:Vehicle,
                        processingCentreAreas:Processingcentrearea,
                        crmEventLogs:CrmEventlog,
                        crmSharedRelationships:CrmSharedrelationship,
                        crmEmailAuditLogs:CrmEmailauditlog,
                        assetAllocateds:Assetallocated,
                        institutionMemberSupportingdocAccess:Institutionmembersupportingdocaccess,
                        auxBanks: AuxProductoptionbulkingconfig,
                        auxDealers: AuxProductoptionbulkingconfig,
                        fspSuppliers: AuxInstitutionfspconfig,
                        fspDealers: AuxInstitutionfspconfig,
                        auxProducts:AuxProduct,
                        deals:Deal,
                        supportingDocs:Supportingdocs,
                        auxFspcategoryinstitutionconfig:AuxFspcategoryinstitutionconfig,
                        documentTemplateGroups:Documenttemplategroup,
                        auxReferenceCodeConfigs: AuxReferencecodeconfig,
                        otps:Otp,
                        temporaryDocumentStorages: TemporaryDocumentStorage,
                        institutionSetupoptions: InstitutionSetupoption,
                        vehicleoptionalextraInstitutions: VehicleoptionalextraInstitution,
                        documenttemplategroupInstitution: DocumenttemplategroupInstitution,
                        serviceconfigurations: Serviceconfiguration,
                        qualifications: Qualification,
                        profiledocuments: Profiledocument,
                        moduleSpecificFields:Modulespecificfield,
    ]

    static hasOne = [status:Status,
                     contactinfo:Contactinfo,
                     institutiontype:Institutiontype,
                     communicationtype:Communicationtype,
                     theme:Theme,
                     indemnityFedility:IndemnityFedility,
                     parent:Institution,
                     suburb:Suburb, contactinfo:Contactinfo,
                     institutiontype:Institutiontype,
                     bankingdetail:Bankingdetail]

    static auditable = true

    static mappedBy = [banks: 'institution1',
                       dealers: 'institution2',
                       children: 'parent',
                       auxBanks: 'institution1',
                       auxDealers: 'institution2',
                       fspSuppliers: 'institution1',
                       fspDealers: 'institution2']


    static mapping = {
        version false
        reports joinTable: 'report_institution'
        vehicles joinTable: 'institution_vehicle'
        processingCentreAreas joinTable: 'processingcentrearea_institution'
        documentTemplateGroups joinTable: 'documenttemplategroup_institution'
        thirdPartyId column: '3rd_party_id'
    }

    static constraints = {
        name maxSize: 255, nullable: false
        originatorId nullable: true, maxSize: 11
        branchCode nullable: true, maxSize: 45
        vatNumber nullable: true, maxSize: 45
        contractRenewelDate nullable: true
        billingOption nullable: true
        invoiceRate nullable: true
        legalName nullable: true, maxSize: 45
        registrationNumber nullable: true, maxSize: 45
        ncrcpNumber nullable: true, maxSize: 45
        thirdPartyId nullable: true, maxSize: 45
        corporate nullable: true
        lft nullable: true
        rgt nullable: true
        logo nullable: true
        logoMimeType nullable: true
        suburb nullable: true
        createdByMember nullable: true
        active(nullable: false)
        contactinfo(nullable: false)
        institutiontype(nullable: false)
        communicationtype(nullable: true)
        status(nullable: false)
        parent(nullable: true)
        theme(nullable: true)
        indemnityFedility(nullable: true)
        deals(nullable: true)
        bankingdetail(nullable: true)
        dateUpdated(nullable: true)
    }

    String toString() {
        name
    }
}
