package za.co.signio.model


class Contactinfo{

    String physicalAddress_1
    String physicalAddress_2
    String physicalAddress_3
    String physicalAddressCode
    String postalAddress_1
    String postalAddress_2
    String postalAddress_3
    String postalAddressCode
    String latitude
    String longitude
    String telHome
    String telOffice
    String cellphoneNumber
    String faxHome
    String faxOffice
    String email
    String ftpUrl
    String webserviceEndpoint

    static hasMany = [institutions: Institution,
                      clientdetails: Clientdetail,
                      members: Member,
                      companyDetail:Companydetail,
                      auxFspnumbers:AuxFspnumber]

    static auditable = true

    static mapping = {
        version false
    }

    static constraints = {
        physicalAddress_1 nullable: true, maxSize: 100
        physicalAddress_2 nullable: true, maxSize: 100
        physicalAddress_3 nullable: true, maxSize: 100
        physicalAddressCode nullable: true, maxSize: 10
        postalAddress_1 nullable: true, maxSize: 100
        postalAddress_2 nullable: true, maxSize: 100
        postalAddress_3 nullable: true, maxSize: 100
        postalAddressCode nullable: true, maxSize: 10
        latitude nullable: true
        longitude nullable: true
        telHome nullable: true, maxSize: 45
        telOffice nullable: true, maxSize: 45
        cellphoneNumber nullable: true, maxSize: 45
        faxHome nullable: true, maxSize: 45
        faxOffice nullable: true, maxSize: 45
        email nullable: true, maxSize: 100, email: true
        ftpUrl nullable: true ,maxSize: 255
        webserviceEndpoint nullable:true ,maxSize: 255

    }

    def String toString() {
        return email
    }
}
