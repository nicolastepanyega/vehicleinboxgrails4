package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 5/3/12
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
class Dealmessage {
    
    String clientResponse
    Date dateCreated
    Deal deal

    static belongsTo = [Deal]

    static constraints = {
        clientResponse(nullable: false)
        dateCreated(nullable: false)
        deal(nullable: false)
    }

    static mapping = {
        version false
    }
}
