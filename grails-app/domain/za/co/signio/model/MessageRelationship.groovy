package za.co.signio.model

class MessageRelationship {

    boolean isRead;
    Date dateRead;

    static belongsTo = [Message, Relationship]

    static hasOne = [message: Message, relationship: Relationship]

    static mapping = {
        version false;
    }

    static constraints = {
        isRead nullable: true;
        dateRead nullable: true;
        message nullable: false;
        relationship nullable: false;
    }

    String toString() {
        return message?.id + ":" + relationship?.id;
    }
}
