package za.co.signio.model

class BankvapsCriteriafield {

    String label
    Formfield formfield

    //static hasOne = [formfield:Formfield]

    static belongsTo = [Formfield]
    static hasMany = [bankvapsCriteriasetups:BankvapsCriteriasetup]

    static constraints = {
        formfield nullable: false
        label(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }

    public String toString(){
        formfield?.name
    }

}

