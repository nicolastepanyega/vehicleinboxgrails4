package za.co.signio.vehicleinbox.controller

import za.co.signio.model.Vehiclemanufacturer
import grails.converters.JSON
import grails.util.Holders

import za.co.signio.model.Vehicle
import za.co.signio.model.Formfieldvalue

class VehicleDetailUtilController {

    def vehicleDetailService;

    def index() {
        render "hallo"
    }

    /**
     * Get map of vehicle manufacturer id's and names
     *
     * @return JSON map list of vehicle manufacturer id's and names
     */
    def getVehicleManufacturers() {
        render vehicleDetailService.getVehicleManufacturers() as JSON
    }

    /**
     * Get list of possible years
     *
     * @param id (Long): vehicleManufacturer id
     * @return list of possible years
     */
    def getVehicleYears() {
        render vehicleDetailService.getVehicleYears((params.id != null? Long.parseLong(params.id):null)) as JSON
    }

    /**
     * Get list of vehicle models
     *
     * @param id (Long) : vehicleManufacturer id
     * @param year (Integer) : registration year (yyyy)
     * @return Map list of vehicle model id's and descriptions
     */
    def getVehicleModels() {
        render vehicleDetailService.getVehicleModels((params.id != null? Long.parseLong(params.id):null),
                (params.year != null? Integer.parseInt(params.year):null)) as JSON
    }

    /**
     * Get list of vehicle colours from formFieldValue
     *
     * @return List of colours
     */
    def getVehicleColours() {
        render vehicleDetailService.getVehicleColours() as JSON
    }
}
