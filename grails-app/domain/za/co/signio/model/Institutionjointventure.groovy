package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 2/27/12
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
class Institutionjointventure {
    
    String description
    String inputCode
    String sourceCode
    Integer sbsaHiveNumber
    String vdnCode
    String brokerCode
    Boolean isCompulsory
    InstitutionInstitution institutionInstitution

    static belongsTo = [InstitutionInstitution]

    static constraints = {
        description(nullable: false, maxSize: 45)
        inputCode(nullable: false, maxSize: 45)
        sourceCode(nullable: true, maxSize: 45)
        sbsaHiveNumber(nullable: true)
        vdnCode(nullable: true, maxSize: 45)
        brokerCode(nullable: true, maxSize: 45)
        isCompulsory(nullable: true)
        institutionInstitution(nullable: false)
    }

    static mapping = {
        version false
    }
}
