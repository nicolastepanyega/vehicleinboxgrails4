<tbody class='companyDetails'><tr class='BGColourBlue'><td colspan="4">Company Details</td></tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Company Name</td>
    <td style="width:30%;">
        <input name="companyName" id="companyName" type="text" disabled="disabled" style="width:96%" value="${companyName}">
    </td>
    <td style="width:20%;">Business Registration No</td>
    <td style="width:30%;">
        <input name="businessRegistrationNumber" id="businessRegistrationNumber" type="text" disabled="disabled" style="width:96%" value="${businessRegistrationNumber}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Email Address</td>
    <td style="width:30%;">
        <input name="companyEmail" id="companyEmail" type="text" disabled="disabled" style="width:96%" value="${companyEmail}">
    </td>
    <td style="width:20%;">Telephone</td>
    <td style="width:30%;">
        <input name="companyPhoneNumber" id="companyPhoneNumber" type="text" disabled="disabled" style="width:96%" value="${companyPhoneNumber}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Physical Address</td>
    <td style="width:30%;">
        <input name="companyResidentialAddress" id="companyResidentialAddress" type="text" disabled="disabled" style="width:96%" value="${companyResidentialAddress}">
    </td>
    <td style="width:20%;">Postal Address</td>
    <td style="width:30%;">
        <input name="companyPostalAddress" id="companyPostalAddress" type="text" disabled="disabled" style="width:96%" value="${companyPostalAddress}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Natis: Business Registration No</td>
    <td style="width:30%;">
        <input name="natisBusinessRegistrationNumber" id="natisBusinessRegistrationNumber" type="text" style="width:96%">
    </td>
    <td style="width:20%;">Natis: Control No</td>
    <td style="width:30%;">
        <input name="natisControlNumber" id="natisControlNumber" type="text" style="width:96%" value="">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack' style="display: none;">
    <td style="width:20%;">Account No</td>
    <td style="width:30%;">
        <input name="companyBankAccountNumber" id="companyBankAccountNumber" type="text" disabled="disabled" style="width:96%" value="${companyBankAccountNumber}">
    </td>
    <td style="width:20%;"></td>
    <td style="width:30%;"></td>
</tr>
</tbody>