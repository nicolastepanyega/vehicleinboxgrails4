package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmCalendar {

    String body
    Date startDate
    Date endDate

    static hasMany = [crmSharedRelationships:CrmSharedrelationship]

    static constraints = {
        body(nullable: false)
        startDate(nullable: true)
        endDate(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
