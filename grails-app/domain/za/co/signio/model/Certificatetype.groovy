package za.co.signio.model

class Certificatetype {

	String type

	static hasMany = [certificates:Certificate]

	static mapping = {
		version false
	}

	static constraints = {
		type nullable: false, maxSize: 45
	}

    String toString() {
        return type
    }

}
