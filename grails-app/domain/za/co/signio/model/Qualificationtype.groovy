package za.co.signio.model

class Qualificationtype {

    String type

    static hasMany = [qualifications:Qualification]

    static constraints = {
        type(nullable: false, maxSize: 255)
    }

    static mapping = {
        version false
    }
}
