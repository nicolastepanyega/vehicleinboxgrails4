package za.co.signio.vehicleinboxtags

import za.co.signio.model.Formfieldcode
import za.co.signio.model.Formfieldvalue
import za.co.signio.model.Institution
import za.co.signio.model.Institutiontype

class RegTrackTagLib {
    static namespace = "rt"

    def regTrackFormField = {attrs, body ->
        def formFieldName = attrs.formFieldName;
        def formFieldMap = getRegTrackFormFieldMap(formFieldName)

        if (formFieldMap.size() > 2) {
            out << createFormFieldSelectElement(formFieldName, formFieldMap)
        } else if (formFieldMap.size() <= 2 && formFieldMap.size() > 0) {
            out << createFormFieldRadioElement(formFieldName, formFieldMap)
        } else {
            out << ''
        }
    }

    def regTrackNewUsed = {attrs, body ->
        def value = attrs.value;
        def name = attrs.formFieldName
        out << createNewUsedElement(name, value);
    }

    def regTrackClientJuristic = {attrs, body ->
        def value = attrs.value;
        def name = attrs.formFieldName
        out << createClientJuristicElement(name, value);
    }

    private def createFormFieldSelectElement(String name, List list){
        return g.select(
                [
                        namespace: namespace,
                        name: name,
                        id: name,
                        from: list,
                        value: "",
                        optionKey: "code",
                        optionValue: "value",
                        noSelection: ["-1" : "--Select--"]
                ]
        )
    }

    private def createFormFieldRadioElement(String name, List list){
        StringBuilder stringBuilder = new StringBuilder()
        int index = 0I
        for (Map<String, String> m in list) {
            if (index == 0) {
                stringBuilder.append("<input type=\"radio\" name=\"$name\" id=\"$name\" value=\"").append(m.get("code")).append("\" checked=checked/>")
            } else {
                stringBuilder.append("<input type=\"radio\" name=\"$name\" id=\"$name\" value=\"").append(m.get("code")).append("\"/>")
            }
            stringBuilder.append(m.get("value")).append("  ")
            stringBuilder.append("<br />")
            index ++
        }
         return stringBuilder.toString()
    }

    private def createFormFieldHiddenSelectElement(String name, List list){
        return g.select(
                [
                        namespace: namespace,
                        name: name,
                        id: name,
                        from: list,
                        value: "",
                        optionKey: "code",
                        optionValue: "value"
                ]
        )
    }

    private def getRegTrackFormFieldMap(String formFieldName) {
        Institution ubiquitech = Institution.findByNameAndInstitutiontype('Ubiquitech', Institutiontype.findByType("Service Provider"));
        def regTrackFormFieldList = Formfieldcode.createCriteria().list {
            formfieldvalue {
                formfield {
                    eq "name", formFieldName
                }
            }
            eq("institution", ubiquitech)
            projections {
                property "code"
                formfieldvalue {
                    property "value"
                }
            }
        }
        return regTrackFormFieldList.collect{ [code: it[0], value: it[1]] }
    }

    private def createNewUsedElement(String name, String value) {
        List list = Formfieldvalue.createCriteria().list {
            formfield {
                eq "name", name
            }
            projections {
                property "value"
            }
        }

        final boolean enabled = (value != null && ("New".equals(value) || "Used".equals(value)));
        list.add(0, "--Select--");

        return g.select(
                [
                        namespace: namespace,
                        name: name,
                        id: name,
                        from: list,
                        value: value,
                        optionKey: "value",
                        optionValue: "value",
                        disabled: ("New".equalsIgnoreCase(value) || "Used".equalsIgnoreCase(value))
                ]
        )
    }

    private def createClientJuristicElement(String name, String value) {
        List list = new LinkedList()

        list.add(0, "--Select--");
        list.add(1,"Client")
        list.add(2,"Juristic")
        list.add(3,"Foreigner")

        return g.select(
                [
                        namespace: namespace,
                        name: name,
                        id: name,
                        from: list,
                        value: value,
                        optionKey: "value",
                        optionValue: "value",
                        disabled: ("Client".equalsIgnoreCase(value) || "Juristic".equalsIgnoreCase(value))
                ]
        )
    }
}
