package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmEmailauditlog {

    String emailAddress
    Date dateCreated

    static hasOne = [crmEmail: CrmEmail,
            member: Member,
            status: Status,
            institution: Institution]

    static belongsTo = [CrmEmail, Member, Status, Institution]

    static constraints = {
        emailAddress(nullable: false)
        dateCreated(nullable: false)
        crmEmail(nullable: false)
        member(nullable: false)
        status(nullable: false)
        institution(nullable: false)
    }

    static mapping = {
        version(false)
    }

}
