package za.co.signio.security.utils;

public class StringEncryption {
	
    private static final byte[] skey = new byte[]{54, 104, (byte)138, 114, 112, (byte)155, (byte)178, (byte)253, (byte)207, (byte)158, (byte)253, (byte)212, 48, (byte)164, 27, (byte)135, 31, 4, 59, 73, (byte)186, 89, 117, (byte)227};
    private static final byte[] iv = new byte[]{105, 36, 61, 45, 101, (byte)217, (byte)215, (byte)177, (byte)211, 111, (byte)234, 109, 62, 11, (byte)206, 59, (byte)177, 116, (byte)197, 59, (byte)157, (byte)215, 114, 114};

    private static RijndaelEncrypt crypt = new RijndaelEncrypt(skey, iv, (char) '|');
    private static RijndaelEncrypt decrypt = new RijndaelEncrypt(skey, iv, (char) '|');


    /**
     * This method encrypts the string value that is parsed.
     * @param stringVal
     * @return String Encrypted string value.
     */
    public static String getEncryptedString(String stringVal) {
        return crypt.encrypt(stringVal);
    }

    /**
     * This method decrypts the string value that is parsed.
     * @param stringVal
     * @return String Decrypted string value.
     */
    public static String getDecryptedString(String stringVal) {
    	try{
    		return decrypt.decrypt(stringVal);
    	}
    	catch(Exception e){
    		return null;
    	}
    		
    }
}