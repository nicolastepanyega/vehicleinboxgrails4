package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/05/06
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
class Companydetail {

    String companyName
    String registrationNumber
    String vatNumber
    Date dateCreated

    static hasOne = [contactinfo: Contactinfo, lookupdata: Lookupdata]
    static hasMany = [dealgroups:Dealgroup, supportingDocs:Supportingdocs, institutionMemberSupportingdocAccess:Institutionmembersupportingdocaccess, otps: Otp]

    static constraints = {
        companyName nullable: false, maxSize: 255
        registrationNumber nullable: false, maxSize: 45
        vatNumber nullable: true, maxSize: 45
        dateCreated nullable: false
        contactinfo nullable: false
        lookupdata nullable: true
    }

    static mapping = {
        version false
    }

    public static boolean checkCompanyExists(String registrationNumber)
    {
        if(Companydetail.findByRegistrationNumber(registrationNumber))
        {
            return true
        }
        else
        {
            return false
        }
    }


}
