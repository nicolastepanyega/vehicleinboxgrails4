package za.co.signio.model

class TemporaryDocumentStorage {

    String batchRef
    Date dateCreated
    String documentXml

    static hasOne = [institution:Institution]

    static mapping = {
        version false
        table 'temporary_document_storage'
    }

    static constraints = {
        batchRef nullable: false, maxSize: 45
        institution nullable: false
        documentXml nullable: true
        dateCreated nullable: true
    }

    def beforeInsert = {
        dateCreated = new Date()
    }

    String toString() {
        return batchRef
    }
}
