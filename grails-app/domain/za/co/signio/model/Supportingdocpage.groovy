package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/13
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
class Supportingdocpage {

    Byte[] page
    Byte[] thumbnail
    Supportingdocs supportingdocs

    static belongsTo = [Supportingdocs]
    static constraints = {
        page(nullable: false, blank: false)
        thumbnail(nullable: false, blank: false)
        supportingdocs(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
