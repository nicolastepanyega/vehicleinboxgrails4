package za.co.signio.model


class Serviceconfigurationtype {

    String type

    static hasMany = [serviceconfigurations: Serviceconfiguration]

    static mapping = {
        version false
    }

    static constraints = {
        type maxSize: 45, nullable: false
    }

    String toString() {
        return type
    }
}
