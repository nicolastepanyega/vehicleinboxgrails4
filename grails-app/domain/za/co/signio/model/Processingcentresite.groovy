package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 8/21/12
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
class Processingcentresite {

    String name
    Processingcentrearea processingcentrearea
    String siteCode
    String zone
    

    static belongsTo = [Processingcentrearea]

    static constraints = {
        name nullable: false, maxSize: 45
        processingcentrearea nullable: true
        siteCode nullable: false, maxSize: 45
        zone nullable: true, maxSize: 45
    }

    static mapping = {
        version false
    }


}
