package za.co.signio.model


class Country {

    String name

    static hasMany = [provinces: Province]

    static mapping = {
        version false
    }

    static constraints = {
        name maxSize: 45, nullable: false
    }

    String toString() {
        return this.name
    }
}
