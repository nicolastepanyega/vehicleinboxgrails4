package za.co.signio.model

class Communicationtype {

    String type

    static hasMany = [institution:Institution]

    static constraints = {
        type nullable: false
    }

    static mapping = {
        version false
    }

    String toString() {
        return type
    }
}
