package za.co.signio.model

class Role {

	String authority

	static belongsTo = [Membergroup]
	static hasMany = [memberGroups:Membergroup, members:Member]
	

	static mapping = {
		version false
        memberGroups joinTable: 'membergroup_role'
        members(joinTable: 'member_role')
        cache true
	}

	static constraints = {
        authority(maxSize: 45, nullable: false, blank: false, unique: true)
	}

    String toString() {
//        return name
        return authority
    }

}
