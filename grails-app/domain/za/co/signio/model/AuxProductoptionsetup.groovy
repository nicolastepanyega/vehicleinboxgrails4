package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/01/07
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxProductoptionsetup {

    Boolean isDebitOrder
    String debitOrderRange
    Integer debitOrderDayLimit
    Boolean isFirstDebitOrderProrata
    String additionalTk
    String paymentFrequency
    String paymentFrequencyDescription
    Integer term
    Integer cashTerm
    Integer kmValid
    Integer monthsValid
    Boolean isBinderLabel
    Boolean allowSecondInsured
    Boolean isDepositCovered
    Integer productExpireAtClientAge
    Boolean hideProductDetails
    Integer serviceIntervalsValid
    AuxCalculateformula auxCalculateformula
    AuxSignaturetype auxSignaturetype
    Boolean isExtra = false
    Boolean isExtended = false
    String signioIdentifier
    Boolean useProrataCalculation
    Integer prorataNominatedDay
    Boolean useDisplayAdminFee
    Boolean includeInspectionFee
    String displayOnlyValue
    Boolean isRegistrationStartDate
    String extraData

    static belongsTo = [AuxCalculateformula, AuxSignaturetype]

    static hasMany = [auxProductOptions:AuxProductoption]

    static constraints = {
        isDebitOrder(nullable: false)
        debitOrderRange(nullable: true, maxSize: 45)
        debitOrderDayLimit(nullable: true)
        isFirstDebitOrderProrata(nullable: true)
        additionalTk(nullable: true, maxSize: 90)
        paymentFrequency(nullable: false, maxSize: 45)
        paymentFrequencyDescription(nullable: true, maxSize: 45)
        term(nullable: true)
        cashTerm(nullable: true)
        kmValid(nullable: true)
        monthsValid(nullable: true)
        isBinderLabel(nullable: false)
        auxCalculateformula(nullable: false)
        allowSecondInsured(nullable: true)
        isDepositCovered(nullable: true)
        productExpireAtClientAge(nullable: true)
        hideProductDetails(nullable: true)
        serviceIntervalsValid(nullable: true)
        auxSignaturetype(nullable: true)
        isExtra(nullable:  false)
        isExtended(nullable: false)
        signioIdentifier(nullable: true, maxSize: 100)
        useProrataCalculation(nullable: true)
        prorataNominatedDay(nullable: true)
        useDisplayAdminFee(nullable: true)
        includeInspectionFee(nullable: true)
        displayOnlyValue(nullable: true, maxSize: 255)
        isRegistrationStartDate(nullable: true)
        extraData(nullable: true)
    }


    static mapping = {
        //paymentFrequency enumType:"ordinal"
        version false
    }


// Groovy not part of plugins anymore when using grails 3.0
//    def extraDataJsonObject(){
//        def returnObject = null
//        try{
//            returnObject = new JsonSlurper().parseText(extraData)
//        }catch (Exception e){
//            //Do nothing here if it errors return null
//        }
//        return returnObject
//    }

    /**
     * Monthly, Quaterly, Bi-Annually, Annually, Once-off
     */
//    enum PaymentFrequency {
//        M("M"), Q("Q"), B("B"), A("A"), O("O")
//        String id
//        PaymentFrequency(String id) {this.id = id}
////        String toString() {value}
////        String getKey() {name()}
    //}
}
