package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 11/6/12
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
class Qvlicense {
    
    String username
    String password
    Date dateAssigned
    Qvlicensetype qvlicensetype

    static hasMany = [reports:Report,memberReports: MemberReport]
    static belongsTo = [Qvlicensetype]

    static mapping = {
        version false
        reports joinTable: 'report_qvlicense'
    }

    static constraints = {
        username nullable: false, maxSize: 45
        password nullable: true, maxSize: 45
        dateAssigned nullable: false
        qvlicensetype nullable: false
    }


}
