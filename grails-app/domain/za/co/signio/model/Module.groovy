package za.co.signio.model

class Module {

    String name
    String url

    static hasMany = [relationshipmoduleconfigs:Relationshipmoduleconfig,moduletemplates:Moduletemplate]
    static belongsTo = [Moduletemplate]

    static mapping = {
        version false
        moduletemplates joinTable: [name: "module_moduletemplate", key: "module_id"]
    }

    static constraints = {
        name nullable: false, maxSize: 100
        url nullable: true, maxSize: 255
    }
}
