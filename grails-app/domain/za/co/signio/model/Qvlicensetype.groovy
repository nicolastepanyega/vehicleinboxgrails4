package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 11/6/12
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
class Qvlicensetype {

    String type

    static hasMany = [qvlicenses:Qvlicense]

    static mapping = {
        version false
    }

    static constraints = {
        type nullable: false, maxSize: 45
    }

    String toString() {
        return type
    }
}
