package za.co.signio.vehicleinbox.controller

import grails.converters.JSON
import groovy.util.logging.Slf4j
import za.co.signio.model.Contactinfo;
import za.co.signio.model.Member
@Slf4j

public class UserDetailController {
	
	
	

	def getUserDetails(){
		// get the memberID.
		String memberID =  session["memberID"];
		
		// get the list of request fields.
		List<String> fields = request.JSON.fields;
		
		// Get the objects from the DB.
		Member member = Member.get(memberID);
		Contactinfo contact = member?.getContactinfo();	
		
		// build the response.
		Map<String, String> values = [:];
        if (member != null) {
            for(String field : fields){
                switch(field){
                    case "FirstName":
                        setIfNotNull(values, field, member?.getFirstName());
                        break;
                    case "LastName":
                        setIfNotNull(values, field, member?.getLastName());
                        break;
                    case "EmailAddress":
                        setIfNotNull(values, field, contact?.getEmail());
                        break;
                    case "TelOffice":
                        setIfNotNull(values, field, contact?.getTelOffice());
                        break;
                    default:
                        log.error("Unknown fieldRequest: " + field);
                        break;
                }
            }
        } else {
            values["FirstName"] = ""
            values["LastName"] = ""
            values["EmailAddress"] = ""
            values["TelOffice"] = ""
        }
		
		// render the value map.
		render values as JSON;
	}
	
	private void setIfNotNull(Map<String, String> map, key, value){
		if(value != null){
			map.put(key, value);
		}
	}
}
