<tr><td class="BGColourWhite" >
    %{--<form id="natisForm" action="">--}%
	<table style="width:98%" class="responsive BGColourDarkBlue">

		<tbody class="contactDetails"><tr class='BGColourBlue'><td colspan="4">Contact Details</td></tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td style="width:25%;"><label for="attention">For Attention at Dealership</label></td><td style="width:25%;"><input type="text" name="attention" style="width:96%"></td>
				<td colspan="2"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td><label for="telephoneNumber">Telephone Number</label></td><td><input type="text" name="telephoneNumber" style="width:96%"></td>
				<td colspan="2"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis'>
				<td><label for="cellphoneNumber">Cellphone Number</label></td><td><input type="text" name="cellphoneNumber" class="originalNatis" style="width:96%"></td>
				<td colspan="2"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td><label for="emailAddress">Email Address</label></td><td><input type="text" name="emailAddress" style="width:96%"></td>
				<td colspan="2"></td>
			</tr>
		</tbody>
		
		<tbody class="copyNatisDeliveryMethod"><tr class='BGColourBlue'><td colspan="4">Delivery Method</td></tr>
			<tr class='BGColourBlueTD'>
				<td style="width:25%;"><label for="copynatis_deliveryMethod">Method</label></td><td style="width:25%;"><select name="copynatis_deliveryMethod" style="width:100%">
					<option value="DISPLAY">Display</option>
					<option value="EMAIL">Email</option>
					<option value="BOTH">Both</option>
				</select></td>
				<td colspan="2"></td>
			</tr>
			<tr class='BGColourBlueTD copyNatisEmail'>
				<td><label for="deliveryEmail">DeliveryEmail</label></td><td><input type="text" name="deliveryEmail" style="width:98%"></td>
				<td colspan="2"></td>
			</tr>
		</tbody>
		
		<tbody class='existingVehicle'><tr class='BGColourBlue'><td colspan="4">Vehicle Details</td></tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td style="width:20%;">Vehicle VIN</td><td style="width:30%;"><input name="vinNumber" type="text" disabled="disabled" style="width:96%"></td>
				<td style="width:20%;">Register Number</td><td style="width:30%;"><input name="registerNumber" type="text" disabled="disabled" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td style="width:20%;">Vehicle Make</td><td style="width:30%;"><input name="make" type="text" disabled="disabled" style="width:96%"></td>
				<td style="width:20%;">Vehicle Model</td><td style="width:30%;"><input name="model" type="text" disabled="disabled" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis copyNatis'>
				<td style="width:20%;">Engine Number</td><td style="width:30%;"><input name="engineNumber" type="text" disabled="disabled"  style="width:96%"></td>
				<td style="width:20%;">License Number</td><td style="width:30%;"><input name="licenseNumber" type="text" disabled="disabled" style="width:96%"></td>
			</tr>
		</tbody>
		
		<tbody class='newVehicle'><tr class='BGColourBlue'><td colspan="4">Vehicle Details</td></tr>
			<tr class='BGColourBlueTD'>
				<td style="width:20%;">Vehicle Make</td><td style="width:30%;"><select name="make" style="width:96%"></select></td>
				<td style="width:20%;">Register Number</td><td style="width:30%;"><input name="registerNumber" type="text" disabled="disabled" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD'>
				<td>Vehicle Year</td><td><select name="vehicleYear" style="width:96%"></select></td>
				<td>Vehicle VIN</td><td><input name="vinNumber" type="text" disabled="disabled" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD'>
				<td>Vehicle Model</td><td><select name="model" style="width:96%"></select></td>
				<td>License Number</td><td><input name="licenseNumber" type="text" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD'>
				<td>Vehicle Colour</td><td><select name="vehicleColour" style="width:96%"></select></td>
				<td>Engine Number</td><td><input name="engineNumber" type="text" disabled="disabled"  style="width:96%"></td>
			</tr>
		</tbody>
		
		<tbody><tr class='BGColourBlue'><td colspan="4">Request Details</td></tr>
			<tr class='BGColourBlueTD originalNatis'>
				<td>Company / Owner Name</td><td><input name="ownerName" type="text" style="width:96%"></td>
				<td>Account Number</td><td><input name="accountNumber" type="text" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD copyNatis'>
				<td>Bank</td><td><select name="copynatis_bank" style="width:96%">
					%{--<option value="WESBANK">Wesbank</option>--}%
					%{--<option value="ABSA">ABSA</option>--}%
					%{--<option value="MFC">MFC</option>--}%
					%{--<option value="SBSA">Standard Bank</option>--}%
				</select></td>
				<td colspan="2"></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis'>
				<td>Bank</td><td><select name="originalnatis_bank" style="width:96%">
                    %{--<option value="-1">-select-</option>--}%
					%{--<option value="WESBANK">Wesbank</option>--}%
                    %{--<option value="SBSA">Standard Bank</option>--}%
				</select></td>
				<td>Dealer Stock Letter Required</td><td><select name="dealerLetterRequired" style="width:96%">
					<option value="true">YES</option>
					<option value="false">NO</option>
				</select></td>
			</tr>
			<tr class='BGColourBlueTD originalNatis'>
				<td>Comments</td><td><input name="comments" type="text" style="width:96%"></td>
				<td>Preferred Delivery Method</td><td><select name="originalnatis_deliveryMethod" style="width:96%">
					<option value="Courier">Courier</option>
					<option value="Speed Mail">Speed Mail</option>
					<option value="Collect">Collection</option>
				</select></td>
			</tr>
		</tbody>
		<tbody class="deliveryForm"><tr class='BGColourBlue'><td colspan="4">Delivery Details</td></tr>
			<tr class='BGColourBlueTD'>
				<td>Address Line 1</td><td><input name="addressLine1" type="text" style="width:96%"></td>
				<td>Address Line 2</td><td><input name="addressLine2" type="text" style="width:96%"></td>
			</tr>
			<tr class='BGColourBlueTD'>
				<td>City</td><td><select name="city" style="width:100%"></select></td>
				<td>Suburb</td><td><select name="suburb" style="width:100%"><option value='-1'>-select city first-</option></select></td>
			</tr>
			<tr class='BGColourBlueTD'>
				<td>Postal Code</td><td><select name="postalCode" style="width:100%"><option value='-1'>-select suburb first-</option></select></td>
				<td colspan="2"></td>
			</tr>
		</tbody>
		<tbody class="fileUploadForm"><tr class='BGColourBlue'><td colspan="4">Required Document Details</td></tr>
			<tr><td colspan="4" class="BGColourBlueTD"><table>
				<tr class='BGColourBlueTD'>
					<td>POP</td><td>Proof of Payment</td>
					<td>
						<input type="button" id="document_pop" name="document_pop" value="Browse...">
						&nbsp;<div style="display:inline" id="document_pop_label">No File Selected</div>
						<input type="hidden" id="document_pop_uploaded" name="document_pop_uploaded" value="false">
					</td>
				</tr>
				<tr class='BGColourBlueTD'>
					<td>SETL</td><td>Settlement Letter</td>
					<td>
						<input type="button" id="document_setl" name="document_setl" value="Browse...">
						&nbsp;<div style="display:inline" id="document_setl_label">No File Selected</div>
						<input type="hidden" id="document_setl_uploaded" name="document_setl_uploaded" value="false">
					</td>
				</tr>
			</table></td></tr>
		</tbody>
		<tbody class="originalNatis"><tr class='BGColourBlue'><td colspan="4">Consent Details</td></tr>
			<tr class='BGColourBlueTD'>
				<td colspan="4">
					<p><b>Terms And Conditions</b><br>By submitting any requests you confirm and agree that:<br><ul style="margin-left:16px;">
					<li>You have permission to act on behalf of the dealership and the customer.</li>
					<li>You have obtained written consent from the customer in order to facilitate the selected request/s on behalf of the dealership.</li>
					<li>The written consent form is held on file and must be presented by you for dispute and audit purposes when requested.</li>
					</ul><br>
					I accept the terms and conditions.<input type="checkbox" name="concent" value="true">
 				</td></p>
			</tr>
		</tbody>
	</table>
%{--</form>--}%
</td></tr>

<script type="text/javascript">
	
	$(document).ready(function(){
		// declare some fields.
		var copyNatisBank = $("[name='copynatis_bank']");
        var originalNatisBank = $("[name='originalnatis_bank']");

		// check the deal ID.
		if(signio.vehicleInbox.dealID == null || signio.vehicleInbox.dealID == "-1"){
			// hide the existing vechicle section.
			$(".existingVehicle").hide();

			// lock the Copy natis selection
			$("#natisRequestTypeSelection").attr("disabled", "disabled");

			// set the model field.
			var selectYear = function(){
				var options = "<option value='-1'>-select-</option>";
				var makeID = $(".newVehicle").find("[name='make']").val();
				var year = $(".newVehicle").find("[name='vehicleYear']").val();
				$.each(signio.vehicleInbox.callAjax("../../vehicleDetailUtil/getVehicleModels?id=" + makeID + "&year=" + year), function(index, item){
					options += "<option value='" + item[0]  + "'>" + item[1] + "</option>";
				});
				$(".newVehicle").find("[name='model']").find("option").remove().end().append(options);
			};

			$(".newVehicle").find("[name='vehicleYear']").change(selectYear);

			// set the values for the vehicle
			$(".newVehicle").find("[name='vinNumber']").val("${request.getParameter('vinNumber')?.trim()}");
			$(".newVehicle").find("[name='engineNumber']").val("${request.getParameter('engineNumber')?.trim()}");
			$(".newVehicle").find("[name='registerNumber']").val("${request.getParameter('registerNumber')?.trim()}");

			// if the bank was set, we need to lock the bank.
			if(${request.getParameter('bank') != null}){
                var bankOptions = "";
                //Populate CopyNatisBank
                var bankListSize = signio.vehicleInbox.callAjax("../getAvailableBanks/?id="+signio.vehicleInbox.dealID+"&bank=${request.getParameter('bank')}");

                $.each(bankListSize, function(index, item){
                    bankOptions += "<option value='" + index  + "'>" + item + "</option>";
                });
                copyNatisBank.find("option").remove().end().append(bankOptions);
				$("#returnToVehicleInboxButton").hide();
			}
			
			// get the user details and prepopulate them.
			var userDetails = {};
			if(${params.id == null || params.id == '-1'}){
				var fields = ["FirstName", "LastName", "EmailAddress", "TelOffice"];
				userDetails = signio.vehicleInbox.callAjax("../../userDetail/getUserDetails", "POST", {fields:fields});
			}

			$(".contactDetails").find("[name='attention']").val(userDetails.FirstName + " " + userDetails.LastName);
            var telephoneNo = "";
            if (userDetails.TelOffice != undefined) {telephoneNo = userDetails.TelOffice.replace(/\D/g,'');}
			$(".contactDetails").find("[name='telephoneNumber']").val(telephoneNo);
			$(".contactDetails").find("[name='emailAddress']").val(userDetails.EmailAddress);	
		}
		else{
			$(".newVehicle").hide();
			// if this an existing vehicle, then fetch the vehicle details.
			var natisData = signio.vehicleInbox.callAjax("../getNatisData/" + signio.vehicleInbox.dealID);
            $.each($(".contactDetails").find(":input"), function(index, input){
                input = $(input);
                if(natisData != null && natisData[input.attr("name")] != null){
                    input.val(natisData[input.attr("name")]);
                }
            });
			$.each($(".existingVehicle").find(":input"), function(index, input){
				input = $(input);
				if(natisData != null && natisData[input.attr("name")] != null){
					input.val(natisData[input.attr("name")]);
				}
			});
            var options = "<option value='-1'>-select-</option>";
            //Populate CopyNatisBank
            $.each(signio.vehicleInbox.callAjax("../getAvailableBanks/" + signio.vehicleInbox.dealID), function(index, item){
                options += "<option value='" + index  + "'>" + item + "</option>";
            });
            copyNatisBank.find("option").remove().end().append(options);
            var origNatisOptions = "<option value='-1'>-select-</option>";
            //Populate CopyNatisBank
            $.each(signio.vehicleInbox.callAjax("../getAvailableBanks?id="+signio.vehicleInbox.dealID+"&natisType=original"), function(index, item){
                origNatisOptions += "<option value='" + index  + "'>" + item + "</option>";
            });
            originalNatisBank.find("option").remove().end().append(origNatisOptions);
		}
	});
</script>
