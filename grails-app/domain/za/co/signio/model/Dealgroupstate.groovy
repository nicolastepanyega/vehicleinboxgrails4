package za.co.signio.model


class Dealgroupstate {

    String state
    Integer stateCode
    String description

    static hasMany = [dealGroup:Dealgroup]

    static mapping = {
        version false
    }

    static constraints = {
        state maxSize: 45, nullable: false
        description nullable: true, maxSize: 45
        stateCode(nullable: false)
    }

    String toString() {
        return state
    }

}
