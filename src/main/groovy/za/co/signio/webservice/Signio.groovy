package za.co.signio.webservice

interface Signio extends WebServiceProvider{

	public static enum DUMMY_SIGNIO implements WebServiceProvider, WebServiceAction{
		DUMMY_ACTION
	}
	
	public static enum SUPPORTING_DOCS implements WebServiceProvider, WebServiceAction{
		CREATEBATCH;
	}
}
