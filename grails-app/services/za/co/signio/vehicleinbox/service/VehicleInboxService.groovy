package za.co.signio.vehicleinbox.service

import groovy.util.logging.Slf4j
import za.co.signio.model.Deal
import za.co.signio.dataModel.SignioDataModel
import org.hibernate.Criteria
@Slf4j
class VehicleInboxService {
   

    def getVehicleInboxList(def params, def session) {

        // first, build a pull a list of the deals.
        Map<Long, Map<String, Object>> vehicles = new LinkedHashMap<Long, Map<String, Object>>();

        params.offset = params.int('offset') ?: 0
        params.max = Math.min(params.max ? params.int('max') : 15, 100)

        final String searchRequest = params.searchRequest;

        log.info("Offset: " + params + "*****session: " + session.countVehicle)

        log.info("Full session: " + session)

        for (Deal deal : Deal.createCriteria().list(max: params.max, offset: params.offset) {
            createAlias('dealgroup', 'dealgroup', Criteria.LEFT_JOIN)
            createAlias('dealgroup.institution', 'institution', Criteria.LEFT_JOIN)
            dealtype {
                eq("type", "VehicleInbox")
            }
            dealstate {
                eq("state", "Vehicle Inbox")
            }

            if(searchRequest != null && !"".equals((searchRequest).trim())) {
                createAlias('dealgroup.clientdetail', 'clientdetail', Criteria.LEFT_JOIN)
                createAlias('dealgroup.companydetail', 'companydetail', Criteria.LEFT_JOIN)
                or {
                    for (String searchWord : searchRequest.split('-|\\.|\\s|,')){
                        log.info("Search word: ${searchWord}")
                        ilike("clientdetail.firstName", "%${searchWord.trim()}%")
                        ilike("clientdetail.lastName", "%${searchWord.trim()}%")
                        ilike("clientdetail.idNumber", "%${searchWord.trim()}%")
                        ilike("companydetail.companyName", "%${searchWord.trim()}%")
                        ilike("companydetail.registrationNumber", "%${searchWord.trim()}%")
                    }
                }
            }

            eq("institution.id", session["institutionID"])
            order("dateUpdated", "desc")

        }) {

            SignioDataModel sdm = deal.getSignioDataModel("originalXml");

            String clientDetails = ""
            String idNo = ""

            if (deal == null) {
                log.info("Deal is null.")
            } else if (deal.getDealgroup() == null) {
                log.info("Deal group is null. ${deal.getId()}")
            } else if(deal.getDealgroup().getClientdetail() != null) {
                clientDetails = "${deal.getDealgroup().getClientdetail().getFirstName()} ${deal.getDealgroup().getClientdetail().getLastName()}";
                idNo = deal.getDealgroup().getClientdetail().getIdNumber()
            } else if (deal.getDealgroup().getCompanydetail() != null) {
                clientDetails = deal.getDealgroup().getCompanydetail().getCompanyName();
                idNo = deal.getDealgroup().getCompanydetail().getRegistrationNumber()
            }

            Date date = (deal.dateUpdated == null)? deal.dateCreated : deal.dateUpdated;

            // build the vehicle record.
            vehicles.put(deal.id, [
                    id: deal.id,
                    canedit: true,
                    creationDate: date.format("yyyy-MM-dd HH:mm:ss"),
                    clientDetails: clientDetails,
                    idNo: idNo,
                    make: deal.getVehicle()?.getVehiclemanufacturer()?.getName(),
                    model: deal.getVehicle()?.getDescription(),
                    year: sdm.fields.yearOfFirstRegistration?.value,
                    //colour: sdm.fields.articleColour?.value,
                    vin_number: sdm.fields.vinNumber?.value,
//                    vvi: [
//                            text: "VVI Website",
//                            link: "http://auto.test.lightstone.co.za/"],
                    copyNatis: [
                            text: "Not Requested",
                            link: "../natis/index/" + deal.id + "?natisType=copy"],
                    originalNatis: [
                            text: "Not Requested",
                            link: "../natis/index/" + deal.id + "?natisType=original"],
                    regTrack: [
                            text: "Not Requested",
                    ]
            ]);

            // get the children of the deal...
            for (Deal childDeal : Deal.createCriteria().list {
                dealtype { 'in'("type", ["CopyNatis", "OriginalNatis", "RegTrack"]) }
                dealstate {
                    'in'("state",
                            [
                            "Copy Natis - Not Requested", "Copy Natis - Failed", "Copy Natis - Received",
                            "Original Natis - Not Requested", "Original Natis - Requested", "Original Natis - Failed", "Original Natis - Received",
                            "RegTrack - Not Requested", "RegTrack - Requested", "RegTrack - Received", "RegTrack - Failed"

                            ]
                    )
                }
                eq('parentId', (Integer) deal.id)
                order("dateCreated", "asc")
            }) {
                Map<String, Object> vehicle = vehicles.remove(deal.id);
                switch (childDeal.getDealtype().getType()) {
                // HANDLE COPYNATIS DEALS>
                    case "CopyNatis":
                        switch (childDeal.getDealstate().getState()) {
                            case "Copy Natis - Failed":
                                vehicle.put("copyNatis", [text: "Request Failed", message: childDeal.comment, link: "../natis/index/" + deal.id + "?natisType=copy"]);
                                break;
                            case "Copy Natis - Received":
                                vehicle.put("copyNatis", [text: "View CopyNatis", link: "../natis/view/" + childDeal.id])
                                break;
                        };
                        break;

                // HANDLE ORIGINAL NATIS DEALS
                    case "OriginalNatis":
                        switch (childDeal.getDealstate().getState()) {
                        //VAF-8470 PdvW Adding links on to the deals so that the request can be redone.
                            case "Original Natis - Requested":
                                vehicle.put("originalNatis", [text: "Requested", message: ("Reference Number: " + childDeal.getBatchRef() + "\nStatus: " + childDeal.comment), link: "../natis/index/" + deal.id + "?natisType=original"])
                                break;
                            case "Original Natis - Failed":
                                vehicle.put("originalNatis", [text: "Failed", message: childDeal.comment, link: "../natis/index/" + deal.id + "?natisType=original"])
                                break;
                        //VAF-8470 PdvW Adding links on to the deals so that the request can be redone.
                            case "Original Natis - Received":
                                vehicle.put("originalNatis", [text: "Request Received", message: childDeal.comment, link: "../natis/index/" + deal.id + "?natisType=original"])
                                break;
                        };
                        break;

                // HANDLE REGTRACK DEALS>
                    case "RegTrack":
                        switch (childDeal.getDealstate().getState()) {
                            case "RegTrack - Requested":
                                vehicle.put("regTrack", [text: "RegTrack Requested", link: "javascript:showStatusMessage($childDeal.id);"]);
                                break;
                            case "RegTrack - Received":
                                vehicle.put("regTrack", [text: "RegTrack request complete. ALV Form received.", link: "../regtrack/view/" + childDeal.id]);
                                break;
                            case "RegTrack - Failed":
                                vehicle.put("regTrack", [text: "RegTrack Failed.", link: "javascript:showStatusMessage($childDeal.id);"]);
                                break;
                        };
                        break;
                    default:
                        log.error("unhandled dealtype: " + deal.getDealtype());
                        break;
                };
                vehicles.put(deal.id, vehicle);
            }
        };

        //render new LinkedList<Map<String, Object>>(vehicles.values()) as JSON;
         log.info("session : ${session["institutionID"]}")
        def listVehicle = Deal.createCriteria().list {
            createAlias('dealgroup', 'dealgroup', Criteria.LEFT_JOIN)
            createAlias('dealgroup.institution', 'institution', Criteria.LEFT_JOIN)
            dealtype {
                eq("type", "VehicleInbox")
            }
            dealstate {
                eq("state", "Vehicle Inbox")
            }

            if(searchRequest != null && !"".equals((searchRequest).trim())) {
                createAlias('dealgroup.clientdetail', 'clientdetail', Criteria.LEFT_JOIN)
                createAlias('dealgroup.companydetail', 'companydetail', Criteria.LEFT_JOIN)
                or {
                    for (String searchWord : searchRequest.split('-|\\.|\\s|,')){
                        log.info("Search word: ${searchWord}")
                        ilike("clientdetail.firstName", "%${searchWord.trim()}%")
                        ilike("clientdetail.lastName", "%${searchWord.trim()}%")
                        ilike("clientdetail.idNumber", "%${searchWord.trim()}%")
                        ilike("companydetail.companyName", "%${searchWord.trim()}%")
                        ilike("companydetail.registrationNumber", "%${searchWord.trim()}%")
                    }
                }
            }

            eq("institution.id", session["institutionID"])
            order("dealgroup.dateCreated", "desc")
            projections {
                property("id", "id")
            }
        }


        log.info("size:" + listVehicle.size())

        [vehicles: vehicles, vehicleCount: listVehicle.size(), searchRequest: ((searchRequest == null)? "" : searchRequest)]

    }
}
