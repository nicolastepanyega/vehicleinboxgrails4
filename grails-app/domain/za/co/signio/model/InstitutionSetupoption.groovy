package za.co.signio.model

class InstitutionSetupoption {

    Institution institution
    Setupoption setupoption
    String value

    static belongsTo = [Setupoption, Institution]

    static constraints = {
        value nullable: false, maxSize: 255
        setupoption nullable: false
        institution nullable: false
    }

    static mapping = {
        version(false)
    }

    public String toString(){
        return value
    }
}
