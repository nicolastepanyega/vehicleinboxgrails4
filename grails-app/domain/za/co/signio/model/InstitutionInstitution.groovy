package za.co.signio.model

class InstitutionInstitution {

    Institution institution1 //Bank
    Institution institution2 //Dealerships
    String merchantId
    Boolean enabled
    String thirdPartyId
    Classification classification
    CrmProfile crmProfile
    Boolean isDefault

    Institutiontype institutiontype

    String toString() { /* WSB-488 VAPs Section 13 split payee lookup (Coert) */
        return institution1.name + " - " + institution2.name
    }

    static belongsTo = [Institution, Institutiontype, Classification, CrmProfile]

    static hasMany = [institutionjointventures: Institutionjointventure, deals:Deal, auxReferencecodeconfigs: AuxReferencecodeconfig, auxProductcategorymapping: AuxProductcategorymapping, igfcosts: Igfcost]

    static mapping = {
        version false
        thirdPartyId column: '3rd_party_id'
        institution1 lazy: false
        institution2 lazy: false
    }

    static constraints = {
        merchantId nullable: true, maxSize: 45
        enabled nullable: true
        thirdPartyId nullable: true, maxSize: 45
        institution1(nullable: false)
        institution2(nullable: false)
        institutiontype(nullable: true)
        classification(nullable: true)
        crmProfile(nullable: true)
        isDefault(nullable: true)
    }
}

