package za.co.signio.model

class Lookupdata {

    Clientdetail clientdetail
    Companydetail companydetail
    String data
    Date dateUpdated

    static constraints = {
        clientdetail nullable: true
        companydetail nullable: true
        data nullable: false
        dateUpdated nullable: false
    }

    static mapping = {
        version false
    }
}
