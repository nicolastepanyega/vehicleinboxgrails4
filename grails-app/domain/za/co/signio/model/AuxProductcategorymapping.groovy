package za.co.signio.model

/* WSB-488 VAPs Section 13 split payee lookup (Coert) */

class AuxProductcategorymapping {

    String categoryCode
    String adminCode
    String splitPremiumCode
    String splitPremiumName
    InstitutionInstitution institutionInstitution
    AuxProductcategory auxProductcategory

    static hasOne = [auxProductcategory: AuxProductcategory, institutionInstitution: InstitutionInstitution]

    static constraints = {
        categoryCode(blank: false)
        adminCode(blank: false)
        splitPremiumCode(blank: false)
        splitPremiumName(blank: false)
        institutionInstitution validator: { val, obj -> val.institutiontype.type.equals("SUPPLIERIGF") }
    }

    static mapping = {
        version(false)
    }
}
