package za.co.signio.model

import za.co.signio.model.Vehiclemanufacturer

class Vehicle {

    String mmCode
    String carId
    String description
    Date introductionDate
    Date discontinuedDate
    Integer registrationStartYear
    Integer registrationEndYear
    Vehiclemanufacturer vehiclemanufacturer

    static belongsTo = [Vehiclemanufacturer, Institution, Vehicleoptionalextra]
    static hasMany = [deals:Deal, vehicleDetails:Vehicledetail, institutions:Institution, lightstoneVehicleLookups:Lightstonevehiclelookup, vehicleOptionalExtras:Vehicleoptionalextra]

    static mapping = {
        version false
        institutions joinTable: 'institution_vehicle'
        vehicleOptionalExtras joinTable: 'vehicle_vehicleoptionalextra'
    }

    static constraints = {
        mmCode nullable: true, maxSize: 45
        carId nullable: true, maxSize: 45
        description nullable: true, maxSize: 255
        introductionDate nullable: true
        discontinuedDate nullable: true
        registrationStartYear nullable: true
        registrationEndYear nullable: true
        vehiclemanufacturer(nullable: false)
    }

    String toString() {
        return description
    }
}
