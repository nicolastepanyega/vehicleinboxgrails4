package za.co.signio.model


class Title{

    String description

    static hasMany = [members:Member, clientDetails:Clientdetail]
    static mapping = {
        version false
    }

    static constraints = {
        description nullable: false, maxSize: 45
    }

    String toString() {
        return description
    }
}
