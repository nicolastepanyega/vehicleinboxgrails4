// New validation Rules.
$(document).ready(function(){
	$.validator.addMethod("notEqual", function(value, element, param) {
		return this.optional(element) || value !== param;
	}, "Please choose a value!");
	
	$.validator.addMethod("match", function(value, element, param) {
		return this.optional(element) || value.match(param) !== null;
	}, "Please choose a value!");
	
	$.validator.addMethod("notMatch", function(value, element, param) {
		return this.optional(element) || value.match(param) == null;
	}, "Please choose a value!");
	
	$.validator.addMethod("checked", function(value, element, param) {
		var e = $(element);
		if(e.attr("type") == "checkbox"){
			return (e.prop("checked") == param);
		}
		return true;
	}, "Please Check the Box");
	
	// FORM VALIDATION	
	$("#natisForm").validate({
		rules: {
			attention: { required: true, minlength: 4 },
//			telephoneNumber: {required: true, digits: true, minlength:10, maxlength:10, notMatch:"^0(60|710|711|712|713|714|715|716|717|718|719|72|73|74|741|76|78|79|811|812|813|814|82|83|84)[0-9]+"},
            telephoneNumber: {required: true, digits: true, minlength:10, maxlength:10},
			cellphoneNumber: {required: true, digits: true, minlength:10, maxlength:10, match:"^0(60|710|711|712|713|714|715|716|717|718|719|72|73|74|741|76|78|79|811|812|813|814|82|83|84)[0-9]+"},
			emailAddress : {required: true, match:"^[0-9a-zA-Z._%+-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,4}$"},
			deliveryEmail  : {required: true, match:"^[0-9a-zA-Z._%+-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,4}$"},
			registrationNumber : {required: true},
			vinNumber : {required: true},
			make : { required: true, notEqual:"-1"},
			model : { required: true, notEqual:"-1"},
			vehicleYear : { required: true, notEqual:"-1"},
			engineNumber : {},
			ownerName : {required: true, minlength: 4, maxlength:50},
			accountNumber : {required: true,  number:true,
                minlength: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 11
                    } else if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 12
                    } else return 12;
                },
                maxlength:function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 11
                    } else if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 12
                    } else return 12;
                },
                match: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return '^(85|86|87)[0-9]+'
                    } else {
                        return '^[0-9]*$'
                    }
                }
            },
			referenceNumber : {},
			paidUpLetterRequired : {},
			copynatis_bank : {required: true, notEqual:"-1"},
			originalnatis_bank : {required: true, notEqual:"-1"},
			branch : {},
			comments : {},
			deliveryMethod : {required: true},
			addressLine1 : {required: true},
			addressLine2 : {required: true},
			city : {required: true, notEqual:"-1"},
			suburb : {required: true, notEqual:"-1"},
			postalCode : {required: true, notEqual:"-1"},
			concent : {checked : true },
			document_pop_uploaded:{
                required:function(element) {
                    return $("[name='originalnatis_bank']").val() == 'WESBANK';
                },
                match:function(element){
                    if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'true'
                    } else {
                        return $("#document_pop_uploaded").val()
                    }
                }
            },
			document_setl_uploaded:{
                required:function(element) {
                    return $("[name='originalnatis_bank']").val() == 'WESBANK';
                },
                match:function(element){
                    if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'true'
                    } else {
                        return $("#document_setl_uploaded").val()
                    }
                }
            }
		},
		messages:{
			attention: { required: "Name Required", minlength: $.format("Name needs to be at least {0} characters") },
			telephoneNumber: {required: "Phone Number Required", notMatch:"Cellphone number entered"},
			cellphoneNumber: {required: "Cellphone Number Required",  match:"Landline number entered"},
			accountNumber: {
                match: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 'Please enter a 12-digit value.'
                    } else if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'Please enter a valid Wesbank account number (11-digits value that starts with 85, 86 or 87).'
                    } else {
                        return 'Please select a Bank.'
                    }
                },
                required: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 'Please enter a 12-digit value.'
                    } else if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'Please enter a valid Wesbank account number (11-digits value that starts with 85, 86 or 87).'
                    } else {
                        return 'Please select a Bank.'
                    }
                },
                minlength: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 'Please enter a 12-digit value.'
                    } else if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'Please enter a valid Wesbank account number (11-digits value that starts with 85, 86 or 87).'
                    } else {
                        return 'Please select a Bank.'
                    }
                },
                maxlength: function(element) {
                    if ($("[name='originalnatis_bank']").val() == 'SBSA') {
                        return 'Please enter a 12-digit value.'
                    } else if ($("[name='originalnatis_bank']").val() == 'WESBANK') {
                        return 'Please enter a valid Wesbank account number (11-digits value that starts with 85, 86 or 87).'
                    } else {
                        return 'Please select a Bank.'
                    }
                }
            },
			emailAddress: {match:"Enter a Valid Email Address"},
			deliveryEmail: {match:"Enter a Valid Email Address"},
			copynatis_bank: { notEqual:"Please Select a valid bank"},
			originalnatis_bank: { notEqual:"Please specify a bank"},
			concent: {checked: "Please Check the Consent Indicator"},
			document_pop_uploaded: {match:"Please upload a file"},
			document_setl_uploaded: {match:"Please upload a file"}
		}
	});
});