package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/27
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
class Accreditation {
    
    String name
    Accreditationtype accreditationtype

    static belongsTo = [Accreditationtype]
    static hasOne = [parent: Accreditation]
    static hasMany = [memberAccreditations:MemberAccreditation, children: Accreditation]

    static constraints = {
        name nullable: false, maxSize: 45
        accreditationtype nullable: false
        parent nullable: true
    }

    static mappedBy = [children: 'parent']

    static mapping = {
        version(false)
    }

    public String toString(){
        return(name)
    }
}
