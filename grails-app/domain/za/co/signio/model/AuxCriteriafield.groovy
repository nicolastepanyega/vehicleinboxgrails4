package za.co.signio.model

class AuxCriteriafield {

    String label

    static hasOne = [formfield:Formfield]

    static belongsTo = [Formfield]
    static hasMany = [auxCriteriasetups:AuxCriteriasetup]

    static constraints = {
        formfield nullable: false
        label(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }

    public String toString(){
        formfield?.name
    }
}
