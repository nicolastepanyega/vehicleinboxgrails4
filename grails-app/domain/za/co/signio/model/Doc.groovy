package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 4/23/12
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
class Doc {

    Deal deal
    Formfield formfield
    String value

    static belongsTo = [Deal, Formfield]

    static constraints = {
        deal nullable: false
        formfield(nullable: false)
        value(nullable: false)
    }

    static mapping = {
        version false
    }
}
