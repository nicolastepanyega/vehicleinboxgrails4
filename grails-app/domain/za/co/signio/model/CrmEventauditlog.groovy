package za.co.signio.model

class CrmEventauditlog {

    Date dateCreated
    Member member
    Institution institution
    Status status
    CrmEventlogtype crmEventlogtype
    CrmSharedrelationship crmSharedrelationship
    CrmEventlog crmEventlog

    static belongsTo = [CrmEventlogtype, Member, Status, Institution, CrmEventlog, CrmSharedrelationship]

    static constraints = {
        dateCreated(nullable: false)
        member(nullable: false)
        institution(nullable: false)
        status(nullable: false)
        crmEventlogtype(nullable: false)
        crmEventlog(nullable: false)
        crmSharedrelationship(nullable: true)
    }

    static mapping = {
        version(false)
    }

}
