package za.co.signio.model

class BankvapsPremiums {

    String type
    String field
    String value

    static hasOne = [bankvaps:Bankvaps]
    static belongsTo = [Bankvaps]

    static constraints = {
        bankvaps(nullable: false)
        type(nullable: false, maxSize: 45)
        field(nullable: true, maxSize: 45)
        value(nullable: false)
    }

    static mapping = {
        version false;
    }

}
