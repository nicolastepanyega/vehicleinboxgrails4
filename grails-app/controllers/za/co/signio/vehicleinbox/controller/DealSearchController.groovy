package za.co.signio.vehicleinbox.controller

import grails.converters.JSON
import groovy.util.logging.Slf4j

@Slf4j

class DealSearchController {


	
	def findDealsByClientIDNumber(){
		log.info("Search For ID: " + params.id);
		
		List<Map<String, String>> result = [[dealID:"ABC", vin:"111"],[dealID:"PQR", vin: "123"],[dealID:"XYZ", vin: "999"]]; 
		render result as JSON;
	}
}
