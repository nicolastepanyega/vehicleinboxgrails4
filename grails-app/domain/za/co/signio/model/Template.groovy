package za.co.signio.model

class Template {
    String name
    String templateName
    String uri
    String comment
    String relationship

    static mapping = {
        version false
    }
}
