package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 12/6/12
 * Time: 12:11 PM
 * To change this template use File | Settings | File Templates.
 */
class ReportInstitution implements Serializable {

    Report report
    Institution institution
    Boolean ssoIndicator
    Boolean premiumSubscription
    Date dateSubscription

    static belongsTo = [Report, Institution]

    static constraints = {
        report(nullable: false)
        institution(nullable: false)
        ssoIndicator(nullable: false)
        premiumSubscription(nullable: false)
        dateSubscription(nullable: false)
    }

    static mapping = {
        version false
        id composite: ['report', 'institution']
    }

}
