package za.co.signio.model


class Province {

    String name
    Country country

    static hasMany = [cities: City]
    static belongsTo = [Country]

    static mapping = {
        version false
    }

    static constraints = {
        name maxSize: 45, nullable: false
        country(nullable: false)
    }

    String toString() {
        return name
    }
}
