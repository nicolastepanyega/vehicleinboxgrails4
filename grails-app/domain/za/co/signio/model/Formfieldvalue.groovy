package za.co.signio.model


class Formfieldvalue {

    String value
    Formfield formfield

    static belongsTo = [Formfield, Fieldvaluemappingfilter]
    static hasMany = [formFieldCodes:Formfieldcode, value: Fieldvaluemappingfilter, valueMapping: Fieldvaluemappingfilter]

    static mappedBy = [value: 'formfieldvalue1', valueMapping: 'formfieldvalue2']

    static mapping = {
        version false
    }

    static constraints = {
        value maxSize: 255, nullable: false
        formfield(nullable: false)
    }

    String toString() {
        return value
    }
}
