package za.co.signio.model


class DealstateParent {

    String comment

    //Dealstate dealstate
    //static hasOne = [dealstate:Dealstate]
    static hasMany = [dealstates: Dealstate]

    static belongsTo = [dealstate:Dealstate]


    static constraints = {
        comment nullable: true
    }

    static mapping = {
        version false
        table('dealstate_parent')
        dealstates joinTable: 'dealstate_parent_dealstate'
    }

}
