package za.co.signio.model

class Member {

    transient springSecurityService

    String firstName
    String middleName
    String lastName
    String idNumber
    Identificationtype identificationtype
    String passportNumber
    Date dateCreated
    Date dateUpdated
    Member createdByMember
    Boolean enabled
    Boolean accountExpired
    Boolean accountLocked
    String username
    Date passwordDateUpdated
    String password
    Boolean passwordExpired
    String authToken
    Boolean isUnderSupervision
    Byte failedLoginAttempts
    Boolean hasSigningRights

    static belongsTo = [Suburb, Contactinfo, Title, Membergroup, Role, Notification, Identificationtype, Note]
    static hasOne = [status:Status, contactinfo:Contactinfo, parent:Member, title:Title, suburb: Suburb]
    static hasMany = [children: Member,
                      certificate: Certificate,
                      memberGroups: Membergroup,
                      relationships: Relationship,
                      memberReports: MemberReport,
                      supportingDocs: Supportingdocs,
                      profiledocuments: Profiledocument,
                      roles: Role,
                      notifications: Notification,
                      consultants: Consultantlog,
                      fAndIs: Consultantlog,
                      dealGroups:Dealgroup,
                      deals:Deal,
                      formFields:Formfield,
                      processingCentreAreas:Processingcentrearea,
                      memberInterests: Memberinterest,
                      vehicleDetails:Vehicledetail,
                      consultantCommissions:Consultantcommission,
                      retentions:Retention,
                      crmEventLogs:CrmEventlog,
                      crmSharedRelationships:CrmSharedrelationship,
                      crmEmailAuditLogs:CrmEmailauditlog,
                      assetAllocateds:Assetallocated,
                      memberAccreditations:MemberAccreditation,
                      otps:Otp,
                      memberPreferences:MemberPreference,
                      notes:Note,
                      relationshipmoduleconfigs:Relationshipmoduleconfig,
                      messagecontents: Messagecontent,
                      createdMessages: Message,
                      endedMessages: Message,
                      consultantMessages: Message,
                      dealdocuments:Dealdocument,
                      qualifications: Qualification,
                      memberCompliances:MemberCompliance
                    ]

    static auditable = [ignore: ['version', 'lastUpdated', 'contactinfo', 'dateUpdated']]

    static mappedBy = [children: 'parent', consultants: 'member1', fAndIs: 'member2', createdMessages: 'createdByMember', endedMessages: 'endedByMember', consultantMessages: 'consultant']

    static mapping = {
        version false
        memberGroups joinTable: 'membergroup_members'
        notifications joinTable: 'member_notification'
        roles(joinTable: 'member_role', lazy: false)
        formFields joinTable: 'member_formfield'
        processingCentreAreas joinTable: 'member_processingcentrearea'
        password column: '`password`'
        failedLoginAttempts defaultValue: 0
        createdByMember column: "created_by_member"
    }

    static constraints = {
        firstName maxSize: 45, nullable: false
        middleName nullable: true, maxSize: 45
        lastName maxSize: 45, nullable: false
        idNumber unique: true, nullable: true, maxSize: 25
        identificationtype(nullable: true)
        passportNumber unique: true, nullable: true, maxSize: 25
        dateCreated nullable: false
        dateUpdated nullable: true
        enabled nullable: false
        accountExpired nullable: false
        accountLocked nullable: true
        parent(nullable: true, blank:true)
        title(nullable: false)
        contactinfo(nullable: false)
        status(nullable: false)
        username(nullable: true, maxSize: 45, unique: true, blank: false)
        password(nullable: true)
        passwordExpired(nullable: true)
        authToken(nullable: true,  maxSize: 36)
        isUnderSupervision(nullable: true)
        failedLoginAttempts(nullable: true)
        hasSigningRights(nullable: true)
        createdByMember(nullable: true)
        suburb(nullable: true)
        passwordDateUpdated(nullable: true)
    }

    String toString() {
        return this.firstName + ' ' + this.lastName
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateCreated field in the object before insert.
     */
    def beforeInsert = {
        dateCreated = new Date()
        dateUpdated = dateCreated
        if(password!=null){
            encodePassword()
        }
        if (identificationtype == null) {
            identificationtype = Identificationtype.findById(1)
        }
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateUpdated field in the object before update.
     */
    def beforeUpdate = {
        dateUpdated = new Date()
// The isDirty "if" is commented out as this causes the update profile action to reset passwords in SignioBI to update the new selected password twice.
//        if (isDirty('password')) {
//           encodePassword()
//        }
    }

    /**
     * @see za.co.signio.model.Role
     * @return Set < Role >
     */
    Set<Role> getAuthorities() {
        MemberRole.findAllByMember(this).collect { it.role } as Set
    }

    Set<Relationshiptype> getAbsaDrmAuthorities() {

        List<Institution> institutionList=[]

        institutionList.add(Institution.findById(8321))
        institutionList.add(Institution.findById(41))

        List<Relationshiptype> compulsoryRelationshipList=[]
        compulsoryRelationshipList.add(Relationshiptype.findById(7))
        compulsoryRelationshipList.add(Relationshiptype.findById(24))


        List<Relationshiptype> relationshiptypeList=[]
        relationshiptypeList.add(Relationshiptype.findByAuthority('ROLE_MARKETER'))
        relationshiptypeList.add(Relationshiptype.findByAuthority('ROLE_MIS_SUPER_USER'))
        relationshiptypeList.add(Relationshiptype.findByAuthority('ROLE_PORTFOLIO_MANAGER'))
        relationshiptypeList.add(Relationshiptype.findByAuthority('ROLE_BANK_EMPLOYEE'))
        relationshiptypeList.add(Relationshiptype.findByAuthority('ROLE_DEALER_BANK_MIS_USER'))

        if(Relationship.findAllByInstitutionInListAndRelationshiptypeInListAndStatusAndMember(institutionList,compulsoryRelationshipList,Status.findById(2),Member.findById(this.id)).size() > 0){
            return Relationship.findAllByMemberAndInstitutionInListAndRelationshiptypeInListAndStatus(this,institutionList,relationshiptypeList,Status.findById(2)).collect { it.relationshiptype } as Set
        }
        else{
            List<Relationshiptype> emptyRelationshiptypeList=[]
            return emptyRelationshiptypeList as Set
        }
    }

    /**
     * This method encodes password.
     */
    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }

}
