<tbody class='clientDetail'><tr class='BGColourBlue'><td colspan="4">Vehicle Details</td></tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">VIN Number</td>
    <td style="width:30%;">
        <input name="vinNumber" id="vinNumber" type="text" disabled="disabled" style="width:96%" value="${vinNumber}">
    </td>
    <td style="width:20%;">Registration Number</td>
    <td style="width:30%;">
        <input name="registrationNumber" id="registrationNumber" type="text" disabled="disabled" style="width:96%" value="${registrationNumber}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">MMCode</td>
    <td style="width:30%;">
        <input name="mmCode" id="mmCode" type="text" disabled="disabled" style="width:96%" value="${mmCode}">
    </td>
    <td style="width:20%;">Year</td>
    <td style="width:30%;">
        <input name="yearOfFirstRegistration" id="yearOfFirstRegistration" type="text" disabled="disabled" style="width:96%" value="${yearOfFirstRegistration}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">New/Used</td>
    <td style="width:30%;">
        %{--<input name="newOrUsed" id="newOrUsed" type="text" disabled="disabled" style="width:96%">--}%
        <rt:regTrackNewUsed formFieldName="newOrUsed" value="${newOrUsed}" />
    </td>
    %{--<input name="clientOrJuristic" id="clientOrJuristic" type="text" disabled="disabled" style="width:96%">--}%
    <td style="width:20%;">Client/Juristic</td>
    <td style="width:30%;">
        <rt:regTrackClientJuristic formFieldName="clientOrJuristic" value="${clientOrJuristic}" />
    </td>
</tr>
</tbody>
