package za.co.signio.model

class VehicleoptionalextraInstitution implements Serializable {

    Vehicleoptionalextra vehicleoptionalextra
    Institution institution
    String type

    static hasOne = [vehicleoptionalextra:Vehicleoptionalextra, institution:Institution]
    static auditable = true

    static mapping = {
        id composite: ['vehicleoptionalextra', 'institution']
        version false
    }

    static constraints = {
        type maxSize: 25, nullable: false
    }

    String toString() {
        return type
    }
}
