package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/11/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
class Deadletter {

    String description
    Date dateCreated
    byte[] messageObject

    static constraints = {
        description(nullable: false, maxSize: 255)
        dateCreated nullable: false
        messageObject(nullable: false)
    }

    static mapping = {
        version false
    }

    String toString(){
        return description
    }
}
