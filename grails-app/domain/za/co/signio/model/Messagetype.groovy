package za.co.signio.model

class Messagetype {

    String type;

    static hasMany = [messages: Message]

    static mapping = {
        version false;
    }

    static constraints = {
        type maxSize: 45, nullable: false;
    }

    String toString() {
        return type;
    }
}
