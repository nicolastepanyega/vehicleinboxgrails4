package za.co.signio.vehicleinbox.service

import groovy.util.ConfigObject;


import grails.util.Holders
import groovy.util.logging.Slf4j;
@Slf4j
class SupportNotificationService {
	
	
	private ConfigObject config = Holders.config;
	
	def mailService
	
	def notifySupport(String prefix, String subject, String message){
		notifySupport(prefix + " - " + subject, message);
	}
	
	def notifySupport(String subject, String message){
		log.info("\nSUBJECT: " + subject + "\nMESSAGE: " + message);

		try {
			mailService.sendMail{
				to config.emails.to
				from config.emails.from
				subject subject
				body message
			}
			log.info("Email sent!")
		} catch (Exception e) {
			log.error("Failed to send eMail with error "+ e.message+ " " +e.cause);
			log.error(e.stackTrace);
		}
	}
}
