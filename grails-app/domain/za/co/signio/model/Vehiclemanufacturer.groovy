package za.co.signio.model


class Vehiclemanufacturer {

    String name

    static hasMany = [vehicles:Vehicle]

    static mapping = {
        version false
    }

    static constraints = {
        name maxSize: 45, nullable: false
    }

    String toString() {
        return name
    }
}
