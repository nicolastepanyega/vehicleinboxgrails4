package za.co.signio.vehicleinbox.controller

class UrlMappings {

    static mappings = {

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        // mapping the authentication controller.
        "/auth/$action"(controller : "authentication")

        // mapping the regTrack view
        "/regtrack/$action"(controller : "regtrack")

        // mapping the VehicleInbox
        "/vehicleInbox/$action"(controller : "vehicleInbox")
        "/vehicleinbox/index" (view : "vehicleInbox/index")

        // mapping the Natis Request
        "/natis/$action/$id?"(controller : "natis")
        "/natis/index/$id" (view : "natis/index")
        "/natis/edit/$id" (view : "natis/edit")

        // mapping the SBSA Dealsearch
        "/dealSearch"(view: "dealsearch/index")
        "/dealSearch/$action/$id?" (controller: "dealSearch")

        // mapping the VehicleDetail
        "/vehicledetail/newVehicle/$id?"(view : "vehicledetail/newVehicle")
        "/vehicledetail/$action/$id?"(controller : "vehicleDetail")

        // mapping VehicleDetailUtil
        "/vehicledetailutil/index/$id"(view : "/vehicledetailutil/index")
        "/vehicledetailutil/$action"(controller: "vehicleDetailUtil")

        // mapping the authentication controller.
        "/"(controller : "authentication", action:"index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
