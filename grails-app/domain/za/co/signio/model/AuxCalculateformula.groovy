package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 2/27/12
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
class AuxCalculateformula {
    

    Boolean isExtraFixed
    Boolean isExtraDisplayOnly
    Boolean isVatable
    Double premium
    Double commissionPercentage
    Double commissionValue
    Double payoverPercentage
    Double payoverValue
    Double extraPercentage
    Double extraValue
    Double adminPercentage
    Double adminValue
    Double extraCap
    Double maintenancePortion
    Double maxValue
    Double minValue
    AuxProductcategoryformula auxProductcategoryformula
    AuxProductcategoryformula totalPremiumProductcategoryformula
    Double ratePer1000
    Integer residualPercentageCovered
    Boolean allowExtraFee
    Double inspectionOnceoffFee
    Boolean allowPremiumEdit
    Boolean canEditInspectionOnceoffFee


    //static hasOne = [auxProductcategoryformula:AuxProductcategoryformula, totalPremiumProductcategoryformula: AuxProductcategoryformula]

    static hasMany = [auxProductOptionSetups:AuxProductoptionsetup]

    static constraints = {
        isExtraFixed(nullable: false)
        isExtraDisplayOnly(nullable: false)
        isVatable(nullable: false)
        premium(nullable: true)
        commissionPercentage(nullable:true)
        commissionValue(nullable: true)
        payoverPercentage(nullable: true)
        payoverValue(nullable: true)
        extraPercentage(nullable: true)
        extraValue(nullable: true)
        adminPercentage(nullable: true)
        adminValue(nullable: true)
        extraCap(nullable: true)
        maintenancePortion(nullable: true)
        maxValue(nullable: true)
        minValue(nullable: true)
        auxProductcategoryformula(nullable: false)
        totalPremiumProductcategoryformula (nullable: true)
        ratePer1000(nullable: true)
        residualPercentageCovered(nullable: true)
        allowExtraFee(nullable: true)
        inspectionOnceoffFee(nullable: true)
        allowPremiumEdit(nullable: true)
        canEditInspectionOnceoffFee(nullable: true)
    }

    static mapping = {
        version false
        ratePer1000(column: 'rate_per_1000')

    }

}
