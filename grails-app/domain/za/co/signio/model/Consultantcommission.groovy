package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 3/25/13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
class Consultantcommission {

    Double commissionAllocated
    Date commissionDate
    Date dateCreated
    String comment

    static hasOne = [member: Member,
                     consultantcommissiontype: Consultantcommissiontype]

    static belongsTo = [Member, Consultantcommissiontype]

    static constraints = {
        commissionAllocated nullable: false
        commissionDate(nullable: false)
        dateCreated(nullable: false)
        comment(nullable: true, maxSize: 255)
        member(nullable: false)
        consultantcommissiontype(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
