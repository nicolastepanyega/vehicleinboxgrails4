package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/05/02
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxProductAuxLogo {

    AuxProduct auxProduct
    AuxLogo auxLogo
    String logoPosition

    static belongsTo = [AuxProduct, AuxLogo]

    static constraints = {
        auxProduct(nullable: false)
        auxLogo(nullable: false)
        logoPosition(nullable: false, maxSize: 2)
    }

    static mapping = {
        version false
    }


}
