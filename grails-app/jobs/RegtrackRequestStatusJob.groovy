import grails.util.Holders;

class RegtrackRequestStatusJob {	
	def regtrackService;
    
	static triggers = {
        if(Holders.config.jobs.regtrackRequestStatus.enabled){
            cron cronExpression: Holders.config.jobs.regtrackRequestStatus.expression;
        }
    }

    def execute() {
        regtrackService.requestStatusUpdate()
    }
}