package za.co.signio.vehicleinbox.service

import grails.converters.JSON
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder



import za.co.signio.dataModel.SignioDataModel
import za.co.signio.model.Deal;
import za.co.signio.model.Dealstate;
import za.co.signio.model.Dealtype;
import za.co.signio.vehicleinbox.helper.OriginalNatisStatusCode;
import za.co.signio.webservice.ThirdParty;
import za.co.signio.webservice.WebServiceResponse;
import za.co.signio.webservice.WebServiceResponseState;
import grails.util.Holders;
import za.co.signio.webservice.ThirdParty.CERTRACK;

class CertrackNatisStatusService {
	def supportNotificationService;
	def thirdPartyWebserviceRequestService;
	
	/*private static final Logger LOGGER = Logger.getLogger(this);*/
	
	def checkNatisStatus(){
		
		for(ThirdParty.CERTRACK status : [CERTRACK.WESBANK_STATUS, CERTRACK.SBSA_STATUS]){
			log.info("**** Check Original Natis Status: " + status + " *****");
			
			// make the third party webservice call...
			WebServiceResponse wsResponse = thirdPartyWebserviceRequestService.sendRequest(ThirdParty.CERTRACK, status, "");
			
			new XmlSlurper().parseText(wsResponse.getResult()).signioNotificationElement.each { notification ->
				
				// try to deals with the given batchref.
				String batchRef = notification.requestReference;
				Dealtype dealType = Dealtype.findByType("OriginalNatis");
				Dealstate dealState = Dealstate.findByStateCode(61200);
	
				Deal deal = Deal.findByBatchRefAndDealtypeAndDealstate(batchRef, dealType, dealState);
		
				if(deal != null){
					String requestStatus = OriginalNatisStatusCode.valueOf(notification.eventType.value.toString()).getMessage();
					String comment = requestStatus + " - " + notification.comment.toString();
					log.info("Found: " + batchRef + " (id:" + deal.id + ") - Updating comment to: '" + comment + "'");
					deal.setComment(comment);
					deal.setDateUpdated(new Date());
					deal.save(flush: true, failOnError: true);
				}
			}
		}
	}
}
