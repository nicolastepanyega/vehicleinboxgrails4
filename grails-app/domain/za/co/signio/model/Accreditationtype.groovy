package za.co.signio.model

class Accreditationtype {

    String type

    static hasMany = [accreditations:Accreditation]
    static constraints = {
        type(nullable: false, maxSize: 100)
    }
    static mapping = {
        version(false)
    }

    public String toString(){
        type
    }
}
