package za.co.signio.model

class Preference {
    String type

    static hasMany = [memberPreferences:MemberPreference]
    static auditable = true

    static mapping = {
        version false
    }

    static constraints = {
        type maxSize: 255, nullable: false
    }

    String toString() {
        return type
    }
}
