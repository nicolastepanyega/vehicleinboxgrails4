package za.co.signio.vehicleinbox.controller

import za.co.signio.model.City;
import za.co.signio.model.Suburb;
import grails.converters.JSON;

class AddressController {
	
	def getCities(){
		List cities = new LinkedList();
		City.findAll(sort:"name").each { city ->
			cities.add([key:city.id, value:city.getName()]);
		};
		render cities  as JSON;
	}
	
	def getSuburbs(){
		List suburbs = new LinkedList();
		if(params.id != null && !params.id.isEmpty()){
			Suburb.findAllByCity(City.get(params.id), [sort: "name"]).each{ suburb ->
				suburbs.add([key:suburb.id, value:suburb.getName()]);
			};
		}
		else{
			// do nothing.
		}
		
		render suburbs  as JSON;
	}
	
	def getPostalCode(){
		List<String> postalAddress = new LinkedList<String>();
		if(params.id != null){
			Suburb suburb = Suburb.get(params.id);
			if(!suburb.getBoxCode().isEmpty()){
				postalAddress.add(suburb.getBoxCode());
			}
			if(!suburb.getStreetCode().isEmpty() && !suburb.getStreetCode().equals(suburb.getBoxCode())){
				postalAddress.add(suburb.getStreetCode());
			}
		}
		else{
			// do nothing.
		}
		
		render postalAddress  as JSON;
	}
}
