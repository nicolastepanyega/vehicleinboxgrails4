package za.co.signio.model

class Servicehandler {

    String handlerName

    static belongsTo = [AuxProduct]

    static constraints = {
        handlerName(nullable: false, maxSize: 100)
    }

    static mapping = {
        version false
    }
}
