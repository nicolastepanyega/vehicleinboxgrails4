package za.co.signio.model

class AuxValueindicator {

    String indicator

    static hasMany = [auxCriteriaSetups:AuxCriteriasetup]

    static constraints = {
        indicator nullable: false, maxSize: 45
    }

    static mapping = {
        version false
    }

    String toString() {
        return indicator
    }
}
