package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 5/28/12
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
class Processingcentrearea {

    String areaCode
    String name

    static belongsTo = [Institution, Member]

    static hasMany = [deals:Deal, members:Member, institutions:Institution]

    static constraints = {
        areaCode (nullable: false, maxSize: 45)
        name (nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
        members joinTable: 'member_processingcentrearea'
        institutions joinTable: 'processingcentrearea_institution'
    }
}
