package za.co.signio.model

class SupportingdocpageImage {

    Supportingdocpage supportingdocpage
    Image image
    static belongsTo = [Supportingdocpage, Image]

    static constraints = {
        supportingdocpage(nullable: false)
        image(nullable: false)
    }

    static mapping = {
        version false
    }
}
