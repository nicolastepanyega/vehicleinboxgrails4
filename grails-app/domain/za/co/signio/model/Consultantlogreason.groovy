package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/20/12
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
class Consultantlogreason {
    
    String reason

    static hasMany = [consultantLogs:Consultantlog]

    static constraints = {
        reason nullable: false, maxSize: 45
    }

    static mapping = {
        version false;
    }
}
