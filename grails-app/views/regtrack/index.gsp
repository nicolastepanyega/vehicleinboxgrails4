<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="minimal"/>
        <title>Vehicle Inbox - V5 - RegTrack Request</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    </head>
    <body>
        <table width="98%" class="responsive BGColourDarkBlue">
            <tr><td class="BGColourWhite"><img alt="SignioLogo" src="${resource(dir: 'images', file: 'signio_logo.png')}"></td>
            </tr>
            <tr><td class="BGColoursHeading">
                <h4>Submit RegTrack Request</h4>
            </td></tr>
            <g:render template="/regtrack/view_controls"/>
            <g:render template="/regtrack/regtrack_form"/>
        </table>
    %{-- <r:layoutResources/> --}%
    </body>
</html>

