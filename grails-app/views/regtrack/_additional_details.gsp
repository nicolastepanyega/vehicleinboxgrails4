<tbody class='clientDetail'><tr class='BGColourBlue'><td colspan="4">Additional Details</td></tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Used On Public Road</td>
    <td style="width:30%;">
        %{--<input name="vinNumber" id="vinNumber" type="text" disabled="disabled" style="width:96%">--}%
        <rt:regTrackFormField formFieldName="regtrackUsedOnPublicRoad" />
    </td>
    <td style="width:20%;">Vehicle Usage</td>
    <td style="width:30%;">
        <rt:regTrackFormField formFieldName="regtrackVehicleUsage" />
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Economic Sector</td>
    <td style="width:30%;">
        <rt:regTrackFormField formFieldName="regtrackEconomicSector" />
    </td>
    <td style="width:20%;">Nature Of Ownership</td>
    <td style="width:30%;">
        <rt:regTrackFormField formFieldName="regtrackNatureOfOwnership" />
    </td>
</tr>
<tr class='BGColourBlueTD regTrack' style="display: none;">
    <td style="width:20%;">Reason for Registration</td>
    <td style="width:30%;">
        <rt:regTrackFormField formFieldName="regtrackReasonForRegistration" />
    </td>
    %{--Open Cell--}%
    <td style="width:20%;"></td>
    <td style="width:30%;">
    </td>
</tr>
</tbody>