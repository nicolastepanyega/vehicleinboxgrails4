package za.co.signio.vehicleinbox.controller



class VehicleInboxController {


    def vehicleInboxService

    def index(){
       def results = vehicleInboxService.getVehicleInboxList(params,session)


        results
    }

    def vviWebsite(){
        redirect(url: "http://auto.test.lightstone.co.za")
    }

}
