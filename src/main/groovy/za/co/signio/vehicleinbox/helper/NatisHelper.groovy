package za.co.signio.vehicleinbox.helper

import groovy.xml.MarkupBuilder


import org.grails.web.json.JSONObject;
import za.co.signio.model.Suburb;
import za.co.signio.model.InstitutionInstitution;
import org.apache.commons.codec.binary.Base64;

public class NatisHelper {
	

	
	public static String composeOriginalNatisSDM(String originalNatisRequest, String originalNatisResponse){
		def requestXML = new XmlSlurper().parseText(originalNatisRequest).requestInfo;
		def responseXML = new XmlSlurper().parseText(originalNatisResponse).signioOriginalNatisResponse.success;
		Writer writer = new StringWriter();
		MarkupBuilder builder = new MarkupBuilder(writer);
		builder.field(formFieldName:"externalReferenceNumber", mapping:"-1", requestXML.coreReferenceNumber);
		builder.field(formFieldName:"referenceNumber", mapping:"332", responseXML.certrackRequestReference);
		return "<signioPackage><dataBundle>" + writer.toString() + "</dataBundle></signioPackage>";
	}

	public static String composeOriginalNatisXML(JSONObject json, session){
		Writer writer = new StringWriter();
		MarkupBuilder builder = new MarkupBuilder(writer);
		
		// parse the suburb
		Suburb suburb;
		if(json.suburb != null && !json.suburb.isEmpty()){
			suburb = Suburb.get(json.suburb);
		}
		
		// If json.branch has been set, then use branch code, else use dealerCode.
		if(json.branch == null || json.branch.isEmpty() || true){
			InstitutionInstitution.createCriteria().list(max:1){
				institution1{ eq("name", json.bank) }
				institution2{ eq("id", session["institutionID"]) }
			}.each{ inst_inst ->
				json.dealerCode = inst_inst.getMerchantId();
				json.branch = "";
			}
		}
		else{
			json.dealerCode = "";
		}

		builder.signioOriginalNatis{
			signioOriginalNatisRequest{
				requestInfo{
					dealersReferenceNumber(json.BatchRef)
					ownerName(json.ownerName)
					forAttentionOf(json.attention)
					accountNumber(json.accountNumber)
					comments(json.comments)
					contactTelephoneNumber(json.telephoneNumber)
					contactCellphoneNumber(json.cellphoneNumber)
					contactEmailAddress(json.emailAddress)
					dealerCode(json.dealerCode)
					dealerLetterRequired("" + json.dealerLetterRequired)
					registerNumber(json.registerNumber)
					registrationNumber(json.registrationNumber)
					vehicleMake(json.make)
					vinOrChassisNumber(json.vinNumber)
					engineNumber(json.engineNumber)
					preferredDeliveryMethod{
						value(json.originalnatis_deliveryMethod)
					}
					deliveryAddress{
						buildingOrRoom(json.addressLine1)
						streetOrPoBox(json.addressLine2)
						suburbOrPostOffice((suburb != null) ? suburb.getName() : "")
						cityOrTown((suburb != null) ? suburb.getCity().getName() : "")
						postalCode(json.postalCode)
					}
				}
			}
		};
		
		if(json.bank == "Wesbank"){
			builder.signioUploadDocuments(){
				document(
					documentTypeCode:"POP",
					originalFilename:"ProofOfPayment",
					mimeType: json.document_pop.mimeType,
					new String(Base64.encodeBase64(json.document_pop.fileContents)))
				document(
					documentTypeCode:"SETL",
					originalFilename:"SettlementLetter",
					mimeType: json.document_setl.mimeType,
					new String(Base64.encodeBase64(json.document_setl.fileContents)))
			}
		} else {
            String settlementLetterDoc =
                    (json.document_setl == null || json.document_setl?.fileContents == "Browse...")? "" : new String(Base64.encodeBase64(json.document_setl.fileContents));

            String proofOfPaymentDoc =
                    (json.document_pop == null || json.document_pop?.fileContents == "Browse...")? "" : new String(Base64.encodeBase64(json.document_pop.fileContents));

            if (!"".equals(proofOfPaymentDoc) || !"".equals(settlementLetterDoc)) {
                builder.signioUploadDocuments(){
                    if (!"".equals(proofOfPaymentDoc)) {
                        document(
                                documentTypeCode:"POP",
                                originalFilename:"ProofOfPayment",
                                mimeType: json.document_pop.mimeType,
                                proofOfPaymentDoc)
                    }
                    if (!"".equals(settlementLetterDoc)) {
                        document(
                                documentTypeCode:"SETL",
                                originalFilename:"SettlementLetter",
                                mimeType: json.document_pop.mimeType,
                                settlementLetterDoc)
                    }
                }
            }
        }
		
		return "<request>" + writer.toString() + "</request>";
	}
	
	public static String composeCopyNatisSDM(String copyNatis){
		def xml = new XmlSlurper().parseText(copyNatis).success;
		Writer writer = new StringWriter();
		MarkupBuilder builder = new MarkupBuilder(writer);
		builder.field(formFieldName:"documentIssueDate", mapping:"-1", xml.documentIssueDate);
		builder.field(formFieldName:"description", mapping:"-1", xml.description);
		builder.field(formFieldName:"vehicleMake", mapping:"-1", xml.make);
		builder.field(formFieldName:"vehicleModel", mapping:"-1", xml.model);
		builder.field(formFieldName:"registerNumber", mapping:"-1", xml.natisRegisterNumber);
		builder.field(formFieldName:"ownerName", mapping:"-1", xml.ownerName);
		builder.field(formFieldName:"ownerIdNumber", mapping:"-1", xml.ownerIdNumber);
		builder.field(formFieldName:"titleHolderName", mapping:"-1", xml.titleholderName);
		builder.field(formFieldName:"titleHolderIdNumber", mapping:"-1", xml.titleholderIdNumber);
		builder.field(formFieldName:"yearOfLiabilityForLicensing", mapping:"-1", xml.yearOfLiabilityForLicensing);
		builder.field(formFieldName:"registrationNumber", mapping:"-1", xml.natisRegisterNumber);
		builder.field(formFieldName:"vinChassis", mapping:"-1", xml.vin);
		builder.field(formFieldName:"engineNumber", mapping:"-1", xml.engineNumber);
		return "<signioPackage><dataBundle>" + writer.toString() + "</dataBundle></signioPackage>";
	}
	
	public static String composeCopyNatisXML(JSONObject json, session){
		// get the bank code.
		String bCode = [
			"STANDARD BANK":"SBVAF",	// Standard Bank
			"ABSA BANK":"AVAF",			// Absa Bank
			"Wesbank":"WES",			// WesBank
			"MFC":"MFC"					// MFC
		].get(json.bank);
	
		// get the dealer Code.
		InstitutionInstitution.createCriteria().list(max:1){
			institution1{ eq("name", json.bank) }
			institution2{ eq("id", session["institutionID"]) }
		}.each{ inst_inst ->
			json.dealerCode = inst_inst.getMerchantId();
		}
		
		// build the XML
		Writer writer = new StringWriter();
		MarkupBuilder builder = new MarkupBuilder(writer);
		builder.registrationInformationQueryRequest(){
			criteria{
				VIN(json.vinNumber)
			}
			dealer{
				bankCode(bCode)
				dealerCode(json.dealerCode)
			}
			requesterDetails{
				requesterEmail(json.emailAddress)
				requesterName(json.attention)
				requesterPhone(json.telephoneNumber)
			}
			uniqueReference(json.batchRef)
		}
		return writer.toString();
	}
}
