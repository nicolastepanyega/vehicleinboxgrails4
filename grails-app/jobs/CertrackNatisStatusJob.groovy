
import groovy.util.ConfigObject;
import grails.util.Holders;


import java.beans.SimpleBeanInfo;

class CertrackNatisStatusJob {
	def certrackNatisStatusService; 

	static triggers = {
		if(Holders.config.jobs.certrackNatisStatus.enabled){
			cron cronExpression: Holders.config.jobs.certrackNatisStatus.expression;
		}
    }

    def execute() {
		certrackNatisStatusService.checkNatisStatus();
    }
}