<%--
  User: chris.ferreira@signio.co.za
  Date: 2013/08/16
  Time: 11:41 AM
--%>
<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="minimal"/>
    <g:if test="${title != null}">
        <title>${title}</title>
    </g:if>
    <g:else>
        <title>Vehicle Inbox - V5 - Error</title>
    </g:else>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}">
</head>
<body>
<table width="98%" class="BGColourLightBlue">
    <tr>
        <td colspan="4"  class="BGColourWhite" ><g:img dir="images" file="signio_logo.png"/></td>
    </tr>
    <tr>
        <g:if test="${shortMessage != null}">
            <td colspan="4" class="BGColoursHeading" ><h4>${shortMessage}<br>Date: ${new java.util.Date()}</h4></td>
        </g:if>
        <g:else>
            <td colspan="4" class="BGColoursHeading" ><h4>An Unknown Error Occurred. Please Contact Support.
                <br>Date: ${new java.util.Date()} </h4></td>
        </g:else>
    </tr>
    <g:render template="errorContact" />
    %{--<g:render template="errorDisplay" />--}%
</table>
</body>
</html>

