package za.co.signio.model

class CrmDocument {
    String uri
    Boolean status
    Date dateCreated
    CrmEventlog crmEventlog
    CrmDocumenttype crmDocumenttype

    static belongsTo = [CrmEventlog, CrmDocumenttype]

    static constraints = {
        uri (maxSize: 255, nullable: false)
        status nullable: false
        dateCreated nullable: false
        crmEventlog nullable: false
        crmDocumenttype nullable: false
    }

    static mapping = {
        version false
    }

    def beforeInsert = {
        dateCreated = new Date()
    }
}
