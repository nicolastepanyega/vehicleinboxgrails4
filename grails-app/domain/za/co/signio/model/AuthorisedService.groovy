package za.co.signio.model

class AuthorisedService {
    String service

    static hasMany = [memberCompliances:MemberCompliance]

    static constraints = {
        service(nullable: false, maxSize: 255)
    }

    static mapping = {
        version false
    }
}
