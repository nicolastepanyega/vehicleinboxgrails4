package za.co.signio.model

class Igfcost {

    Double cost
    InstitutionInstitution institutionInstitution
    Igfcosttype igfcosttype

    static mapping = {
        version false
    }

    static constraints = {
        cost(nullable: false)
        institutionInstitution(nullable: false)
        igfcosttype(nullable: false)
    }
}
