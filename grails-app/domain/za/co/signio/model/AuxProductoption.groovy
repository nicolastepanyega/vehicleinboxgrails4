package za.co.signio.model

class AuxProductoption {

	String productOptionName
    String supplierReferenceCode
    Date optionStartDate
    Date optionEndDate
    Boolean isBulk
	Boolean active

    static belongsTo = [AuxProduct, AuxProductoptionsetup]
    static hasOne = [auxProduct:AuxProduct, auxProductoptionsetup:AuxProductoptionsetup]
    static hasMany = [auxCriteriaSetups:AuxCriteriasetup, auxProductOptionBulkingConfigs:AuxProductoptionbulkingconfig,
                      auxProductoptioninstitutionfspconfig:AuxProductoptioninstitutionfspconfig,
                      auxDocuments: AuxDocument,
                      documentTemplateGroups:Documenttemplategroup,
                      auxComments: AuxComment,
                      auxReferenceCodeConfigs: AuxReferencecodeconfig]

	static mapping = {
        auxDocuments joinTable: 'aux_document_aux_productoption'
        documentTemplateGroups joinTable: 'documenttemplategroup_aux_productoption'
		version false
	}

	static constraints = {
		productOptionName maxSize: 255, nullable: false
        supplierReferenceCode nullable: true, maxSize: 255
        optionStartDate nullable: true
        optionEndDate nullable: true
        isBulk nullable: false
        active nullable: false
        auxProduct nullable: false
        auxProductoptionsetup nullable: true
	}

    def String toString()
    {
      return "${productOptionName}"
    }
}
