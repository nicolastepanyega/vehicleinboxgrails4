package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Pieter
 * Date: 2015/05/01
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxLogo {

    String name
    Byte[] image

    static hasMany = [auxProductAuxLogos: AuxProductAuxLogo]

    static constraints = {
        name(nullable: false, maxSize: 45)
        image(nullable: false)
    }
    static mapping = {
        version(false)
    }

}
