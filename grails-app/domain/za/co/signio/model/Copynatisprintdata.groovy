package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 3/20/13
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */
class Copynatisprintdata {
    
    String accountNumber
    String bankName
    String description
    String documentIssueDate
    String vehicleMake
    String vehicleModel
    String vinChassis
    String engineNumber
    String registerNumber
    String ownerIdNumber
    String ownerName
    String titleHolderIdNumber
    String titleHolderName
    String yearOfLiabilityForLicensing
    String registrationNumber


    static hasOne = [vehicledetail:Vehicledetail]

    static constraints = {
        accountNumber (nullable: true, maxSize: 45)
        bankName (nullable: true, maxSize: 45)
        description (nullable: true, maxSize: 45)
        documentIssueDate (nullable: true, maxSize: 45)
        vehicleMake (nullable: true, maxSize: 45)
        vehicleModel (nullable: true, maxSize: 45)
        vinChassis (nullable: true, maxSize: 45)
        engineNumber (nullable: true, maxSize: 45)
        registerNumber (nullable: true, maxSize: 45)
        ownerIdNumber (nullable: true, maxSize: 45)
        ownerName (nullable: true, maxSize: 45)
        titleHolderIdNumber (nullable: true, maxSize: 45)
        titleHolderName (nullable: true, maxSize: 45)
        yearOfLiabilityForLicensing (nullable: true, maxSize: 45)
        registrationNumber (nullable: true, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
