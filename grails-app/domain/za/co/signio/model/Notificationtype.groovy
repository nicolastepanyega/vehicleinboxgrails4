package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/14/12
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */
class Notificationtype {
    
    String type

    static hasMany = [notifications:Notification]

    static constraints = {
        type nullable: false
    }

    static mapping = {
        version false
    }
}
