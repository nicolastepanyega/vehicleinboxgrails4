package za.co.signio.model

class Addendum {

	byte[] document
	Boolean status
    Date dateCreated

    Dealstate dealstate

    static hasOne = [addendumtype:Addendumtype,
                     dealgroup:Dealgroup]

    static belongsTo = [Dealgroup, Addendumtype, Dealstate]
    static auditable = true

	static mapping = {
		version false
	}

	static constraints = {
        document nullable:false
        status nullable:false
        dateCreated nullable: false
        addendumtype nullable:false
        dealgroup nullable:false
	}

    String toString(){
        return "${addendumtype.toString()}"
    }
}
