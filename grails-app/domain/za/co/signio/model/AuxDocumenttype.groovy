package za.co.signio.model

class AuxDocumenttype {
    String type

    static hasMany = [auxDocuments:AuxDocument]

    static constraints = {
        type(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false;
    }

    public String toString(){
        type
    }
}
