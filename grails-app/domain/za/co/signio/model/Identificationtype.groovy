package za.co.signio.model

class Identificationtype {
    String type

    static hasMany = [members:Member]
    static auditable = true

    static mapping = {
        version false
    }

    static constraints = {
        type maxSize: 45, nullable: false
    }

    String toString() {
        return type
    }
}
