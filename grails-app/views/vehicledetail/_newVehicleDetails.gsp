<tr><td class="BGColourWhite" ><form id="newVehicleForm" action="">
    <input type="hidden" id="dealID" name="dealID" value="${params.dealID}"/>
    <table style="width:98%" class="responsive BGColourDarkBlue">
        <tbody><tr class='BGColourBlue'><td colspan="4">Contact Details</td></tr>
        <tr class='BGColourBlueTD'>
            <td style="width:25%;"><label for="vehicleManufacturer">Vehicle Make</label></td>
            <td style="width:25%;"><vd:vehicleDetailSelectField class="select" elementName="vehicleManufacturer" deal_id="${params.dealID}"/></td>
            <td style="width:25%;"><label for="vehicleModel">Vehicle Model</label></td>
            <td style="width:25%;"><vd:vehicleDetailSelectField class="select" elementName="vehicleModel" deal_id="${params.dealID}"/></td>
        </tr>
        <tr class='BGColourBlueTD'>
            <td><label for="vehicleYear">Vehicle Year</label></td>
            <td><vd:vehicleDetailSelectField class="select" elementName="vehicleYear" deal_id="${params.dealID}"/></td>
            <td><label for="vehicleColour">Vehicle Colour</label></td>
            <td><vd:vehicleDetailSelectField class="select" elementName="vehicleColour" deal_id="${params.dealID}"/></td>
        </tr>
        <tr class='BGColourBlueTD'>
            <td><label for="vinNumber">Vehicle VIN</label></td>
            <td><vd:vehicleDetailTextField elementName="vinNumber" deal_id="${params.dealID}"/></td>
            <td><label for="registrationNumber">Licence No</label></td>
            <td><vd:vehicleDetailTextField elementName="registrationNumber" deal_id="${params.dealID}"/></td>
        </tr>
        <tr class='BGColourBlueTD'>
            <td><label for="natisRegisteredNumber">Register No</label></td>
            <td><vd:vehicleDetailTextField elementName="natisRegisteredNumber" deal_id="${params.dealID}"/></td>
            <td colspan="2"></td>
        </tr>
        </tbody>
    </table>
</form></td></tr>

<script type="text/javascript">
	var inboxVinNumbers = [{key:"6", value:"AAPV0420530449459"}];
	$(document).ready(function(){
		inboxVinNumbers = signio.vehicleInbox.callAjax("./getVehicleVinNumbers");

        var dealID = $('#dealID').val();
        if (!dealID) {
            modelSelect.prop('disabled', 'disabled');
            yearSelect.prop('disabled', 'disabled');
        }

	});

    // Global Variables
    var makeSelect = $("#vehicleManufacturer"),
            yearSelect = $("#vehicleYear"),
            modelSelect = $("#vehicleModel"),
            colourSelect = $("#vehicleColour");

    $(document).ready(function(){
        makeSelect.change(function() {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            populateYear(valueSelected);
        });

        // Add change Listener.
        yearSelect.change(function() {
            var optionSelected = $("option:selected", this);
            var yearSelected = this.value;
            var manID = makeSelect.val();
            populateModel(manID, yearSelected);
        });

        // VALIDATION!
        // create some validation rules.
        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            return this.optional(element) || value !== param;
        }, "Please choose a value!");

        jQuery.validator.addMethod("nullable", function(value, element, param) {
            return true;
        }, "");

        jQuery.validator.addMethod("match", function(value, element, param) {
            return this.optional(element) || value.match(param) !== null;
        }, "Please choose a value!");

        jQuery.validator.addMethod("vinUnique", function(value, element, param) {
            if(!this.optional(element)){
                var noMatch = true;
                $.each(inboxVinNumbers, function(index, vinNumber){
					if($("#dealID").val() != vinNumber.key && $("#vinNumber").val() == vinNumber.value){
						noMatch = false;
						return false;
					}
                })
				return noMatch;
            }
            else{
				return true;
            };
        }, "Please choose a value!");

        

        // set the validation on the form.
        $("#newVehicleForm").validate({
            rules: {
                vehicleManufacturer: {required: true, notEqual:"-1"},
                vehicleModel: {required: true, notEqual:"-1"},
                vehicleYear: {required: true, notEqual:"-1"},
                vehicleColour: {required: true, notEqual:"-1"},
                vinNumber : {required: true, match:"^[A-HJ-NP-Z0-9]{17}$", vinUnique:true},
                registrationNumber : {nullable: true},
                natisRegisteredNumber : {nullable: true}

            },
            messages:{
                vehicleManufacturer: {notEqual:"Please Select a valid vehicle make"},
                vehicleModel: {notEqual:"Please Select a valid vehicle model"},
                vehicleYear: {notEqual:"Please Select a valid vehicle registration year"},
                vehicleColour: {notEqual:"Please Select a valid vehicle colour"},
                vinNumber: { match:"Please Enter a valid VIN.", vinUnique:"Vin Number already exists in the Vehicle Inbox"}
            }
        });
    });

    function populateYear(manufacturerID) {
        var selectOptions = "<option value='-1'>-Select-</option>";
        if (manufacturerID != null && manufacturerID != "-1") {
            $.ajax({
                url: "/VehicleInbox/vehicledetailutil/getVehicleYears?id=" + manufacturerID,
                contentType: 'application/javascript; charset=utf-8',
                async : false,
                type : "GET",
                error: function (xhr, ajaxOptions, thrownError) {
                    //console.log("Error occurred retrieving Years \nStatus: " + xhr.status + "\n Error: " + thrownError)
                },
                success: function(data){
                    // Populate year select-element.
                    $.each(data, function (index, year) {
                        selectOptions += "<option value='" + year + "'>" + year + "</option>";
                    });
                }
            });
        }
        yearSelect.find('option').remove().end().append(selectOptions);
        yearSelect.prop('disabled', false);
        modelSelect.find('option').remove().end().append("<option value='-1'>-Select-</option>");
        modelSelect.prop('disabled', 'disabled');
    };

    function populateModel(manufacturerID, year) {
        var selectOptions = "<option value='-1'>-Select-</option>";

        if (manufacturerID != null && manufacturerID != "-1") {
            $.ajax({
                url: "/VehicleInbox/vehicledetailutil/getVehicleModels?id=" + manufacturerID + "&year=" + year,
                contentType: 'application/javascript; charset=utf-8',
                async : false,
                type : "GET",
                error: function (xhr, ajaxOptions, thrownError) {
                    //console.log("Error occurred retrieving Models \nStatus: " + xhr.status + "\n Error: " + thrownError)
                },
                success: function(data){
                    // Populate year select-element.
                    $.each(data, function (index, modelList) {
                        selectOptions += "<option value='" + modelList[0] + "'>" + modelList[1] + "</option>";
                    });
                }
            });
        }
        modelSelect.find('option').remove().end().append(selectOptions);
        modelSelect.prop('disabled', false);
    };
</script>
