package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/14
 * Time: 12:08 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxProductoptionbulkingconfig {

    static hasOne = [institution1:Institution, //Dealership
                     institution2:Institution,  //Bank
                     auxBulkingcode:AuxBulkingcode,
                     auxProductoption:AuxProductoption]

    static belongsTo = [Institution, AuxBulkingcode, AuxProductoption]

    static constraints = {
        institution1(nullable: false)
        institution2(nullable: false)
        auxBulkingcode(nullable: true)
        auxProductoption(nullable: false)
    }

    static mapping = {
        version false
    }

    public String toString(){
        "${institution1?.name} ${institution2?.name} ${auxBulkingcode?.bulkingCode} ${auxProductoption?.productOptionName}"
    }
}
