package za.co.signio.model

class MemberLogin {

    String server
    Date dateCreated

    static hasOne = [ relationship: Relationship ]

    static mapping = {
        version false
    }

    static constraints = {
        server nullable: false, maxSize: 45
        dateCreated nullable: false
        relationship nullable: false
    }

    def beforeInsert = {
        dateCreated = new Date()
    }

    String toString() {
        return "${relationship.toString()}: ${server} - ${dateCreated}"
    }

}
