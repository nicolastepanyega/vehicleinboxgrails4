package za.co.signio.model


class Supportingdocs {

    String documentThumbnail
    Date dateCreated
    Date dateUpdated
    String uri
    Boolean isCertified
    String mimeType
    String fileExtention

    static belongsTo = [Supportingdocsource, Companydetail, Member, MemberAccreditation, Clientdetail, Supportingdocstype, Institution, Note]

    static hasOne = [clientdetail: Clientdetail,
                     supportingdocstype: Supportingdocstype,
                     member: Member,
                     institution: Institution,
                     companydetail: Companydetail,
                     supportingdocsource:  Supportingdocsource,
                     memberAccreditation: MemberAccreditation]

    static hasMany = [institutionMemberSupportingdocAccess:Institutionmembersupportingdocaccess,
                      supportingdocpages:Supportingdocpage, notes: Note]


    static mapping = {
        //id generator: "assigned"
        notes joinTable: 'supportingdocs_note'
        version false
    }

    static constraints = {
        documentThumbnail nullable: false
        dateCreated nullable: false
        dateUpdated nullable: false
        uri nullable: false, maxSize: 255
        isCertified nullable: true
        clientdetail(nullable: true)
        supportingdocstype(nullable: false)
        member(nullable: true)
        institution(nullable: true)
        companydetail nullable: true
        supportingdocsource nullable: false
        memberAccreditation nullable: true
        mimeType nullable: true, maxSize: 45
        fileExtention nullable: true, maxSize: 5
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateCreated field in the object before insert.
     */
    def beforeInsert = {
        dateCreated = new Date()
    }
}
