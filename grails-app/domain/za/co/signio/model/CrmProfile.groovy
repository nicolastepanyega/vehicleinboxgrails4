package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/05/21
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmProfile {

    String name
    String xmlProfile

    static hasMany = [institutionInstitutions:InstitutionInstitution]

    static constraints = {
        name nullable: false, maxSize: 45
        xmlProfile nullable: false
    }

    static mapping = {
        version(false)
    }
}
