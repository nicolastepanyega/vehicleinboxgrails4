import grails.util.Holders;

class RegtrackUploadSupportingDocJob {
    def regtrackService;

    static triggers = {
        if(Holders.config.jobs.regtrackUploadSupportingDoc.enabled){
            cron cronExpression: Holders.config.jobs.regtrackUploadSupportingDoc.expression;
        }
    }

    def execute() {
        regtrackService.uploadSupportingDocs()
    }
}
