package za.co.signio.model

class Setupoption {

    String option

    static hasMany = [institutionSetupoptions: InstitutionSetupoption]
    static constraints = {
        option(nullable: false, maxSize: 50)
    }
    static mapping = {
        version(false)
    }

    public String toString(){
        return option
    }
}
