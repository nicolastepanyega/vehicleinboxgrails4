package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/08/28
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
class Singlesignon {

    String authKey
    Date dateCreated
    String xml

    static constraints = {
        authKey(nullable: false, maxSize: 255)
        dateCreated(nullable: false)
        xml(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
