<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>
<html>
<head>
	<meta name="layout" content="minimal"/>
	<title>Vehicle Inbox - V5 - Standard Bank Deal Search</title>
	%{-- <r:layoutResources/> --}%
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <script src="${resource(dir: 'js/businessRules', file: 'vehicleinbox.js')}"></script>
</head>
<body>
	<table width="98%"  class="responsive BGColourDarkBlue">
		<tr><td colspan="100%"  class="BGColourWhite" > <g:img dir="images" file="signio_logo.png"/></td></tr>
		<tr><td colspan="100%" class="BGColoursHeading" ><h4>Standard Bank Deal Search</h4></td></tr>
		
		<!-- searchbox. -->
		<tr><td class="BGColourWhite" >
			<form id="natisForm" action=""><table style="width:50%">
				<thead>
					<tr class='BGColourLightBlue'><th colspan="2">Search</th></tr>
				</thead>	
				<tbody>
					<tr class='BGColourBlue'>
						<td><label for="attention">Customer ID Number</label></td>
						<td><input type="text" name="idNumber" id="idNumber" style="width:100%"></td>
					</tr>
					<tr class='BGColourBlue originalNatis copyNatis'>
						<td colspan="2"><input type="button" value="Seach" id="searchButton"></td>
					</tr>
				</tbody>
			</table></form>
		</td></tr>
		
		<!-- client details box -->
		<tr id="resultTable" style="display:none"><td class="BGColourWhite">
			<table style="width:100%">
			<thead>
				<!-- <tr class='BGColourLightBlue'><th colspan="4">Results</th></tr> -->
				<tr class='BGColourLightBlue'><th>DealID</th><th>Vin Number</th><th colspan="2"></th></tr>
			</thead>
			<tbody id="results">
				
			</tbody></table>
		</td></tr>
    </table>
    
	<script type="text/javascript">

	var openCopyNatis = function(VIN){
		var form = $("form")
			.attr("action", "./natis/index/-1?natisType=copy").attr("method", "post")
			.append($("input").attr("type", "hidden").attr("name","vinNumber").val(VIN))
			.submit();
	}

	var openRequestSettlement = function(dealID){
		var form = $("form")
			.attr("action", "./redirect/redirectToVAF2").attr("method", "post")
			.append($("input").attr("type", "hidden").attr("name","dealNumber").val(dealID))
			.submit();
	}

	$(document).ready(function(){
		$("#searchButton").click(function(){
			var results = "";
			$.each(signio.vehicleInbox.callAjax("./dealSearch/findDealsByClientIDNumber/" + $("#idNumber").val()), function(index, item){
				results += "<tr class='BGColourBlue'>" + 
					"<td>" + item.dealID + "</td>" + 
					"<td>" + item.vin + "</td>" + 
					"<td><a href='javascript:void(0);' onclick='openRequestSettlement(\"" + item.dealID + "\")'>Request Settlement</td>" + 
					"<td><a href='javascript:void(0);' onclick='openCopyNatis(\"" + item.vin + "\")'>Request CopyNatis</td></tr>";
			});
			
			$("#results").html(results);
			$("#resultTable").show();
		});
	});
	
	</script>
</body>
</html>
