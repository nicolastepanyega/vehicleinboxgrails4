package za.co.signio.model

class Clientdetail {
    String firstName
    String lastName
    String middleName
    Date dateOfBirth
    String idNumber
    String passportNumber
    Date dateCreated

    static hasOne = [title: Title,
                     contactinfo: Contactinfo,
                     lookupdata: Lookupdata
    ]

    static belongsTo = [Title, Contactinfo]
    static hasMany = [dealgroups:Dealgroup, supportingDocs:Supportingdocs, institutionMemberSupportingdocAccess:Institutionmembersupportingdocaccess, otps:Otp]
    static auditable = true

    static mapping = {
        version false
    }

    static constraints = {
        firstName nullable: true, maxSize: 45
        lastName nullable: true, maxSize: 45
        dateOfBirth nullable: true
        idNumber nullable: true, unique: true, maxSize: 13
        contactinfo nullable: false
        title nullable: false
        middleName nullable: true
        passportNumber nullable: true, unique: true, maxSize: 45
        dateCreated nullable: true
        lookupdata nullable: true
    }

    String toString() {
        return firstName + ' ' + lastName
    }

    public static boolean checkClientExists(String IDNumber)
    {
        if(Clientdetail.findByIdNumber(IDNumber))
        {
            return true
        }
        else
        {
            return false
        }
    }
}
