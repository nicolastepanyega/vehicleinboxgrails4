package za.co.signio.vehicleinboxtags

import za.co.signio.model.Deal

class VehicleDetailTagLib {
    static namespace = "vd"

    def vehicleDetailService

    def vehicleDetailSelectField = {attrs, body ->
        def formFieldName = attrs.elementName;
        def dealID = attrs.deal_id;
        def inputType = attrs.class

        out << createFormSelectElement(inputType, dealID, formFieldName)
    }

    def vehicleDetailTextField = {attrs, body ->
        def formFieldName = attrs.elementName;
        def dealID = attrs.deal_id;

        out << createFormTextElement(dealID, formFieldName)
    }

    private def createFormSelectElement(inputType, deal_id, elementName) {
        if (deal_id == null || Integer.parseInt(deal_id) <= 0) {
            return createNewElement(inputType, elementName);
        } else {
            return createUpdateElement(inputType, deal_id, elementName);
        }
    }

    private def createFormTextElement(deal_id, elementName) {
        if (deal_id == null || Integer.parseInt(deal_id) <= 0) {
            return createNewTextElement(elementName);
        } else {
            return createUpdateTextElement(deal_id, elementName);
        }
    }

    private def createNewElement(inputType, elementName) {
        return g.select(
                [
                        namespace: namespace,
                        name: elementName,
                        id: elementName,
                        from: getList(elementName, null, null),
                        value: "",
                        optionKey: "code",
                        optionValue: "value",
                        noSelection: ["-1" : "-Select-"]
                ]
        )
    }

    private def createNewTextElement(elementName) {
        return g.textField(
                [
                        id: elementName,
                        name: elementName,
                        maxlength: 19,
                        value: "",
						style:"width:94%"
                ]
        )
    }

    private def createUpdateTextElement(dealID, elementName) {
        Deal deal = Deal.findById(dealID)
        def origXML = deal.getSignioDataModel("originalXml")
        def value = origXML.getFields().get(elementName)?.value
        return g.textField(
                [
                        id: elementName,
                        name: elementName,
                        maxlength: 19,
                        value: value,
						style:"width:94%"
                ]
        )
    }

    private def createUpdateElement(inputType, deal_id, elementName) {
        Deal deal = Deal.findById(deal_id);
        Long manufacturerID = deal.getVehicle().getVehiclemanufacturer().getId()
        def origXML = deal.getSignioDataModel("originalXml")
        Integer year = Integer.parseInt((String) origXML.getFields().get("yearOfFirstRegistration").value)
        Long modelID = deal.getVehicle().getId()
        String colour = ""
        if (origXML.getFields().get("articleColour") != null) {
            colour = (String) origXML.getFields().get("articleColour").value
        }

        def value;
        switch(elementName) {
            case "vehicleManufacturer" :
                value = manufacturerID
                break;
            case "vehicleColour" :
                value = colour
                break;
            case "vehicleYear" :
                value = year
                break;
            case "vehicleModel" :
                value = modelID
                break;
            default :
                value = ""
                break

        }

        return g.select(
                [
                        namespace: namespace,
                        name: elementName,
                        id: elementName,
                        from: getList(elementName, manufacturerID, year),
                        value: value,
                        optionKey: "code",
                        optionValue: "value",
                ]
        )
    }

    private def getList(elementName, manufacturerID, year) {
        switch(elementName) {
            case "vehicleManufacturer" :
                def vehicleManufacturers = vehicleDetailService.getVehicleManufacturers()
                return vehicleManufacturers.collect{ [code: it[0], value: it[1]] }
                break;
            case "vehicleColour" :
                def vehicleColours = vehicleDetailService.getVehicleColours()
                return vehicleColours.collect{ [code: it, value: it] }
                break;
            case "vehicleYear" :
                if (manufacturerID == null) {return []}
                def vehicleYears = vehicleDetailService.getVehicleYears(manufacturerID)
                return vehicleYears.collect{ [code: it, value: it] }
                break;
            case "vehicleModel" :
                if (manufacturerID == null || year == null) {return []}
                def vehicleModel = vehicleDetailService.getVehicleModels(manufacturerID, year)
                return vehicleModel.collect{ [code: it[0], value: it[1]] }
            break;
            default:
                return []
                break;
        }
    }
}
