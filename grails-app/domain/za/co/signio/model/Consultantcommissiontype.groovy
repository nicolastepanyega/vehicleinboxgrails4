package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/19
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */
class Consultantcommissiontype {

    String type

    static hasMany = [consultantCommissions:Consultantcommission]

    static constraints = {
        type(nullable: false, maxSize: 45)
    }


    static mapping = {
        version false
    }
}
