package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/06/11
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
class Dealdocumenttype {

    String type

    static hasMany = [dealdocuments:Dealdocument, auxProducts:AuxProduct]

    static constraints = {
        type(nullable: false, maxSize: 255)
    }

    static mapping = {
        version false
        auxProducts joinTable: 'aux_product_dealdocumenttype'
    }
}
