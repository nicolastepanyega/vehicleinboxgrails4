package za.co.signio.model

class Serviceconfiguration {

    String name
    String description
    String serviceUrl
    String tokenUrl
    String translationUri
    String username
    String password

    static hasOne = [serviceconfigurationtype:Serviceconfigurationtype, institution:Institution]
    static hasMany = [auxProducts: AuxProduct]
    static belongsTo = [AuxProduct]

    static mapping = {
        version false
        auxProducts joinTable: 'aux_product_serviceconfiguration'
    }

    static constraints = {
        name(nullable: false)
        serviceUrl(nullable: false)
        serviceconfigurationtype(nullable: false)
        translationUri(nullable: true)
        tokenUrl(nullable: true)
        description(nullable: true)
        username(nullable: true)
        password(nullable: true)
        institution(nullable: true)
    }
}


