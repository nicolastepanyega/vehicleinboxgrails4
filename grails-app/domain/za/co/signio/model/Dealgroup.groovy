package za.co.signio.model

import za.co.signio.model.Clientdetail
import za.co.signio.model.Dealgroupstate
import za.co.signio.model.Institution
import za.co.signio.model.Addendum

class Dealgroup {

    String referenceNumber
    Date dateCreated
    Date dateUpdated
    Clientdetail clientdetail
    Companydetail companydetail
    Dealgroupstate dealgroupstate
    Institution institution
    Member member

    static belongsTo = [Member, Clientdetail, Institution, Dealgroupstate, Companydetail, Note]
    static hasMany = [addendums: Addendum, deals: Deal, notes: Note, crmSharedRelationships:CrmSharedrelationship]
    static auditable = [ignore: ['version', 'lastUpdated', 'dateUpdated']]

    static mapping = {
        version false
        table 'dealgroup'
        notes joinTable: 'dealgroup_note'
    }

    static constraints = {
        referenceNumber(nullable: false, maxSize: 30)
        dateCreated nullable: true
        dateUpdated nullable: true
        clientdetail(nullable: true)
        companydetail (nullable: true)
        dealgroupstate(nullable: false)
        institution nullable: true
        member nullable: false
    }

    String toString() {
        return "${referenceNumber} - ${clientdetail.toString()}"
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateCreated field in the object before insert,
     * if the dateUpdate field is null,
     * it will also be assigned with the same date value
     */
    def beforeInsert = {
//        dateCreated = new Date()
//        if (dateUpdated == null) {
//            dateUpdated = dateCreated
//        }
        //Removed cause the DateUtil is kanzania specific there for rather added it into there code.
        //referenceNumber = new Long(DateUtil.getDate(DateUtil.REF_NO_FORMAT, new Date()))
        dateCreated = new Date()
        if ("".equals(dateUpdated) || dateUpdated == null) {
            dateUpdated = new Date()
        }
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateUpdated field in the object before update.
     */
    def beforeUpdate = {
        dateUpdated = new Date()
    }

/*    static List<Member> membersforThisDealgroup() {
        def memberList = new ArrayList()
        memberList = Member.findAll(new Member(dealgroup: this))
        return memberList
    }
    */
}
