package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmSupporttask {

    String affectedApplication
    String shortDescription
    String ticketId
    BigInteger ticketNumber
    String ticketBody
    Integer priority

    static hasMany = [crmSharedRelationships:CrmSharedrelationship]

    static constraints = {
        ticketId(nullable: true,maxSize: 45)
        ticketNumber(nullable: true)
        ticketBody(nullable: true)
        affectedApplication(nullable: true, maxSize: 45)
        shortDescription(nullable: true, maxSize: 45)
        priority(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
