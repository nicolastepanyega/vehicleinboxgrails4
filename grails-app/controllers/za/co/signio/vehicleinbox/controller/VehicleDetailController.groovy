package za.co.signio.vehicleinbox.controller



import grails.converters.JSON
import groovy.xml.MarkupBuilder
import za.co.signio.dataModel.SignioDataModel
import za.co.signio.model.Deal
import za.co.signio.model.Dealgroup;
import za.co.signio.model.Dealstate
import za.co.signio.model.Dealgroupstate
import za.co.signio.model.Vehiclemanufacturer
import za.co.signio.model.Vehicle
import za.co.signio.model.Member
import za.co.signio.model.Institution
import za.co.signio.model.Dealtype
import za.co.signio.model.Institutiontype
import za.co.signio.model.InstitutionInstitution;

class VehicleDetailController {
	
	/*p*//*rivate static final Logger LOGGER = Logger.getLogger(this);*/
	private long institutionID = 584L;

    def beforeInterceptor = [action : this.&setBrowserCompatibility]

    private setBrowserCompatibility(){
        response.addHeader("X-UA-Compatible", "IE=8")
    }

	def test(){
		render "VehicleDetailController: hello world";
	}
	
	def getVehicleDetails(){
		Map<String, String> vehicle = [:];
		
		Deal deal = Deal.get(params.id);
		if(deal != null){
			SignioDataModel sdm = deal.getSignioDataModel("originalXml");
			vehicle = [
				id: params.id,
				vin_number : sdm.fields.vinNumber?.value,
				registration_number : sdm.fields.registrationNumber?.value,
				make : deal.getVehicle().getVehiclemanufacturer().getName(),
				model : deal.getVehicle().getDescription(),
				year : sdm.fields.yearOfFirstRegistration?.value,
				colour : sdm.fields.articleColour?.value
			]
		}
		render vehicle as JSON;
	}

    def saveNewVehicle() {
        //JSONObject params = params.JSON;
        def params = request.JSON

        def dealID = params.dealID

        // Vehicle manufacturer
        Vehiclemanufacturer make = Vehiclemanufacturer.get(params.vehicleManufacturer)

        // Vehicle model
        Vehicle  model = Vehicle.get(params.vehicleModel)

        if (dealID == null || dealID == "") {
			log.info("memberID = " + session["memberID"]);
			log.info("institutionID = " + session["institutionID"]);

            // Get dealGroupState
            def dealGroupState = Dealgroupstate.findByState("Created")
            Institution dealerInstitution = (Institution) Institution.get(session["institutionID"]);

            def dealGroup = new Dealgroup();
            dealGroup.setDealgroupstate(dealGroupState);
            Long batchRef = System.nanoTime()
            dealGroup.setReferenceNumber(batchRef.toString());
            dealGroup.setMember(Member.get(session["memberID"]));
            dealGroup.setInstitution(dealerInstitution);

            dealGroup.save(flush: true, failOnError: true);

			// Build the OrignialSDM
			Writer writer = new StringWriter();
			MarkupBuilder builder = new MarkupBuilder(writer);
			builder.field(formFieldName:"vinNumber", mapping:"68", params.vinNumber);
			builder.field(formFieldName:"registrationNumber", mapping:"66", params.registrationNumber);
			builder.field(formFieldName:"model", mapping:"219", model?.description);
			builder.field(formFieldName:"make", mapping:"218", make?.name);
			builder.field(formFieldName:"yearOfFirstRegistration", mapping:"60", params.vehicleYear);
			builder.field(formFieldName:"articleColour", mapping:"64", params.vehicleColour);
            builder.field(formFieldName:"natisRegisteredNumber", mapping: "-1", params.natisRegisteredNumber);
			String originalXML = "<signioPackage><dataBundle>" + writer.toString() + "</dataBundle></signioPackage>";

            Institution providerInstitution = Institution.findByNameAndInstitutiontype('Ubiquitech', Institutiontype.findByType('Service Provider'))

            Deal deal = new Deal();
            deal.setOriginalXml(originalXML);
            deal.setInstitution(providerInstitution)
            deal.setVehicle(model)
            deal.setDealgroup(dealGroup)
            deal.setDealstate(Dealstate.findByStateCode(60000))
            deal.setMember(Member.get(session["memberID"]))
            deal.setDealtype(Dealtype.findByType("VehicleInbox"))
            deal.setIsResendable(true)
            //println("UBIQ-> ${providerInstitution.id}:${providerInstitution.name};\n DEALER->${dealerInstitution.id}: ${dealerInstitution.name}")
            deal.institutionInstitution = InstitutionInstitution.findByInstitution1AndInstitution2(providerInstitution, dealerInstitution)
            deal.setDateUpdated(new Date())
            deal.save(flush: true, failOnError: true)
            //RegTrackHelper.insertDealData(deal)
			
			log.info("Vehicle Created - dealID:" + deal.id);
			dealID = deal.id;
        } 
		else {
            Deal deal = Deal.get(dealID)
            deal.setVehicle(model)

            if (deal.institutionInstitution == null) {
                deal.institutionInstitution = InstitutionInstitution
                        .findByInstitution1AndInstitution2(
                        Institution.findByNameAndInstitutiontype('Ubiquitech',
                                Institutiontype.findByType('Service Provider')),
                        (Institution) Institution.get(session["institutionID"]))
            }
            
			// Build the OrignialSDM
			Writer writer = new StringWriter();
			MarkupBuilder builder = new MarkupBuilder(writer);
			builder.field(formFieldName:"vinNumber", mapping:"68", params.vinNumber);
			builder.field(formFieldName:"registrationNumber", mapping:"66", params.registrationNumber);
			builder.field(formFieldName:"model", mapping:"219", model?.description);
			builder.field(formFieldName:"make", mapping:"218", make?.name);
			builder.field(formFieldName:"yearOfFirstRegistration", mapping:"60", params.vehicleYear);
			builder.field(formFieldName:"articleColour", mapping:"64", params.vehicleColour);
            builder.field(formFieldName:"natisRegisteredNumber", mapping: "-1", params.natisRegisteredNumber);
			String originalXML = "<signioPackage><dataBundle>" + writer.toString() + "</dataBundle></signioPackage>";
			
            deal.setOriginalXml(originalXML)
            deal.save(flush: true, failOnError: true)
            //RegTrackHelper.insertDealData(deal)
        }

        //chain(controller: "vehicleInbox", action: "index")
        def result = [success:true, dealID: dealID]
        render result as JSON
    }
	
	def getVehicleVinNumbers(){
		List vinList = new LinkedList();
		for(Deal deal : Deal.createCriteria().list{ 
			dealtype { 
				eq("type", "VehicleInbox")}
			dealgroup {
				institution{
					eq("id", session["institutionID"])
				}
			}
			order("dateCreated", "desc")
		}){
			vinList.add([key: deal.id, value:deal.getSignioDataModel("originalXml").fields.vinNumber?.value])
		}
		render vinList as JSON;
	}

    def updateVehicle() {

    }

    def newVehicle() { }
}
