package za.co.signio.model

class TemplateMapping {
    String name
    String uri
    String comment
    String type

    static mapping = {
        version false
    }
}
