package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/10
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
class Institutionmembersupportingdocaccess {

    Member member
    Institution institution
    Supportingdocs supportingdocs
    Boolean isLiveScan
    Boolean isIxScan
    Clientdetail clientdetail
    Companydetail companydetail

    static belongsTo = [Member, Institution, Supportingdocs, Clientdetail, Companydetail]

    static constraints = {
        member(nullable: false)
        institution(nullable: false)
        supportingdocs(nullable: false)
        isLiveScan(nullable: true)
        isIxScan(nullable: true)
        clientdetail(nullable: true)
        companydetail(nullable: true)
    }


    static mapping = {
        version false
    }


}
