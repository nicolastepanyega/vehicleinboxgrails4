package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/14
 * Time: 1:14 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxFspnumber {

    String fspNumber
    String fspName
    Contactinfo contactinfo
    String financialOfficerName
    String financialOfficerContactNumber
    String financialOfficerFaxNumber
    String financialOfficerEmailAddress
    String financialOfficerHolds

    static hasMany = [auxFspCategoryInstitutionConfigs:AuxFspcategoryinstitutionconfig,
                      auxInstitutionFspConfigs:AuxInstitutionfspconfig,
                      relationships:Relationship, documenttemplategroupInstitutions:DocumenttemplategroupInstitution]

    static constraints = {
        fspNumber(nullable: false, maxSize: 45)
        fspName(nullable: true, maxSize: 100)
        contactinfo(nullable: true)
        financialOfficerName(nullable: true, maxSize: 45)
        financialOfficerContactNumber(nullable: true, maxSize: 45)
        financialOfficerFaxNumber(nullable: true, maxSize: 45)
        financialOfficerEmailAddress(nullable: true, maxSize: 100)
        financialOfficerHolds(nullable: true, maxSize: 45)
    }

    static mapping = {
        version false
    }

    public String toString(){
        fspNumber
    }
}
