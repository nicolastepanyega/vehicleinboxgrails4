package za.co.signio.model

class MemberPreference implements Serializable {
    Member member
    Preference preference
    String value

    static hasOne = [member:Member, preference:Preference]
    static auditable = true

    static mapping = {
        id composite: ['member', 'preference']
        version false
    }

    static constraints = {
        value maxSize: 255, nullable: false
    }

    String toString() {
        return value
    }
}
