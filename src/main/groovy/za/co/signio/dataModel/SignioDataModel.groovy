package za.co.signio.dataModel

import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder


/**
 * Created by IntelliJ IDEA.
 * User: lindsayc
 * Date: 2012/03/18
 * Time: 9:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SignioDataModel {
    def fields = [:].sort({ a, b -> a <=> b } as Comparator)
    def rawFields = [:].sort({ a, b -> a <=> b } as Comparator)
    def xml = null

    /**
     * Returns the SDM
     *
     * @param transformedXML
     */
    /**
     * Returns the SDM
     *
     * @param transformedXML
     */
    public SignioDataModel(transformedXML) {
        xml = new XmlSlurper().parseText(transformedXML);
        def fieldsCollection = xml.dataBundle.field

        fieldsCollection.each { field ->
            if (field.text() != "") {
                if (field.subField.size() > 0) {
                    def subFields = [:]
                    field.subField.each { subField ->
                        subFields.put((subField?.@formFieldName?.text()), [value: subField?.text(), formFieldName: subField?.@formFieldName?.text(), originalName: subField?.@originalName?.text(), originalValue: subField?.@originalValue?.text()])
                    }
                    /**
                     * Check if the key alreay exists
                     * if so increment the field count to +1
                     */
                    if (fields.find { k, v -> k.equals("${field?.@formFieldName?.text()}") }) {
                        fields.get("${field?.@formFieldName?.text()}").list.add(subFields)
                    } else {
                        fields.put("${field?.@formFieldName?.text()}", [list: [subFields], formFieldName: field?.@formFieldName?.text()])
                    }
                } else {
                    fields.put("${field?.@formFieldName?.text()}", [value: field?.text(), formFieldName: field?.@formFieldName?.text(),originalName: field?.@originalName?.text(), originalValue: field?.@originalValue?.text()])
                }
            }
        }
    }

    public GPathResult getRawXml(){
        return xml
    }


    public String toXml() {
        final def valueKey = "value"
        final def listKey = "list"

        def tempList
        def tempValue
        def tempAttributes
        def tempSubfieldValue

        def writer = new StringWriter()
        def signioDataModelString = new MarkupBuilder(writer)

        signioDataModelString.signioPackage {
            dataBundle {
                this.fields.each { f ->
                    tempAttributes = f.value
                    tempValue = tempAttributes.value
                    tempAttributes.remove(valueKey)

                    if (tempAttributes.list) {

                        tempList = tempAttributes.list
                        tempAttributes.remove(listKey)

                        tempList.each { sf ->
                            field(tempAttributes) {
                                sf.each { ssf ->
                                    tempSubfieldValue = ssf.value.value
                                    ssf.value.remove(valueKey)

                                    subField(ssf.value, tempSubfieldValue)
                                }
                            }
                        }

                    } else {
                        field(tempAttributes, tempValue)
                    }
                }
            }
        }
        return writer.toString()
    }

    public String toJson(){

        def fieldValue
        Map fields = [:]

        this.fields.each{ field ->
            fieldValue = field.value
            if (field.value.list){

                field.value.list.each {
                    def subFields = [:]
                    it.each {
                        subFields.put(it.key, it.value.value)
                    }
                    if (!fields.get("${field.value.formFieldName}".toString())){
                        fields.put("${field.value.formFieldName}".toString(), [])
                    }
                    fields.get("${field.value.formFieldName}".toString()).add(subFields)
                }
            }else{
                fields.put(field.value.formFieldName, field.value.value)
            }
        }

        def jsonBuilder = null;//new JsonBuilder(fields)

        return jsonBuilder.toPrettyString()
    }

    public SignioDataModel(Map map){

        map.each { k, v ->

            if (v.class == ArrayList){
                def subFields = [:]
                v.each { subField ->
                    subFields.put(k, [value: subField.value, formFieldName: subField.key, mapping: null, originalName: null, originalValue: null])
                }
                if (fields.find { it.key.equals(k) }) {
                    fields.get(k).list.add(subFields)
                } else {
                    fields.put(k, [list: [subFields], formFieldName: k, mapping: null])
                }
            }
            fields.put(k, [value: v, formFieldName: k, mapping: null, originalName: null, originalValue: null])
        }
    }
}