package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/03/09
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxComment {

    String value
    String note
    Date dateCreated
    Integer memberId

    static hasOne = [auxProduct: AuxProduct, auxProductoption: AuxProductoption]

    static belongsTo = [AuxProduct, AuxProductoption]

    static constraints = {
        value(maxSize: 255, nullable: false)
        note(maxSize: 500, nullable: true)
        dateCreated(nullable: false)
        memberId(nullable: false)
        auxProductoption(nullable: true)
    }

    static mapping = {
        version false
    }
}
