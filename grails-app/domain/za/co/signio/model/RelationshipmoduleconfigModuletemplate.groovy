package za.co.signio.model

class RelationshipmoduleconfigModuletemplate {
    Boolean isReadonly

    static hasOne = [moduletemplate:Moduletemplate,relationshipmoduleconfig:Relationshipmoduleconfig]
    static belongsTo = [Relationshipmoduleconfig]

    static mapping = {
        version false
    }

    static constraints = {
        isReadonly nullable: false
    }
}
