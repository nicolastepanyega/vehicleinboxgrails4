package za.co.signio.model

class Uniquenumbergenerator {

    String forApplication
    String prefix
    Long maxNumber

    static constraints = {
        forApplication nullable: false, maxSize: 45
        prefix nullable: false, maxSize: 45
        maxNumber nullable: false
    }

    static mapping = {
        version false
    }
}
