package za.co.signio.model

import za.co.signio.dataModel.SignioDataModel

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/07/05
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
class Dealdata{

    String originalXml
    String transformedXml
    Date dateCreated
    String comment
    Deal deal
    Dealstate dealstate

    static belongsTo = [Deal]

    static constraints = {
        originalXml nullable: false
        transformedXml(nullable: true)
        dateCreated(nullable: false)
        comment(nullable: true)
        deal(nullable: false)
        dealstate(nullable: false)
    }

    static mapping = {
        version(false)
    }

    /**
     * get the Signio Data Model defaults to the transformedXml
     * @param SDMSource
     * @return Xml String
     */
    def getSignioDataModel(String SDMSource)
    {
        switch(SDMSource)
        {
            case "originalXml":
                return(new SignioDataModel(originalXml))
                break;
            case "transformedXml":
                return(new SignioDataModel(transformedXml))
                break;
            default:
                return(new SignioDataModel(transformedXml))
                break;
        }
    }
}
