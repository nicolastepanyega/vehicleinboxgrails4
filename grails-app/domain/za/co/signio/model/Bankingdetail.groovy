package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/06/17
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
class Bankingdetail {

    String accountHolderName
    String bankName
    String accountType
    String accountNumber
    String branchCode


    static hasMany = [institutions:Institution]



    static constraints = {
        accountHolderName nullable: false, maxSize: 45
        bankName nullable: false, maxSize: 45
        accountType nullable: false, maxSize: 45
        accountNumber nullable: false, maxSize: 45
        branchCode nullable: false, maxSize: 45
    }

    static mapping = {
        version false
    }

}
