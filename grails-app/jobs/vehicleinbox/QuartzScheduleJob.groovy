package vehicleinbox

import java.text.SimpleDateFormat

import za.co.signio.model.Vehicledetail

//import za.co.ubiquitech.ws.certrack.core.wsclient.CertrackCoreServiceSoap11Stub
//import za.co.ubiquitech.ws.certrack.core.wsclient.CertrackCoreServiceServiceLocator
import za.co.signio.model.Request
//import za.co.ubiquitech.ws.certrack.core.DealerActivityRequestStatusQueryResponse
//import za.co.ubiquitech.ws.certrack.core.DealerActivityRequestStatusQueryRequest
//import za.co.ubiquitech.ws.certrack.core.ActivityRequestStatusSuccessElementRequestStatus

@Deprecated
class QuartzScheduleJob {

    def grailsApplication;
	def supportNotificationService;

    public final int TIMEOUT_TIME = 3 * 60 * 1000; // 3 minutes
    public static final int REPEAT_MINUTES =360//180;//15; // 15 minutes

    //                  authentication  = "SIGNIO-WES:v3h1cl31nbx";
    public  String CERTRACK_LOGON = "SIGNIO-WES";
    public  String CERTRACK_PASSWORD = "v3h1cl31nbx";
    public  String CERTRACK_URL = "https://qa.ubiquitech.co.za/wesbank/ws/certrackCoreService.wsdl;"

    public String WS_URL_WESBANK ="https://qa.ubiquitech.co.za/wesbank/ws/certrackCoreService.wsdl";
    public String WS_URL_ABSA    ="https://qa.ubiquitech.co.za/wesbank/ws/certrackCoreService.wsdl";
    public String WS_URL_MFC 	   ="https://qa.ubiquitech.co.za/wesbank/ws/certrackCoreService.wsdl";
    public String WS_URL_SBSA    ="https://qa.ubiquitech.co.za/wesbank/ws/certrackCoreService.wsdl";

    static triggers = {
    }
	
    def group = "MyGroup"
	
    def execute(){

    }

/*
    def _executeOriginalNatisStatus(){
     
        int dateRange=90; // days

        Date earliestDate=dateMinusDays(dateRange);
        String startDate  =now(earliestDate);
        println("Start date : $startDate ")

        Request request=null;

        String requestReference=null;
        String originalNatisStatus;

        DealerActivityRequestStatusQueryResponse theResponse;
        DealerActivityRequestStatusQueryRequest  theRequest;
        ActivityRequestStatusSuccessElementRequestStatus[] success;
        String externalRequestReference;// ="2013040101";//null;
        String errorMsg=null;

        String statusDateAndTime
        String statusComment
        String statusStatus


        int sentErrorEmail=0;

        List <Vehicledetail> vehicledetailsUnsorted =Vehicledetail.findAllByDateCreatedGreaterThan(earliestDate, [sort: "dateCreated", order: "desc"]) ;
        for( Vehicledetail vehicledetailInstance : vehicledetailsUnsorted){
            requestReference=vehicledetailInstance.requestReference
            originalNatisStatus =vehicledetailInstance.originalNatisStatus;

            request =Request.findByVehicledetail(vehicledetailInstance)

            externalRequestReference=vehicledetailInstance.externalRequestReference;

            if(requestReference!= null && externalRequestReference!= null){
                try {
                    println("Setting theRequest $requestReference , $originalNatisStatus. $externalRequestReference ");
                    theRequest=new DealerActivityRequestStatusQueryRequest(requestReference,externalRequestReference);

                    println("CertificateQueryRequest : \n"+theRequest.toString());
                    println("\nCalling the WS for Status Updates: "+now()+"\n")
//                    CertificateQueryResponse certificateQuery(CertificateQueryRequest certificateQueryRequest)
                    theResponse = binding.dealerActivityRequestStatusQuery(theRequest);
                    println("Successfully called the WS $requestReference $externalRequestReference ") ;
                } catch (Exception e1) {
                    println("ERROR:Failed to call OriginatNatisService with error");
                    println(e1.cause+" "+e1.message+"\n"+e1.stackTrace);

                    if(sentErrorEmail==0){
                        sentErrorEmail++;
                        String eBody  = "Please get someone to verify there is no problem with the scheduled CERTRAC ORIGINATNATISSERVICE service "
						supportNotificationService.notifySupport("VI_SCD", "FATAL ERROR :Failed to call  CERTRAC OriginatNatisService", eBody);
                    }
                }

                if(theResponse!=null){
                    println("succeded to get theResponse (not null) now getting result values")
                    errorMsg=theResponse.error;
                    success=theResponse.success

                    if(errorMsg!=null){
                        println("Got Error Message from service : "+errorMsg)
                    }

                    String theRequestStatus;

                    println("Got resultElement")
                    if(success!=null){
                        println("retrieved success elements (not null) processing success elements")

                        try {

                            int len =success.length;
                            println("retrieved success elements size : $len");
                            ActivityRequestStatusSuccessElementRequestStatus status;

                            for(int i=0;i<len ; i++){
                                statusComment=success[i].comment
                                println(" statusComment : $statusComment")

                                statusStatus=success[i].status
                                println(" statusStatus : ->$statusStatus<-")

                                statusDateAndTime= now()


                                if(statusDateAndTime!=null){
                                    println("statusDateAndTime : $statusDateAndTime  status : "+statusStatus+" comment: "+statusComment);
                                }


                                break;

                            }

                        } catch (Exception e2) {
                            println("ERROR:Failed to set theRequestStatus ")
                            println(e2.cause+" "+e2.message+"\n"+e2.stackTrace);
                        }



                        if(statusStatus!=null && originalNatisStatus!=null ){
                            try {
                                println("Updating originalNatisStatus to $statusStatus")
                                vehicledetailInstance.originalNatisStatus =statusStatus
                            } catch (Exception e2) {
                                println("ERROR:Failed to update vehicledetailInstance.originalNatisStatus  ")
                                println(e2.cause+" "+e2.message+"\n"+e2.stackTrace);
                            }

                            try {
                                println("Updating in database : vehicledetailInstance : "+vehicledetailInstance.id)
                                vehicledetailInstance.save();
                            } catch (Exception e2) {
                                println("ERROR:Failed to update vehicledetailInstance in database")
                                println(e2.cause+" "+e2.message+"\n"+e2.stackTrace);
                            }
                        }


                    } else {
                        println("success elements array was null")
                    }
                }
            }
        }
    }
*/
}
