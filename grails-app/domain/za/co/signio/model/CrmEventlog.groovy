package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmEventlog {

    Date dateCreated
    Date eventDate
    Date dateUpdated
    CrmEventlogtype crmEventlogtype
    Member member
    Status status
    Institution institution
    CrmSharedrelationship crmSharedrelationship
    CrmEmail crmEmail
    Integer parentId

    static belongsTo = [CrmEventlogtype, Member, Status, Institution, CrmSharedrelationship, CrmEmail]
    static hasMany = [crmEventauditlog:CrmEventauditlog, crmDocument:CrmDocument]

    static constraints = {
        dateCreated(nullable: true)
        eventDate(nullable: true)
        dateUpdated(nullable: true)
        crmEventlogtype(nullable: false)
        member(nullable: false)
        status(nullable: false)
        institution(nullable: false)
        crmSharedrelationship(nullable: true)
        crmEmail(nullable: true)
        parentId(nullable: true)
    }

    static mapping = {
        version(false)
    }
    /**
     * This method automatically assigns the current date and time
     * to the dateCreated field in the object before insert.
     */
    def beforeInsert = {
        dateCreated = new Date()
        dateUpdated = new Date()
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateUpdated field in the object before update,
     * and also updates the dateUpdated field in its relating dealgroup record.
     */
    def beforeUpdate = {
        dateUpdated = new Date()

        try{
            CrmEventauditlog crmEventauditlog=new CrmEventauditlog()
            crmEventauditlog.dateCreated = this.getPersistentValue('dateUpdated')
            crmEventauditlog.member = this.getPersistentValue('member')
            crmEventauditlog.institution = this.getPersistentValue('institution')
            crmEventauditlog.status = this.getPersistentValue('status')
            crmEventauditlog.crmEventlogtype = this.getPersistentValue('crmEventlogtype')
            crmEventauditlog.crmSharedrelationship = this.getPersistentValue('crmSharedrelationship')
            crmEventauditlog.crmEventlog = this
            crmEventauditlog.save()
        }catch(Exception ex){
            println("Gorm Error:${ex.toString()}")
        }

    }

}
