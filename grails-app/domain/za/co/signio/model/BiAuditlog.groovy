package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/15/12
 * Time: 5:31 PM
 * To change this template use File | Settings | File Templates.
 */
class BiAuditlog {
    
    Integer institutionId
    Integer memberId
    String memberDetail
    Integer reportId
    String reportReference
    String reportFrequency
    String comments
    Date dateCreated

    static hasOne = [biAuditlogaction: BiAuditlogaction]

    static belongsTo = [BiAuditlogaction]

    static constraints = {
        institutionId nullable: true
        memberId(nullable: false)
        memberDetail(nullable: true, maxSize: 255)
        reportId(nullable: true)
        reportReference(nullable: true, maxSize: 255)
        reportFrequency(nullable: true, maxSize: 45)
        comments(nullable: true)
        dateCreated(nullable: true)
        biAuditlogaction nullable: false
    }

    static mapping = {
        version false
    }
    
}
