package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/14
 * Time: 1:08 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxFspcategoryinstitutionconfig {

    static hasOne = [institution: Institution,
                     auxProductcategory: AuxProductcategory,
                     auxFspnumber: AuxFspnumber]

    static belongsTo = [Institution, AuxFspnumber, AuxProductcategory]

    static constraints = {
        institution(nullable: false)
        auxFspnumber(nullable: false)
        auxProductcategory(nullable: false)
    }

    static mapping = {
        version false

    }

    public String toString(){
        "${institution?.name} >> ${auxProductcategory?.productCategoryName}"
    }
}
