package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmSharedrelationship {

    Member member
    Institution institution
    Status status
    CrmDealernote crmDealernote
    CrmCalendar crmCalendar
    CrmSupporttask crmSupporttask
    CrmComplaint crmComplaint
    Dealgroup dealgroup

    static hasMany = [crmEventLogs:CrmEventlog]

    static belongsTo = [Member, Institution, Status, CrmDealernote, CrmCalendar, CrmSupporttask, CrmComplaint, Dealgroup]


    static constraints = {
        member(nullable: true)
        institution(nullable: false)
        status(nullable: true)
        crmDealernote(nullable: true)
        crmCalendar(nullable: true)
        crmSupporttask(nullable: true)
        crmComplaint(nullable: true)
        dealgroup(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
