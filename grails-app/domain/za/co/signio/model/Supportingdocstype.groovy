package za.co.signio.model

class Supportingdocstype {

	String type
    Integer sequence
    Boolean isGeneric
    String label

    static hasMany = [supportingDocs:Supportingdocs, profiledocuments: Profiledocument]

	static mapping = {
		version false
	}

	static constraints = {
		type maxSize: 45, nullable: false
        sequence nullable: false
        isGeneric nullable: false
        label maxSize:  255, nullable: true
	}

    def String toString()
    {
      return type
    }
}
