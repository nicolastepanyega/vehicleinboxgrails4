package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/06/11
 * Time: 2:14 PM
 * To change this template use File | Settings | File Templates.
 */
class CrmEmail {

    String subject
    String body
    Date eventDate
    Boolean isFinalised
    String recipientLookup

    static hasMany = [crmEventLogs:CrmEventlog,crmEmailAuditLogs:CrmEmailauditlog]

    static constraints = {
        subject(nullable: false, maxSize: 45)
        body(nullable: false)
        eventDate(nullable: true)
        isFinalised(nullable: false)
        recipientLookup(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
