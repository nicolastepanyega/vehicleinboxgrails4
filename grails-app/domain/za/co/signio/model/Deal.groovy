package za.co.signio.model

import za.co.signio.dataModel.SignioDataModel

/**
 *
 */
class Deal {

    String batchRef
    String originalXml
    String transformedXml
    Date dateCreated
    Date dateUpdated
    Boolean isResendable
    String comment
    Integer parentId
//    Vehicle vehicle
//    Institution institution
    Dealstate dealstate
     Dealtype dealtype
//    Dealgroup dealgroup
//    String SDMSource = "transformedXml"
//    SignioDataModel getSignioDataModel(){getSDM(SDMSource)}
//    Member member
    Processingcentrearea processingcentrearea
    InstitutionInstitution institutionInstitution

    //static hasOne = [vehicle:Vehicle, institution:Institution, dealstate:Dealstate, dealtype:Dealtype, dealgroup:Dealgroup, member:Member]
    //static hasOne = [vehicle:Vehicle, institution:Institution, dealstate:Dealstate, dealgroup:Dealgroup, member:Member]
    static hasOne = [vehicle:Vehicle, institution:Institution, dealgroup:Dealgroup, member:Member]
    static hasMany = [dealDocuments:Dealdocument, docs: Doc, dealMessages: Dealmessage, dealDatas:Dealdata, moduleSpecificFields:Modulespecificfield, notes:Note]
    static belongsTo = [Dealgroup, Member, Dealstate, Dealtype, Institution, Vehicle, Processingcentrearea, InstitutionInstitution, Note]
    static auditable = [ignore: ['version', 'lastUpdated', 'dateUpdated']]

    static mapping = {
        //DISABLE DIRTY CHECKING
        version false

        //DISABLE DYNAMIC UPDATING OF ALL COLUMNS BY DEFAUL
        dynamicUpdate false

        table 'deal'
        notes joinTable: 'deal_note'

//        auxProductoptions joinTable: 'deal_aux_productoptions'
    }

    static constraints = {
        batchRef nullable: true, maxSize: 45
        originalXml nullable: true
        transformedXml nullable: true
        dateCreated nullable: true
        dateUpdated nullable:  true
        isResendable nullable: false
        comment nullable: true
        parentId nullable: true
        institution nullable: false
        dealstate nullable: false
        dealtype nullable: false
        dealgroup nullable: false
        member nullable: true
        vehicle nullable: true
        processingcentrearea nullable: true
        institutionInstitution nullable: false
    }

//    static transients = ['signioDataModel', 'SDMSource']

    /**
     * This method automatically assigns the current date and time
     * to the dateCreated field in the object before insert.
     */
    def beforeInsert = {
        dateCreated = new Date()
        dateUpdated = new Date()
    }

    /**
     * This method automatically assigns the current date and time
     * to the dateUpdated field in the object before update,
     * and also updates the dateUpdated field in its relating dealgroup record.
     */
    def beforeUpdate = {

//        dateUpdated = new Date()
//        try {
//
//            Dealgroup dealgroup =  Dealgroup.findById(this.dealgroup.id)
//
//            if (TimeCategory.minus(dateUpdated,dealgroup.dateUpdated).minutes>1) {
//                dealgroup.dateUpdated = dateUpdated
//                dealgroup.save()
//            }
//
//        }catch (Exception e){
//            println("Error updating dealgroup: ${e.message}")
//        }

        try{
            Dealdata dealdata=new Dealdata()
            dealdata.originalXml = (this.getPersistentValue('originalXml')!=null?this.getPersistentValue('originalXml'):"<BLANK/>")
            dealdata.transformedXml = (this.getPersistentValue('transformedXml')!=null?this.getPersistentValue('transformedXml'):"<BLANK/>")
            dealdata.dateCreated = this.getPersistentValue('dateUpdated')
            dealdata.comment = this.getPersistentValue('comment')
            dealdata.deal = this
            dealdata.dealstate = this.getPersistentValue('dealstate')
            dealdata.save()
        }catch(Exception ex){
            println("Gorm Error:${ex.toString()}")
        }

    }

    String toString() {
        return batchRef
    }

    def onLoad = {
        //println("GORM(Deal) Result: ${this.id}");

    }

    /**
     * Determines the location of the signio data model and returns it.
     * @return Xml String
     */
    def getSignioDataModel(){
        return (originalXml?.contains("signioPackage")) ? getSignioDataModel("originalXml") : getSignioDataModel("transformedXml")
    }

    /**
     * get the Signio Data Model defaults to the transformedXml
     * @param SDMSource
     * @return Xml String
     */
    def getSignioDataModel(String SDMSource)
    {
        switch(SDMSource)
        {
            case "originalXml":
                return(new SignioDataModel(originalXml))
                break;
            case "transformedXml":
                return(new SignioDataModel(transformedXml))
                break;
            default:
                return(new SignioDataModel(transformedXml))
                break;
        }

    }


}

