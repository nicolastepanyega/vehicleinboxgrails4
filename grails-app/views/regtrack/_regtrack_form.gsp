<tr>
    <td class="BGColourWhite" >
        <g:form action="createRegTrackRequestFromPOST" id="regTrackForm">
            <table style="width:98%" class="responsive BGColourDarkBlue">
                <g:render template="/regtrack/dealer_details" />
                <g:render template="/regtrack/vehicle_details" />
                <g:if test="${clientOrJuristic == 'Client'}">
                    <g:render template="/regtrack/client_details" />
                </g:if>
                <g:if test="${clientOrJuristic == 'Juristic'}">
                    <g:render template="/regtrack/company_details" />
                </g:if>
                <g:if test="${clientOrJuristic == 'Foreigner'}">
                    <g:render template="/regtrack/client_details_foreign" />
                </g:if>
                <g:render template="additional_details" />
            </table>
        </g:form>
    </td>
</tr>

<script type="text/javascript">

    if (typeof signio === 'undefined') {
        var signio = {}
    }

    signio.vehicleInbox = {
        callAjax:function (url, type, data, success) {
            $.ajax({
                url:url,
                contentType:'application/javascript; charset=utf-8',
                async:false,
                type:type,
                data:data,
                error:function (data) {
                    //	console.log("could not retrieve data");
                },
                success:success
            });
        }
    };

    window.onload = function () {
        if ($("#clientOrJuristic").val() == 'Juristic') {
            var message = '<div>'
                    + '<p>Your Application has been identified as a Juristic Application.</p>'
                    //+ '<br />'
                    + '<p>Please provide the <b>Natis Business Registration Number</b>'
                    + '<br /> and <b>Natis Control Number</b> in order to'
                    + '<br />request Regtrack.</p>'
                    + '<p>Please find these values on the'
                    + '<br /><b>Business Registration Number Certificate.</b></p>'
                    + '</div>';

            $('<div></div>').appendTo('body')
                    .html(message)
                    .dialog({
                        modal: true,
                        title: 'Juristic Notification',
                        zIndex: 10000,
                        autoOpen: true,
                        width: 'auto',
                        resizable: false,
                        buttons: {
                            OK: function () {
                                $(this).dialog("close");
                            }
                        },
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    });
        }

        $("#submitPostRequestButton").removeAttr('disabled');
        $("#submitPostRequestButton").show();
        $("#returnToVehicleInboxButton").hide();

        $("#submitPostRequestButton").click(function(){
            // Vehicle Detail
            var vinNo = $("#vinNumber").val();
            var regNo = $("#registrationNumber").val();
            var mmCode = $("#mmCode").val();
            var year = $("#yearOfFirstRegistration").val();
            var newUsed = $("#newOrUsed").val();
            var clientJuristic = $("#clientOrJuristic").val();

            // Client Details
            var customerTitle = ($("#customerTitle") == null)? "" : $("#customerTitle").val();
            var customerFirstName = ($("#customerFirstName") == null)? "" : $("#customerFirstName").val();
            var customerSurname = ($("#customerSurname") == null)? "" : $("#customerSurname").val();
            var customerIdNumber = ($("#customerIdNumber") == null)? "" : $("#customerIdNumber").val();
            var customerDateOfBirth = ($("#customerDateOfBirth") == null)? "" : $("#customerDateOfBirth").val();
            var customerEmail = ($("#customerEmail") == null)? "" : $("#customerEmail").val();
            var customerPhoneNumber = ($("#customerPhoneNumber") == null)? "" : $("#customerPhoneNumber").val();
            var customerResidentialAddress = ($("#customerResidentialAddress") == null)? "" : $("#customerResidentialAddress").val();
            var customerPostalAddress = ($("#customerPostalAddress") == null)? "" : $("#customerPostalAddress").val();
            var customerBankAccountNumber = ($("#customerBankAccountNumber") == null)? "" : $("#customerBankAccountNumber").val();

            // Juristic Details
            var companyName = ($("#companyName") == null)? "" : $("#companyName").val();
            var businessRegistrationNumber = ($("#businessRegistrationNumber") == null)? "" : $("#businessRegistrationNumber").val();
            var companyEmail = ($("#companyEmail") == null)? "" : $("#companyEmail").val();
            var companyPhoneNumber = ($("#companyPhoneNumber") == null)? "" : $("#companyPhoneNumber").val();
            var companyResidentialAddress = ($("#companyResidentialAddress") == null)? "" : $("#companyResidentialAddress").val();
            var companyPostalAddress = ($("#companyPostalAddress") == null)? "" : $("#companyPostalAddress").val();
            var companyBankAccountNumber = ($("#companyBankAccountNumber") == null)? "" : $("#companyBankAccountNumber").val();
            var natisBusinessRegistrationNumber = ($("#natisBusinessRegistrationNumber") == null)? "" : $("#natisBusinessRegistrationNumber").val();
            var natisControlNumber = ($("#natisControlNumber") == null)? "" : $("#natisControlNumber").val();

            // Dealer Details
            var dealerCode = $("#dealerCode").val();
            var institutionID = $("#institutionID").val();
            var bank = $("#bank").val();
            var thirdPartyID = $("#thirdPartyID").val();
            var memberIDNo = $("#memberId").val();
            var batchRef = $("#batchRef").val();

            // Additional Details
            var usedOnPublicRoad = $("#regtrackUsedOnPublicRoad").val();
            var vehicleUsage = $("#regtrackVehicleUsage").val();
            var economicSector = $("#regtrackEconomicSector").val();
            var natureOfOwnership = $("#regtrackNatureOfOwnership").val();
            // var reasonForRegistration = $("#regtrackReasonForRegistration").val();
            var reasonForRegistration = '01';

            var validationFailed = false;

            var validationMap = {
                "regtrackEconomicSector":economicSector,
                "regtrackNatureOfOwnership":natureOfOwnership,
                "regtrackReasonForRegistration":reasonForRegistration,
                "newOrUsed": newUsed
            };

            if ($("#clientOrJuristic").val() == 'Juristic') {
                validationMap["natisBusinessRegistrationNumber"] = natisBusinessRegistrationNumber;
                validationMap["natisControlNumber"] = natisControlNumber;
            }

            if ($("#clientOrJuristic").val() == 'Foreigner') {
                validationMap["customerIdNumber"] = customerIdNumber;
            }

            $.each(
                    validationMap,
                    function(i, val) {

                        if (val == null || val == -1 || val == '--Select--' || $.trim(val) == '') {
                            $("#" + i).css({backgroundColor: '#CC9999',
                                cursor: 'default'});
                            validationFailed = true
                        }
                    });

            if (validationFailed) {
                alert("Validation failed.\nPlease complete the indicated fields.");
            } else {
                signio.vehicleInbox.callAjax(
                        "../regtrack/createRegTrackRequestFromPOST",
                        "GET",
                        {
                            // Vehicle Details
                            vinNumber:vinNo,registrationNumber:regNo, mmCode:mmCode, yearOfFirstRegistration:year, newOrUsed:newUsed, clientOrJuristic:clientJuristic,
                            // Client Details
                            customerFirstName:customerFirstName, customerSurname:customerSurname, customerTitle:customerTitle,
                            customerIdNumber:customerIdNumber, customerDateOfBirth:customerDateOfBirth, customerEmail:customerEmail, customerPhoneNumber:customerPhoneNumber,
                            customerResidentialAddress:customerResidentialAddress, customerPostalAddress:customerPostalAddress, customerBankAccountNumber:customerBankAccountNumber,
                            // Juristic Details
                            companyName: companyName, businessRegistrationNumber: businessRegistrationNumber, companyEmail: companyEmail,
                            companyPhoneNumber: companyPhoneNumber, companyResidentialAddress: companyResidentialAddress,
                            companyPostalAddress: companyPostalAddress, companyBankAccountNumber: companyBankAccountNumber,
                            natisBusinessRegistrationNumber: natisBusinessRegistrationNumber, natisControlNumber: natisControlNumber,
                            // Dealer Details
                            dealerCode:dealerCode, batchRef:batchRef,  thirdPartyID:thirdPartyID, bank: bank, memberId:memberIDNo,
                            institutionID:institutionID,
                            // RegTrack Details
                            usedOnPublicRoad:usedOnPublicRoad, vehicleUsage:vehicleUsage, economicSector:economicSector,
                            natureOfOwnership:natureOfOwnership, reasonForRegistration:reasonForRegistration
                        },
                        function(data){
                            //alert(data);
                            if(!data.success) {
                                alert(data.message)
                            } else{
                                // VAF-8372 PdvW Adding reminder for the F&I to upload needed supporting documents.
//                                alert("Please remember to add the supporting documents for this vehicle.");
                            }
                            window.location.assign("../vehicleInbox/index");
                        }
                );
            }
        });

        $("#returnToVehicleInboxButton").click(function(){
            window.location.assign("../vehicleInbox/index");
        });
    };

</script>

