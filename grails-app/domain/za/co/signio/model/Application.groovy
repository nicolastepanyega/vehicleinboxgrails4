package za.co.signio.model

class Application {

    String name;

    static hasMany = [auditlogs: Auditlog]

    static mapping = {
        version false;
    }

    static constraints = {
        name maxSize: 100, nullable: false;
    }

    String toString() {
        return name;
    }
}
