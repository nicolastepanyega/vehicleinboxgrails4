package za.co.signio.dataModel

import grails.util.Holders
import groovy.sql.Sql
import groovy.transform.Synchronized

/**
 * Created with IntelliJ IDEA.
 * User: pieter
 * Date: 15/03/20
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */
class SignioUniqueNumber {

    static def grailsApplication = Holders.grailsApplication

    @Synchronized
    public static String generateNumber(String fromApplication, String prefix){
        def dataSource = grailsApplication.mainContext.getBean('dataSource')
        List results = new Sql(dataSource).rows("call VAF5.SP_GEN_BATCHREF(${fromApplication}, ${prefix})")
        String batchRef = null
        results.each{ item ->
            batchRef = item.batchRef
        }
        return batchRef
    }

}
