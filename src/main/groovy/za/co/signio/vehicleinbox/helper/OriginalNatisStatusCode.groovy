package za.co.signio.vehicleinbox.helper

public enum OriginalNatisStatusCode {
	REQUEST_LOGGED("Request Logged"),
	DUPLICATE_ORDERED("Duplicate Ordered"),
	NATIS_PICKED("Natis Picked"),
	NATIS_DISPATCHED("Natis Dispached"),
	NATIS_RETURNED("Natis Returned"),
	NATIS_COLLECTED("Natis Collected"),
	NATIS_READY_FOR_COLLECTION("Natis Ready for Collection"),
	IN_CLEARANCE_QUEUE("In Clearance Queue"),
	APPROVED("Approved"),
	REJECTED("Rejected");
	
	private String message;
	
	private OriginalNatisStatusCode(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return this.message;
	}
}
