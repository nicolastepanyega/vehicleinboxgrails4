package za.co.signio.model

class DocumenttemplategroupInstitution {

    String customDocumentData

    static hasOne = [documenttemplategroup: Documenttemplategroup, institution: Institution, auxFspnumber: AuxFspnumber]
    static belongsTo = [Documenttemplategroup, Institution, AuxFspnumber]

    static constraints = {
        documenttemplategroup nullable: false
        institution nullable: false
        customDocumentData(nullable: true)
        auxFspnumber nullable: true
    }

    static mapping = {
        version false
    }
}
