<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2013/08/16
  Time: 11:41 AM
  To change this template use File | Settings | File Templates.
--%>
<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>
<html>
<head>
    <meta name="layout" content="minimal"/>
    <title>Vehicle Inbox - V5 - RegTrack Error</title>
    %{-- <r:layoutResources/> --}%
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}">
</head>
<body>
<table width="98%" class="BGColourLightRed">
    <tr>
        <td colspan="4"  class="BGColourWhite" ><g:img dir="images" file="signio_logo.png"/></td>
    </tr>
    <tr>
        <td colspan="4" class="BGColourLightRed" ><h4>An Error Occurred while sending RegTrack request!</h4></td>
    </tr>
    <tr>
        <td colspan="1"  class="BGColourWhite" >Please contact support at Signio</td>
        <td colspan="3"  class="BGColourWhite" >Signio Support:
            <a href="mailto:${grailsApplication.config.support.email}?Subject=RegTrack%20Error&body=${params.errorMessage}" target="_top">
            ${grailsApplication.config.support.email}
            </a><br/>
            ${grailsApplication.config.support.phone}
        </td>
    </tr>
    <tr>
        <td colspan="1"  class="BGColourCellRed" >Cause of Error </td>
        <td colspan="3"  class="BGColourCellRed" >${params.errorMessage}</td>
    </tr>
</table>
</body>
</html>

