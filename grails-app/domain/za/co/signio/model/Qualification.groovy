package za.co.signio.model

class Qualification {

    Date dateCreated;
    Date dateAcquired;

    static belongsTo = [Institution, Supportingdocs, Member, Qualificationtype]
    static hasOne = [institution:Institution, profiledocument:Profiledocument, member:Member, qualificationtype: Qualificationtype]

    static constraints = {
        dateCreated nullable: false
        dateAcquired nullable: true
        institution nullable: true
        profiledocument nullable: true
        member nullable: true
        qualificationtype nullable: true
    }

    static mapping = {
        version false
    }
}
