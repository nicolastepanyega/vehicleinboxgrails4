package za.co.signio.model

class AuxProductcategorytype {

    String type

    static hasMany = [auxProductcategories:AuxProductcategory]
    
    static constraints = {
        type(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }

    String toString() {
        return type
    }
}
