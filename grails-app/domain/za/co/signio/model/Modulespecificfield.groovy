package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/09/16
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
class Modulespecificfield {

    String generic1
    String generic2
    String generic3
    Date generic4
    Date generic5
    Date generic6
	
	String vinChassis
	String registrationNumber
	String stockNumber
	

    static belongsTo = [Deal, Institution]
    static hasOne = [deal:Deal, forwardingInstitution: Institution]

    static mapping = {
        version false
        generic1 column: 'generic_1'
        generic2 column: 'generic_2'
        generic3 column: 'generic_3'
        generic4 column: 'generic_4'
        generic5 column: 'generic_5'
        generic6 column: 'generic_6'
		vinChassis column: 'vin_chassis'
		registrationNumber column: 'registration_number'
		stockNumber column: 'stock_number'
    }

    static constraints = {
        generic1 nullable: true, maxSize: 45
        generic2 nullable: true, maxSize: 45
        generic3 nullable: true, maxSize: 45
        generic4 nullable: true
        generic5 nullable: true
        generic6 nullable: true
        deal nullable: false
        forwardingInstitution nullable: true
		vinChassis nullable: true, maxSize: 45
		registrationNumber nullable: true, maxSize: 45
		stockNumber nullable: true, maxsize: 45
    }
}
