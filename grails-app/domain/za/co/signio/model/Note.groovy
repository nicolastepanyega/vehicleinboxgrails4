package za.co.signio.model

class Note {

    String text
    Date dateCreated

    static hasOne = [
            member: Member,
    ]

    static hasMany = [
            deals: Deal,
            dealgroups: Dealgroup,
            dealdocuments: Dealdocument,
            supportingdocs: Supportingdocs
    ]

    static mapping = {
        version false
        table 'note'
        deals joinTable: 'deal_note'
        dealgroups joinTable: 'dealgroup_note'
        dealdocuments joinTable: 'dealdocument_note'
        supportingdocs joinTable: 'supportingdocs_note'
    }

    static constraints = {
        text nullable: false
    }

    def beforeInsert = {
        dateCreated = new Date()
    }
}
