package za.co.signio.model

class AuxProductcategory {

	String productCategoryName
	String productCategoryUniquePrefix
	Boolean active
    Boolean allowMultiple
    Boolean isRegulated

    static hasOne = [auxProductcategorytype: AuxProductcategorytype]
	static hasMany = [auxProducts: AuxProduct, auxFspCategoryInstitutionConfigs:AuxFspcategoryinstitutionconfig, auxProductcategoryformulas:AuxProductcategoryformula, auxProductcategorymapping:AuxProductcategorymapping]
    static belongsTo = [AuxProductcategorytype]


	static mapping = {
		version false
	}

	static constraints = {
		productCategoryName(nullable: false, maxSize: 255)
		productCategoryUniquePrefix(nullable: true, maxSize: 5)
        active(nullable: false)
        allowMultiple(nullable: false)
        isRegulated(nullable: false)
        auxProducts(nullable: true, blank:true)
        auxFspCategoryInstitutionConfigs(nullable: true, blank:true)
	}

    def String toString()
    {
      return "${productCategoryName}"
    }
}
