package za.co.signio.webservice;

public class WebServiceResponse {
	
	private WebServiceResponseState state;
	private String message;
	private String result;

	public WebServiceResponse(WebServiceResponseState state, String message, String result){
		this.state = state;
		this.message = message;
		this.result = result;
	}
	
	public WebServiceResponseState getState(){
		return this.state;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public String getResult(){
		return this.result;
	}
	
	public String toString(){
		return this.toXMLString();
	}
	
	public String toXMLString(){
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<response>\n" +
			"\t<state>" + this.state.name() + "</state>\n" +
			"\t<message><![CDATA[" + this.message + "]]></message>\n" +
			"\t<result><![CDATA[" + this.result + "]]></result>\n" +
		"</response>";
	}
	
	public Map<String, String> toMap(){
		return [ 
			state : this.state.name(), 
			message: this.message, 
			result : this.result
		];
	}
}
