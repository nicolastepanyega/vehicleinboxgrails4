<tr><td colspan="100%" class="BGColourWhite">
    <table style="width:98%" id="vehicleInboxTableHolder">
        <thead>
        <tr class="BGColourBlue">
            <td></td>
            <td>Date</td>
            <td>Client Details</td>
            <td>ID / Reference Number</td>
            <td>Make</td>
            <td>Model</td>
            <td>Year</td>
            %{--<td>Colour</td>--}%
            <td>Vin / Chassis</td>
            %{--<td>VVI</td>--}%
            <td>Copy Natis</td>
            <td>Original Natis</td>
            <td>RegTrack</td>
        </tr>
        </thead>
        <tbody>

        <g:each in="${vehicles}" var="items">
            <g:each in="${items}" var="entry">
                <g:each in="${entry.value}" var="vehicle">

                    <g:set var="key" value="${vehicle.key}"></g:set>
                    <g:set var="value" value="${vehicle.value ?: null}"></g:set>

                    <g:if test="${key == 'id'}">
                    </g:if>

                    <g:elseif test="${key == 'canedit'}">
                        <g:if test="${value.toString().equalsIgnoreCase('true')}">
                            <td><input type="button" title="Edit" value="Edit"
                                       onclick="window.location.href = '../vehicledetail/newVehicle?dealID=${entry.key}'"/>
                            </td>
                        </g:if>
                        <g:else>
                            <td><input type="button" title="Edit" value="Edit" disabled="true"></td>
                        </g:else>
                    </g:elseif>

                    %{--<g:elseif test="${key == 'vvi'}">--}%
                        %{--<td><g:link controller="vehicleInbox" action="vviWebsite">VVI WebSite</g:link></td>--}%
                    %{--</g:elseif>--}%
                %{--testing copy of natis different status's--}%
                    <g:elseif test="${key == 'copyNatis'}">
                        <g:if test="${value.toString().substring(6, 10).equalsIgnoreCase('View')}">
                            <td><g:link
                                    url="${value.toString().substring(value.toString().lastIndexOf('link') + 5, value.toString().length() - 1)}">Received</g:link></td>
                        </g:if>
                        <g:if test="${value.toString().substring(6, 20).equalsIgnoreCase('Request Failed')}">
                            <td>${'<a href="' + value.link + '">' + value.text + '</a>' + "<br>" + value.message?.replaceAll('\n', "<br>")}</td>
                        </g:if>
                        <g:if test="${value.toString().substring(6, 19).equalsIgnoreCase('Not Requested')}">
                            <td><g:link
                                    url="${value.toString().substring(value.toString().lastIndexOf('link') + 5, value.toString().length() - 1)}">Not Requested</g:link></td>
                        </g:if>

                    </g:elseif>
                %{--testing original natis different status's--}%
                    <g:elseif test="${key == 'originalNatis'}">

                        <g:if test="${value.toString().substring(6, 12).equalsIgnoreCase('Failed')}">
                            <td>${'<a href="' + value.link + '">' + value.text + '</a>' + "<br>" + value.message?.replaceAll('\n', "<br>")}</td>
                        </g:if>
                        <g:elseif test="${value.toString().substring(6, 15).equalsIgnoreCase('Requested')}">
                            <td>${'<a href="' + value.link + '">' + value.text + '</a>' + "<br>" + value.message?.replaceAll('\n', "<br>")}</td>
                        </g:elseif>
                        <g:elseif test="${value.toString().substring(6, 19).equalsIgnoreCase('Not Requested')}">
                            <td><g:link
                                    url="${value.toString().substring(value.toString().lastIndexOf('link') + 5, value.toString().length() - 1)}">Not Requested</g:link></td>
                        </g:elseif>
                        <g:elseif test="${value.toString().substring(6, 22).equalsIgnoreCase('Request Received')}">
                            <td>${'<a href="' + value.link + '">' + value.text + '</a>' + "<br>" + value.message?.replaceAll('\n', "<br>")}</td>
                        </g:elseif>

                    </g:elseif>
                %{--testing regtrack different status's--}%
                    <g:elseif test="${key == 'regTrack'}">
                        <g:if test="${value.toString().substring(6, 19).equalsIgnoreCase('Not Requested')}">
                            <td>Not Requested</td>
                        </g:if>
                        <g:elseif test="${value.toString().substring(6, 21).equalsIgnoreCase('RegTrack Failed')}">
                            <td><a href="#"
                                   onclick="showStatusMessage(${value.toString().substring(value.toString().indexOf('(') + 1, value.toString().lastIndexOf(')'))})">RegTrack Failed.</a>
                            </td>
                        </g:elseif>
                        <g:elseif test="${value.toString().substring(15, 24).equalsIgnoreCase('Requested')}">
                            <td><a href="#"
                                    onclick="showStatusMessage(${value.toString().substring(value.toString().indexOf('(') + 1, value.toString().lastIndexOf(')'))})">RegTrack Requested.</a></td>
                        </g:elseif>
                        <g:elseif test="${value.toString().substring(15, 31).equalsIgnoreCase('request complete')}">
                            <td><g:link
                                    url="${value.toString().substring(value.toString().lastIndexOf('link') + 5, value.toString().length() - 1)}">RegTrack request complete. ALV Form received.</g:link></td>
                        </g:elseif>

                    </g:elseif>
                    <g:elseif test="${value != null}">
                        <td>${value}</td>
                    </g:elseif>
                    <g:else>
                        <td></td>
                    </g:else>

                </g:each>

            </g:each>
            </tr>
        </g:each>

        </tbody>

    </table>
    <div class="pagination">
       %{-- <g:paginate total="${vehicleCount}"/>--}%
    </div>
</td></tr>
<div id="regtrack_status_modal" class="regTrackStatus" style="display:none" title="RegTrack Status">
    <p>Current RegTrack Status:</p>

    <p id="regtrack_status_messsage">Current RegTrack Status Unknown.</p>
</div>

<script type="text/javascript">

    // renders the Links for the various requests.
    var renderLink = function(record){
        var message = (record.message != null) ?  ("<br>" + record.message.replace(/\n/g, "<br>")) : "";

        if(record.link == null){
            return record.text + message;
        }
        else{
            return '<a href="' + record.link + '">' + record.text + '</a>' + message;
        }
    };

    function showStatusMessage(dealID) {

        var regTrackMessage = function () {
            var message = "Current RegTrack Status Unknown.";
            if (dealID != null && dealID != "-1") {
                $.ajax({
                    //VAF-8372 PdvW changing to getting the audit trail and not just errors.
                    url:"/VehicleInbox/regtrack/getDealdataForRegtrackDeal?id=" + dealID,

                    contentType:'application/javascript; charset=utf-8',
                    async:false,
                    type:"GET",
                    error:function (xhr, ajaxOptions, thrownError) {
                        //console.log("Error occurred retrieving Years \nStatus: " + xhr.status + "\n Error: " + thrownError)
                    },
                    success:function (comment) {
                        message = comment
                    }
                });
            }
            return message;
        };

        var newLineToBreak = function (str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }

        var messageField = $("#regtrack_status_messsage");
        var message = regTrackMessage();
        messageField.html(newLineToBreak(message, true));
        //messageField.html(message);

        var box = $("#regtrack_status_modal").dialog({
            autoOpen:true,
            resizable:false,
            //model: true,
            height:600,
            width:800,
            buttons:{
                "OK":function () {
                    $(this).dialog("close");
                }
            }
        });
        //box.dialog("open")
    }

</script>

