package za.co.signio.model

class BankvapsCriteriasetup {

    String operator
    String value

    static hasOne = [
            bankvapsCriteriafield:BankvapsCriteriafield,
            bankvaps:Bankvaps
    ]

    static belongsTo = [BankvapsCriteriafield, Bankvaps]


    static constraints = {
        bankvaps(nullable: false)
        bankvapsCriteriafield(nullable: false)
        operator(nullable: false, maxSize: 45)
        value(nullable: true)
    }

    static mapping = {
        version false;
    }

}