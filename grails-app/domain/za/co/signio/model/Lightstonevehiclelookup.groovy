package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/06/30
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
class Lightstonevehiclelookup {

    Integer carId
    String smartId
    String vehicleType
    Vehicle vehicle

    static belongsTo = [Vehicle]

    static constraints = {
        carId(nullable: true)
        smartId(nullable: true, maxSize: 45)
        vehicleType(nullable: true, maxSize: 45)
        vehicle(nullable: false)
    }

    static mapping = {
        version false
    }
}
