package za.co.signio.model

class RelationshipmoduleconfigModuleaction {

    static hasOne = [moduleaction:Moduleaction,relationshipmoduleconfig:Relationshipmoduleconfig]
    static belongsTo = [Relationshipmoduleconfig]

    static mapping = {
        version false
    }
}
