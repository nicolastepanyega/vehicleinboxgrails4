package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2014/12/18
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */
class Dealdatadocument {

    String uri
    Date dateCreated

    static hasOne = [dealdocument: Dealdocument]

    static belongsTo = [Dealdocument]

    static constraints = {
        uri (maxSize: 255, nullable: false)
    }

    static mapping = {
        version false
    }
}
