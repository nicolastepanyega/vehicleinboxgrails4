package za.co.signio.model

class Queue {

    String name
    String queueName
    String comment
    String type
    String forAction

    static belongsTo = [institution:Institution]

    static constraints = {
        name maxSize: 255, nullable: false
        queueName maxSize: 45, nullable: false
    }

    static mapping = {
        version false
    }
}
