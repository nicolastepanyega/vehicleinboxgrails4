package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/06/17
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
class Asset {

    String serialNumber
    Date dateLogged

    static hasOne = [assettype:Assettype]

    static belongsTo = [Assettype]
    static hasMany = [assetallocateds:Assetallocated]

    static constraints = {
        serialNumber nullable: true,maxSize: 45
        dateLogged nullable: false
        assettype nullable: false
    }

    static mapping = {
        version false
    }
}
