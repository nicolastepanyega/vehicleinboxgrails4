package za.co.signio.model

class Moduleaction {

    String name

    static hasMany = [moduletemplates:Moduletemplate,relationshipmoduleconfigModuleaction:RelationshipmoduleconfigModuleaction]

    static mapping = {
        version false
        moduletemplates joinTable: [name: "moduletemplate_moduleaction", key: "moduleaction_id"]
    }

    static constraints = {
        name nullable: false, maxSize: 45
    }
}
