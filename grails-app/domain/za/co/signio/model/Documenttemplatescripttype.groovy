package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 5/3/12
 * Time: 3:47 PM
 * To change this template use File | Settings | File Templates.
 */
class Documenttemplatescripttype {
    
    String type

    static hasMany = [documentTemplateScripts: Documenttemplatescript]

    static constraints = {
        type nullable: false
    }

    static mapping = {
        version false
    }
}
