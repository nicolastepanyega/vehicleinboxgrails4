package za.co.signio.model

class Messagerecipient {

    String recipient;

    static hasMany = [messages: Message]

    static mapping = {
        version false;
    }

    static constraints = {
        recipient maxSize: 100, nullable: false;
    }

    String toString() {
        return recipient;
    }
}