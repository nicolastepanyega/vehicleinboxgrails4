package za.co.signio.model

/**
 * Created by stephan_coetzer on 2016-06-20.
 */
class Otp {
    String batchRef
    Date dateCreated
    Date expiryDate
    Date replyDate
    String pin
    String message
    String replyMessage
    String target
    String transactionId


    static mapping = {
        version false
    }

    static hasOne = [
            member:Member,
            institution: Institution,
            clientdetail: Clientdetail,
            companydetail: Companydetail,
            otpaction: Otpaction,
            status: Status,
            otpdeliverymethod: Otpdeliverymethod
    ]

    static constraints = {
        batchRef(nullable: true, maxSize: 50)
        dateCreated(nullable: false)
        expiryDate(nullable: false)
        replyDate(nullable: true)
        pin(nullable: false, maxSize: 100)
        replyMessage(nullable: true, maxSize: 255)
        transactionId(nullable: false, maxSize: 100)
        target(nullable: false)
        otpaction(nullable: false)
        status(nullable: false)
        otpdeliverymethod(nullable: false)
        member(nullable: true)
        institution(nullable: true)
        clientdetail(nullable: true)
        companydetail(nullable: true)
    }

}
