package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/06/17
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
class Assetallocated {

    Boolean active
    String updatedBy
    Date dateAssigned
    Date dateUpdated

    static hasOne = [institution:Institution,
                     member:Member,
                     asset:Asset]

    static belongsTo = [Asset,Member,Institution]


    static constraints = {
        institution nullable: false
        member nullable: false
        asset nullable: false
        active nullable: false
        updatedBy nullable: true, maxSize: 255
        dateAssigned nullable: true
        dateUpdated nullable: true

    }
    static mapping = {
        version false
    }
}
