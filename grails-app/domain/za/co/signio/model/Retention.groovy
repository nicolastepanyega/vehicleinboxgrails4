package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 5/17/13
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 */
class Retention {
    String branchAreaCode
    String customerId
    String customerTitle
    String customerFirstName
    String customerSurname
    String customerHomeTel
    String customerCell
    String customerWorkTel
    String dealerCode
    String dealerName
    String dealerPhoneNumber
    String languageIndicator
    String articleDescription
    String quoteDate
    Double settlementAmount
    String existingAccountNumber
    Double instalmentAmount
    Double accountBalance
    Integer remainingTerm
    String riskGrade
    Integer propensityScore
    Integer prioritisation
    String nextBestSell
    Date receivedDate
    String campaignCode
    String businessUnit
    String sourceOfLead
    Date dateUpdated
    String comment
    Member member
    Status status
    Dealstate dealstate
    List retentionActions

    static belongsTo = [Member, Status, Dealstate]

    static hasMany = [retentionActions:Retentionaction]


    static constraints = {
        branchAreaCode nullable: true, maxSize: 45
        customerId nullable: true, maxSize: 45
        customerTitle nullable: true, maxSize: 45
        customerFirstName nullable: true, maxSize: 45
        customerSurname nullable: true, maxSize: 45
        customerHomeTel nullable: true, maxSize: 45
        customerCell nullable: true, maxSize: 45
        customerWorkTel nullable: true, maxSize: 45
        dealerCode nullable: true, maxSize: 45
        dealerName nullable: true, maxSize: 45
        dealerPhoneNumber nullable: true, maxSize: 45
        languageIndicator nullable: true, maxSize: 45
        articleDescription nullable: true, maxSize: 45
        quoteDate nullable: true, maxSize: 45
        settlementAmount nullable: true
        existingAccountNumber nullable: true, maxSize: 45
        instalmentAmount nullable: true
        accountBalance nullable: true
        remainingTerm nullable: true
        riskGrade nullable: true, maxSize: 45
        propensityScore nullable: true
        prioritisation nullable: true
        nextBestSell nullable: true, maxSize: 45
        receivedDate nullable: true
        campaignCode nullable: true, maxSize: 45
        businessUnit nullable: true, maxSize: 45
        sourceOfLead nullable: true, maxSize: 45
        dateUpdated nullable: true
        comment(nullable: true, maxSize: 500)
        member nullable: true
        status(nullable: true)
        dealstate(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
