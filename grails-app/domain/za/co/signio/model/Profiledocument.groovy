package za.co.signio.model

class Profiledocument {

    String uri;
    Date dateCreated, dateUpdated;

    static belongsTo = [Member, Supportingdocstype, Institution]

    static hasOne = [
            supportingdocstype: Supportingdocstype,
            member: Member,
            institution: Institution
    ]

    static hasMany = [qualifications: Qualification]

    static constraints = {
        dateCreated nullable: false
        dateUpdated nullable: false
        uri nullable: false
    }

    static mapping = {
        version false
    }
}
