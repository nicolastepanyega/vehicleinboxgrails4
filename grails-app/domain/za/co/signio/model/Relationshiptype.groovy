package za.co.signio.model


class Relationshiptype {

    String type
    String authority
    String displayName

    static belongsTo = [Institutiontype]
    static hasMany = [institutiontypes: Institutiontype, relationships:Relationship, roleSetups:Rolesetup]
    static auditable = true

    static mapping = {
        version false
        institutiontypes joinTable: 'institutiontype_relationshiptype'
        roleSetups joinTable: 'relationshiptype_rolesetup'
    }

    static constraints = {
        type (nullable: false, maxSize: 45)
        authority(nullable: true)
        displayName(nullable: true, maxSize: 45)
    }

    String toString() {
        return type
    }
}
