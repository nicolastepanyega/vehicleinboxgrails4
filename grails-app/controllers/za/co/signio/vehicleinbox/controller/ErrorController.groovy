package za.co.signio.vehicleinbox.controller

import org.apache.commons.lang.StringEscapeUtils

class ErrorController {

    def viError = {
        // Escape String used on HTML page.
//        [
//                title: params.title,
//                shortMessage: escapeForURL(params.shortMessage),
//                fullMessage: escapeForURL(params.fullMessage),
//                cause: escapeForURL(params.cause)
//        ]
    }

    private String escapeForURL(String s) {
        if (s == null) return s;
        s = StringEscapeUtils.escapeHtml(s)
        s = s.replaceAll("\n", "%0A")
        return s;
    }
}
