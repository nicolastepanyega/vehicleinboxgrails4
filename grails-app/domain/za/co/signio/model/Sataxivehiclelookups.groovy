package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/08/19
 * Time: 12:38 PM
 * To change this template use File | Settings | File Templates.
 */
class Sataxivehiclelookups {

    String manufacturerName
    String manufacturerCode
    String modelName
    String modelYear
    String modelCode
    Integer vehicleId
    
    static constraints = {
        manufacturerName(nullable: false, maxSize: 100)
        manufacturerCode(nullable: false, maxSize: 25)
        modelName(nullable: false, maxSize: 100)
        modelYear(nullable: false, maxSize: 4)
        modelCode(nullable: false, maxSize: 25)
        vehicleId(nullable: true)
    }

    static mapping = {
        version false
    }
}
