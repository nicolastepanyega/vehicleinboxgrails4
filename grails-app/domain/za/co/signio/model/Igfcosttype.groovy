package za.co.signio.model

class Igfcosttype {

    String type

    static hasMany = [igfcosts:Igfcost]

    static mapping = {
        version false
    }

    static constraints = {
        type(nullable: false, maxSize: 45)
    }
}
