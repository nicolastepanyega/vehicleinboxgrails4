package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/03/25
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxReferencecodeconfig {

    String referenceCode
    Institutiontype institutiontype

    static hasOne = [auxProductoption: AuxProductoption, institution: Institution, institutionInstitution: InstitutionInstitution]
    static belongsTo = [AuxProductoption, Institution, Institutiontype, InstitutionInstitution]

    static constraints = {
        referenceCode (nullable: false, maxSize: 45)
        auxProductoption nullable: false
        institution nullable: false
        institutiontype nullable: false
        institutionInstitution nullable: true
    }

    static mapping = {
        version false
    }
}
