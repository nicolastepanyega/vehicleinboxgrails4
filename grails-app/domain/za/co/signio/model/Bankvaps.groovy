package za.co.signio.model

class Bankvaps {

    String productName
    String metaData
    Institution institution
    Date startDate
    Date endDate

    static belongsTo = [Institution]

    static hasMany = [
        bankvapsCriteriasetup:BankvapsCriteriasetup,
        bankvapsPremiums:BankvapsPremiums
    ]

    static mapping = {
        version false
    }

    static constraints = {
        productName nullable: false, maxSize: 255
        metaData nullable: true
        institution(nullable: false)
        startDate nullable: true
        endDate nullable: true
    }

    String toString() {
        return productName
    }
}
