package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/16/12
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */
class Documenttemplate {

    String name
    String fileName
    String template
    String xml
    Boolean isAlwaysIncluded
    Integer templateOrder
    Date dateAdded
    Boolean active
    Institution institution


    static belongsTo = [Institution, Documenttemplategroup]

    static hasMany = [documentTemplateScripts: Documenttemplatescript, documentTemplateGroups: Documenttemplategroup]

    static constraints = {
        name( nullable: false, maxSize: 45)
        fileName nullable: true, maxSize: 45
        template(nullable: true)
        xml(nullable: true)
        isAlwaysIncluded nullable: false
        templateOrder(nullable: false)
        dateAdded(nullable: false)
        active(nullable: false)
        institution nullable: false
    }

    static mapping = {
        version false
        documentTemplateScripts joinTable: 'documenttemplate_documenttemplatescript'
        documentTemplateGroups joinTable: 'documenttemplategroup_documenttemplate'
    }
}
