package za.co.signio.model

class Report {

    String name
    String description
    Date dateCreated
    String fileName
    String thumbNail
    Integer order
    static hasMany = [institutions: Institution, memberReports: MemberReport, qvlicenses:Qvlicense, reportInstitutions:ReportInstitution]
    static belongsTo = [Institution, MemberReport, Qvlicense]

    static constraints = {
        name(nullable: false, maxSize: 255)
        description(nullable: true, maxSize: 255)
        dateCreated(nullable: true)
        fileName(nullable: true, maxSize: 45)
        thumbNail(nullable: true)
        order(nullable: true)
    }

    String toString()
    {
        return name;
    }

    static mapping = {
        version false
        dynamicUpdate true
        institutions joinTable: 'report_institution'
        qvlicenses joinTable: 'report_qvlicense'

    }
}
