package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 4/5/12
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */
class Documenttemplatescript {

    String name
    String script
    Documenttemplatescripttype documenttemplatescripttype

    static belongsTo = [Documenttemplate, Documenttemplatescripttype, Documenttemplatescript, Documenttemplategroup]

    static hasMany = [documentTemplates: Documenttemplate, documentTemplateGroups: Documenttemplategroup]

    static mapping = {
        version false
        documentTemplates joinTable: 'documenttemplate_documenttemplatescript'
        documentTemplateGroups joinTable: 'documenttemplategroup_documenttemplatescript'
    }

    static constraints = {
        name(nullable: false, maxSize: 45)
        script(nullable: false)
        documenttemplatescripttype nullable: false
    }
}
