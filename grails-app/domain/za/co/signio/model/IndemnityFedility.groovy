package za.co.signio.model

class IndemnityFedility {

    String type

    static hasMany = [institutions:Institution]

    static constraints = {
        type(nullable: false, maxSize: 100)
    }
    static mapping = {
        version(false)
    }
}
