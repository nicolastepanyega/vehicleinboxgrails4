package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/07/24
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
class Publicholiday {

    Integer year
    String name
    Date date

    static constraints = {
        year(nullable: false)
        name(nullable: false, maxSize: 50)
        date(nullable: false)
    }

    static mapping = {
        version(false)
    }


}
