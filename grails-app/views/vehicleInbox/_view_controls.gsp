<tr class="BGBlueButtonBarr">
    <form method="get" class="SearchForm" action="" style="border: none">
        <td>
            <div class="SearchDiv">
                <input type="button" title="Add New Vehicle" value="Add New Vehicle" id="addNewVehicleButton" style="position: relative;">
                <span class="SearchSpan">
                    <input type="text" class="SearchText"  name="searchRequest" value="${searchRequest}">
                    <g:actionSubmit class="SearchButton" value="Search" action="index" id="searchButton"/>
                </span>
                %{--<input type="button" title="Show All Vehicles" value="Show All Vehicles" id="showAllVehicles" style="position: relative;">--}%
                <input type="button" name="showAllButton" value="Show All">
            </div>
        </td>
    </form>
</tr>

<script type="text/javascript">
	$(document).ready(function(){
		$("#addNewVehicleButton").click(function(){
			window.location.assign("../vehicledetail/newVehicle");
		});
        $("[name='showAllButton']").click(function(){
            $("[name='searchRequest']").val("");
            $("#searchButton").trigger("click");
        });
	});

    function showError(e) {
        var errorDiv = document.getElementById('error');
        errorDiv.innerHTML = '<ul><li>'
                + e.responseText + '</li></ul>';
        errorDiv.style.display = 'block';
    }
</script>