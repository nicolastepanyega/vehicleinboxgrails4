package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/06/17
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
class Assettype {

    String type

    static hasMany = [assets:Asset]

    static constraints = {
        type nullable: false,maxSize: 45
    }

    static mapping = {
        version false
    }
}
