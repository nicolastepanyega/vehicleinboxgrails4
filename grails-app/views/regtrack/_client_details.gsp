<tbody class='clientDetail'><tr class='BGColourBlue'><td colspan="4">Client Details</td></tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">First Name</td>
    <td style="width:30%;">
        <input name="customerFirstName" id="customerFirstName" type="text" disabled="disabled" style="width:96%" value="${customerFirstName}">
    </td>
    <td style="width:20%;">Surname</td>
    <td style="width:30%;">
        <input name="customerSurname" id="customerSurname" type="text" disabled="disabled" style="width:96%" value="${customerSurname}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">ID Number</td>
    <td style="width:30%;">
        <input name="customerIdNumber" id="customerIdNumber" type="text" disabled="disabled" style="width:96%" value="${customerIdNumber}">
    </td>
    <td style="width:20%;">Date of Birth</td>
    <td style="width:30%;">
        <input name="customerDateOfBirth" id="customerDateOfBirth" type="text" disabled="disabled" style="width:96%" value="${customerDateOfBirth}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Email Address</td>
    <td style="width:30%;">
        <input name="customerEmail" id="customerEmail" type="text" disabled="disabled" style="width:96%" value="${customerEmail}">
    </td>
    <td style="width:20%;">Telephone</td>
    <td style="width:30%;">
        <input name="customerPhoneNumber" id="customerPhoneNumber" type="text" disabled="disabled" style="width:96%" value="${customerPhoneNumber}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack'>
    <td style="width:20%;">Physical Address</td>
    <td style="width:30%;">
        <input name="customerResidentialAddress" id="customerResidentialAddress" type="text" disabled="disabled" style="width:96%" value="${customerResidentialAddress}">
    </td>
    <td style="width:20%;">Postal Address</td>
    <td style="width:30%;">
        <input name="customerPostalAddress" id="customerPostalAddress" type="text" disabled="disabled" style="width:96%" value="${customerPostalAddress}">
    </td>
</tr>
<tr class='BGColourBlueTD regTrack' style="display: none;">
    <td style="width:20%;">Account No</td>
    <td style="width:30%;">
        <input name="accountNo" id="customerBankAccountNumber" type="text" disabled="disabled" style="width:96%" value="${customerBankAccountNumber}">
    </td>
    <td style="width:20%;">Title</td>
    <td style="width:30%;">
        <input name="customerTitle" id="customerTitle" type="text" disabled="disabled" style="width:96%" value="${customerTitle}">
    </td>
</tr>
</tbody>