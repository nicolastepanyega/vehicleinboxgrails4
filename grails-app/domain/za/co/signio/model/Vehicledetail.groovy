package za.co.signio.model

import java.text.SimpleDateFormat

class Vehicledetail {

    String vehicleRegistrationYear
    String vinChassis
    String colour
    String engineNumber
    String registrationNumber
    Date dateCreated
    String requestReference
    String externalRequestReference
    Vehicle vehicle
    Institution institution
    za.co.signio.model.Member member
    String vviStatus
    String regtrackStatus
    Copynatisstatus copynatisstatus
    Copynatisprintdata copynatisprintdata
    String fileName
    String originalNatisStatus

    static belongsTo = [Vehicle, Institution, Copynatisstatus]
    static hasMany = [requests:Request]
    static hasOne = [copynatisprintdata:Copynatisprintdata]

    static constraints = {
        vehicleRegistrationYear nullable: true, maxSize: 4
        vinChassis(nullable: true, maxSize: 45)
        colour(nullable: true, maxSize: 45)
        engineNumber nullable: true, maxSize: 45
        registrationNumber(nullable: true, maxSize: 45)
        dateCreated(nullable: true)
        requestReference nullable: true, maxSize: 45
        externalRequestReference nullable: true, maxSize: 45
        vviStatus(nullable: true, maxSize: 45)
        regtrackStatus(nullable: true, maxSize: 45)
        copynatisstatus nullable: true
        fileName nullable: true, maxSize: 45
        copynatisprintdata nullable: true
        originalNatisStatus nullable: true, maxSize: 45
    }

    static mapping = {
        version(false)
    }

    public  String now() {
        return now("yyyy-MM-dd HH:mm:ss");
    }

    public  String timeStamp() {
        return now("yyyyMMddHHmmss");
    }



    public  String uniqueRef() {
        return ""+now("yyMMddHHmm")+id;
    }

    public  String now(String s) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(s);
        Date date = new Date();
        GregorianCalendar gregoriancalendar = new GregorianCalendar();
        gregoriancalendar.setTime(date);
        Date date1 = gregoriancalendar.getTime();
        String s1 = simpledateformat.format(date1);
        return s1;
    }


}
