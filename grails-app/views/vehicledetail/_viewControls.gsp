<%--
  Controls/button pane
  User: chris.ferreira@signio.co.za
  Date: 2013/07/26
  Time: 11:29 AM
  --%>
<tr class="BGBlueButtonBarr">
    <td>
        <div style="display:inline;float:left"><input type="button" title="Save Vehicle" value="Save Vehicle" id="saveNewVehicle"></div>
        <div style="display:inline;float:left" onclick="history.go(-1);"><input type="button" title="Return to Vehicle Inbox" value="Return to Vehicle Inbox" id="returnToVehicleInboxButton"></div>
    </td>
</tr>

<script type="text/javascript">

    $("#saveNewVehicle").click(function(){

        // do some validation
        var formValid = true;
        $.each($( "#newVehicleForm" ).validate().elements(), function(index, field){
            field = $(field);
            if(field.valid() == 0){
                //console.log("field " + field.attr("name") + " invalid");
                formValid = false;
            }
        });
        if(!formValid){
            //console.log("Cannot Submit. Form still invalid");
            return;
        }

        /// add an overlay
        $("#blockModal").dialog({
            modal: true,
            dialogClass: "no-close"
        });

        var uiBlocked = true;
        var unblockUI = function(success){
            if(uiBlocked){
                uiBlocked = false;
                // check if there is a result...
                if(!success && result != null){
                    //alert(result.message, result.state);
                }
                // submit timed out...
                $("#blockModal").dialog("close");
            }
        };
        $(".ui-dialog-titlebar-close").hide();

        setTimeout(unblockUI, 5000);

        // get the request type.
        var details = {
            dealID : $("#dealID").val(),
            vehicleManufacturer : $("#vehicleManufacturer").val(),
            vehicleModel : $("#vehicleModel").val(),
            vehicleYear : $("#vehicleYear").val(),
            vehicleColour : $("#vehicleColour").val(),
            vinNumber : $("#vinNumber").val(),
            registrationNumber : $("#registrationNumber").val(),
            natisRegisteredNumber : $("#natisRegisteredNumber").val()
        };

        // submit to backend.
        var result = null;
        signio.vehicleInbox.callAjax("../vehicledetail/saveNewVehicle", "POST", details, function(data){
            //alert(data)
            if(data.success){
                result = data;
                unblockUI(true);
                window.location.assign("../vehicleInbox/index");
            }
            else{
                unblockUI(false);
                window.location.assign("../vehicleInbox/index");
            }
        });

    });

    $("#returnToVehicleInboxButton").click(function(){
        window.location.assign("../vehicleInbox/index");
    });
</script>