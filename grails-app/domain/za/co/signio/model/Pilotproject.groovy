package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Stephanc
 * Date: 2016/07/11
 * Time: 3:06 PM
 */
class Pilotproject {

    String name

    static hasMany = [pilots: Pilot]

    static constraints = {
        name nullable: false, maxSize: 45
    }

    static mapping = {
        version false
    }
}
