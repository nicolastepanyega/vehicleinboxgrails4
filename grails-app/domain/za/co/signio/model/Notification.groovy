package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/14/12
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
class Notification {

    String message
    Boolean active
    Date dateCreated
    Integer createdByMember

    Notificationtype notificationtype

    static belongsTo = [Notificationtype]

    static hasMany = [members:Member]

    static constraints = {
        message nullable: false
        active(nullable: false)
        dateCreated(nullable: false)
        createdByMember(nullable: false)
        notificationtype(nullable: false)
    }

    static mapping = {
        version false
        members joinTable: 'member_notification'
    }

}
