package za.co.signio.model

import za.co.signio.model.Role
import za.co.signio.model.Member


class Membergroup {

    String name
    Date dateCreated
    Date dateUpdated
    Boolean active

    static hasMany = [roles: Role, members: Member]
    static auditable = true

    static mapping = {
        version false
        members joinTable: 'membergroup_members'
        roles joinTable: 'membergroup_role'
    }

    static constraints = {
        name maxSize: 45, nullable: false
        dateCreated nullable: true
        dateUpdated nullable: true
        active(nullable: false)
    }

    String toString() {
        return name
    }

    def beforeInsert = {
        dateCreated = new Date()
    }

    def beforeUpdate = {
        dateUpdated = new Date()
    }
}
