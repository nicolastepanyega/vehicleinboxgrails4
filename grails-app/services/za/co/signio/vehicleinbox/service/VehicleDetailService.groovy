package za.co.signio.vehicleinbox.service

import za.co.signio.model.Vehiclemanufacturer
import za.co.signio.model.Vehicle
import za.co.signio.model.Formfieldvalue

class VehicleDetailService {

    /**
     * Get map of vehicle manufacturer id's and names
     *
     * @return map list of vehicle manufacturer id's and names
     */
    def getVehicleManufacturers() {
        def vehicleManufacturers = Vehiclemanufacturer.createCriteria().list {
            projections {
                property "id"
                property "name"
            }
            order "name", "asc"
        }
        return vehicleManufacturers
    }

    /**
     * Get list of possible years
     *
     * @param manufacturerID (Long): vehicleManufacturer id
     * @return list of possible years
     */
    def getVehicleYears(Long manufacturerID) {
        List<Integer> yearList = new ArrayList<Integer>();

        if (manufacturerID == null || manufacturerID <= 0) {
            return yearList
        }

        Date minDate = Vehicle.createCriteria().get {
            projections {
                vehiclemanufacturer {
                    eq("id", manufacturerID)
                }
                min "introductionDate"
            }
        } as Date

//        Date firstDateMax = Vehicle.createCriteria().get {
//            projections {
//                vehiclemanufacturer {
//                    eq("id", manufacturerID)
//                }
//                max "introductionDate"
//            }
//        } as Date
//
//        Date lastDateMax = Vehicle.createCriteria().get {
//            projections {
//                vehiclemanufacturer {
//                    eq("id", manufacturerID)
//                }
//                max "discontinuedDate"
//            }
//        } as Date

        Date maxDate  = new Date();//(firstDateMax < lastDateMax)? lastDateMax:firstDateMax

        int dateDif = maxDate.getAt(Calendar.YEAR) - minDate.getAt(Calendar.YEAR)

        if (dateDif < 0) {
            return yearList
        }

        for (int i = 0; i <= dateDif; i++) {
            yearList.add(minDate.getAt(Calendar.YEAR) + i)
        }

        return yearList.reverse()
    }

    /**
     * Get list of vehicle models
     *
     * @param id (Long) : vehicleManufacturer id
     * @param year (Integer) : registration year (yyyy)
     * @return Map list of vehicle model id's and descriptions
     */
    def getVehicleModels(Long manufacturerID, Integer year) {

       /* if (manufacturerID == null ||manufacturerID <= 0
                || year == null) {
            return new ArrayList<Set<Long, String>>()
        }*/

        def vehicleModelMap = Vehicle.createCriteria().list {
            vehiclemanufacturer {
                eq "id", manufacturerID
            }
            and {
                le "registrationStartYear" , year
                or {
                    ge "registrationEndYear" , year
                    eq "registrationEndYear" , 0
                }
            }
            projections {
                property "id"
                property "description"
            }
            order "description", "asc"
        }

        return vehicleModelMap
    }

    /**
     * Get list of vehicle colours from formFieldValue
     *
     * @return List of colours
     */
    def getVehicleColours() {

        List<Formfieldvalue> colours= Formfieldvalue.createCriteria().list {
            formfield {
                eq "name", "articleColour"
            }
            projections {
                property("value")
            }
        } as List<Formfieldvalue>

        return colours
    }

    String buildOriginalXML() {

    }

    private String buildXMLField(String fieldFormName, int mapping, String value) {
        StringBuilder builder = new StringBuilder()
        builder.append("<field formFieldName='$fieldFormName' mapping='$mapping'>");
        builder.append(value)
        builder.append("</field>");
        return builder.toString()
    }
}
