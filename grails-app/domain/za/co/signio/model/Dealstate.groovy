package za.co.signio.model


class Dealstate {

    String state
    String description
    Integer stateCode
    String colourCode

    /*static hasMany = [deals: Deal
                      //dealDocuments: Dealdocument,
                      //retentions:Retention,
                      //retentionActions:Retentionaction,
                      //dealDatas:Dealdata
    ]
*/
    static mapping = {
        version false
    }

    static constraints = {
        state maxSize: 45, nullable: false
        description nullable: true, maxSize: 255
        stateCode(nullable: false)
        colourCode maxSize: 10, nullable: true
    }

    String toString() {
        return state
    }
}
