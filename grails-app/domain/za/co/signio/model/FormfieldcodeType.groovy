package za.co.signio.model

class FormfieldcodeType {

    String type

    //static hasMany = [deals: Deal]

    static mapping = {
        version false
    }

    static constraints = {
        type maxSize: 45, nullable: false
    }

    String toString() {
        return type
    }
}
