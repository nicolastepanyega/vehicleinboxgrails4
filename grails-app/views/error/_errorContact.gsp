<tr>
    <td colspan="2">
        <span style="font-weight: bold; color: #FFFFFF;">Signio Support Contact Details:</span>
    </td>
</tr>
<tr>
    <td colspan="2">
        <span style="font-weight: bold;">${grailsApplication.config.support.phone}</span>
    </td>
    <td colspan="2">
        <span style="font-weight: bold;">e-mail:</span>
        <a href="mailto:${grailsApplication.config.support.email}?Subject=${shortMessage}&body=Date:${new Date()}%0A${fullMessage}%0A${cause}" target="_top">
            ${grailsApplication.config.support.email}
        </a>
    </td>
</tr>
<script type="text/javascript">
    $(document).ready(function(){
        $("#sendMailToSupport").click(function(){

        });
    });
</script>