package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 3/20/12
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
class Consultantlog {

    Date dateCreated
    String comment

    static hasOne = [member1: Member, //Consultant
                     member2: Member, //F&I
                     consultantlogreason: Consultantlogreason,
                     institution: Institution]

    static belongsTo = [Consultantlogreason, Member, Institution]

    static constraints = {
        member1(nullable: false)
        member2(nullable: false)
        dateCreated(nullable: false)
        consultantlogreason(nullable: false)
        comment(nullable: false)
        institution(nullable: false)
    }

    static mapping ={
        version(false)
    }
}
