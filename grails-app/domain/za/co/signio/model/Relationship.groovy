package za.co.signio.model

import za.co.signio.model.Status

class Relationship {

    Boolean isContact
    String employeeNumber
    Status status
    String userType
    String alpheraUserType
    String defaultNotificationMethod

    static auditable = true

    static belongsTo = [Relationshiptype, Member, Institution, AuxFspnumber]
    static hasOne = [status:Status,
                     auxFspnumber:AuxFspnumber,
                     relationshiptype:Relationshiptype,
                     institution:Institution,
                     member:Member]

    static hasMany = [relationshipmoduleconfigs:Relationshipmoduleconfig, messageRelationships: MessageRelationship, memberLogins:MemberLogin]

    static mapping = {
        version false
    }

    static constraints = {
        isContact nullable: true
        employeeNumber nullable: true, maxSize: 45
        status(nullable: false)
        institution(nullable: false)
        member(nullable: false)
        relationshiptype(nullable: false)
        userType nullable: true
        alpheraUserType nullable: true, maxSize: 45
        defaultNotificationMethod nullable: true, maxSize: 1
        auxFspnumber nullable: true
    }

    String toString() {
        return "${relationshiptype.type} - ${member.toString()}"
    }
}
