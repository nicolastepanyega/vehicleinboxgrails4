package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 2/27/12
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
class Theme {
    String name
    String logoUri
    String cssUri

    static hasMany = [institutions: Institution]

    static constraints = {
        name nullable: false, maxSize: 45
        logoUri nullable: true, maxSize: 255
        cssUri nullable: true, maxSize: 255
    }

    static mapping = {
        version false
    }

    String toString() {
        return name
    }
}
