package za.co.signio.model

class MemberAccreditation {

    Boolean active

    static belongsTo = [Member, Accreditation]
    static hasOne = [member: Member,
                     accreditation: Accreditation]

    static hasMany = [supportingDocs: Supportingdocs]

    static constraints = {
        member(nullable: false)
    }

    static mapping = {
        version(false)
    }

    public String toString(){
        active
    }
}
