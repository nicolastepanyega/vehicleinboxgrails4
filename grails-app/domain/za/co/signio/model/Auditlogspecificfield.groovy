package za.co.signio.model

class Auditlogspecificfield {

    Long generic1, generic2, generic3;
    String generic4, generic5, generic6, customerIdNumber, totalDealerIncome;

    static belongsTo = [Auditlog];

    static hasOne = [auditlog: Auditlog];

    static mapping = {
        version false;
        generic1 column: "generic_1"
        generic2 column: "generic_2"
        generic3 column: "generic_3"
        generic4 column: "generic_4"
        generic5 column: "generic_5"
        generic6 column: "generic_6"
    }

    static constraints = {
        generic1 nullable: true;
        generic2 nullable: true;
        generic3 nullable: true;
        generic4 nullable: true, maxSize: 255;
        generic5 nullable: true, maxSize: 255;
        generic6 nullable: true, maxSize: 255;
        customerIdNumber nullable: true, maxSize: 25;
        totalDealerIncome nullable: true, maxSize: 25;

        auditlog nullable: false;
    }

    String toString() {
        return "Specific field for audit log: " + auditlog?.id
    }
}
