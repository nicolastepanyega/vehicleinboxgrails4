package za.co.signio.model


class City {

    String name
    static hasOne = [province: Province]

    static belongsTo = [Province]
    static hasMany = [suburbs:Suburb]

    static mapping = {
        version false
    }

    static constraints = {
        name maxSize: 45, nullable: false
        province(nullable: false)
    }

    String toString() {
        return name
    }
}
