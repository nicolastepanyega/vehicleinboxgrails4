package za.co.signio.model

class MemberCompliance {

    Date supervisionStartDate;
    Date supervisionEndDate
    Date dateOfFirstAppointment

    static belongsTo = [Member, AuthorisedService]
    static hasOne = [member:Member, authorisedService: AuthorisedService]

    static constraints = {
        supervisionStartDate nullable: true
        supervisionEndDate nullable: true
        dateOfFirstAppointment nullable: true
        member nullable: false
        authorisedService nullable: true
    }

    static mapping = {
        version false
    }
}
