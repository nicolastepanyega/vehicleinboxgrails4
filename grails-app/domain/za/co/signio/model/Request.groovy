package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 2/13/13
 * Time: 9:44 AM
 * To change this template use File | Settings | File Templates.
 */
class Request {

    Byte[] xmlRequest
    Byte[] xmlResponse
    String oldAccountNumber
    String newAccountNumber
    Boolean paidUpLetterRequired
    String pdmAddressLine1
    String pdmAddressLine2
    Date dateCreated
    Boolean isError
    String errorSuccessMessage
    Vehicledetail vehicledetail
    Requesttype requesttype
    Preferreddeliverymethod preferreddeliverymethod
    Suburb suburb

    static belongsTo = [Preferreddeliverymethod, Vehicledetail, Requesttype, Suburb, Status]

    static constraints = {
        xmlRequest(nullable: true)
        xmlResponse(nullable: true)
        oldAccountNumber nullable: true, maxSize: 45
        newAccountNumber(nullable: true, maxSize: 45)
        paidUpLetterRequired(nullable: true)
        pdmAddressLine1 nullable: true, maxSize: 45
        pdmAddressLine2(nullable: true, maxSize: 45)
        dateCreated(nullable: true)
        isError nullable: true
        errorSuccessMessage(nullable: true, maxSize: 45)
        vehicledetail(nullable: false)
        requesttype(nullable: false)
        preferreddeliverymethod nullable: true
        suburb nullable: true
    }

    static mapping = {
        version false
    }
}
