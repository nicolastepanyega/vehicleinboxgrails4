package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Fmaritz
 * Date: 2/27/12
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
class AuxBulkingcode {
    
    String bulkingName
    String bulkingCode


    static hasMany = [auxProductOptionBulkingConfig:AuxProductoptionbulkingconfig]

    static constraints = {
        bulkingName nullable: false, maxSize: 255
        bulkingCode(nullable: false, maxSize: 45)

    }

    static mapping = {
        version false
    }

    public String toString(){
        "${bulkingName}(${bulkingCode})"
    }
}
