package za.co.signio.model

import za.co.signio.dataModel.SignioDataModel

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2013/07/05
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
class LookupType {

    String type

    //static belongsTo = [Deal, Dealstate]

    static constraints = {
        type nullable: false
    }

    static mapping = {
        version(false)
    }
}
