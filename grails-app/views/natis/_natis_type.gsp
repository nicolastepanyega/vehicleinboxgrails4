<tr class="BGBlueButtonBarr">
	<td>
		<div style="display:inline;float:left;width:200px"><select id="natisRequestTypeSelection" style="display:inline">
			<option value="originalNatis">Original Natis</option>
			<option value="copyNatis">Copy Natis</option>
		</select></div>

		<div style="display:inline;float:left"><input type="button" title="Submit Request" value="Submit Request" id="submitNatisRequestButton"></div>
		<div style="display:inline;float:left"><input type="button" title="Return to Vehicle Inbox" value="Return to Vehicle Inbox" id="returnToVehicleInboxButton"></div>
	</td>
</tr>
 
<script type="text/javascript">

	var natisType = "${params.natisType}";
	if("original" == natisType ){
		$("#natisRequestTypeSelection").val("originalNatis");
	}
	if("copy" == natisType ){
		$("#natisRequestTypeSelection").val("copyNatis");
	}

	$("#submitNatisRequestButton").click(function(){

		// do the regular field validation
		var formValid = true;
		$.each($("#natisForm").validate().elements(), function(index, field){
			field = $(field);
			if(field.valid() == 0){
				formValid = false;
			}
		});
		// do the upload validation
		$.each(["document_pop", "document_setl"], function(index, fieldName){
			if($("#" + fieldName).is(':visible')){
				if($("#" + fieldName + "_uploaded").valid() == 0){
					formValid = false;
				}
			}
		});
		if(!formValid){
			return;
		}

        $("#natisForm").mask("Submitting Natis Request...", 0 , 300);


		// get the request type.
		var request = {
			requestType : $("#natisRequestTypeSelection").val()
		};

		// if there dealID is not specified, it means that we first need to create a vehicle.
		if(signio.vehicleInbox.dealID == -1){
			// set the vin number:
			$(".copyNatis").find("[name='vinNumber']").val($(".newVehicle").find("[name='vinNumber']").val());

			// create the vehicle object.
			var vehicle = {
				vehicleManufacturer : $(".newVehicle").find("[name='make']").val(),
				vehicleModel : $(".newVehicle").find("[name='model']").val(),
				vinNumber : $(".newVehicle").find("[name='vinNumber']").val(),
				registrationNumber : $(".newVehicle").find("[name='lisenceNumber']").val(),
				vehicleYear : $(".newVehicle").find("[name='vehicleYear']").val(),
				vehicleColour : $(".newVehicle").find("[name='vehicleColour']").val(),
				natisRegisteredNumber : $(".newVehicle").find("[name='registerNumber']").val()
			};

			// submit the vehicle code.
			signio.vehicleInbox.callAjax("../../vehicledetail/saveNewVehicle", "POST", vehicle, function(data){
				if(data.success){
					signio.vehicleInbox.dealID = data.dealID;
				}
				else{
					// some sort of error occured.
				}
			});
		}
		
		// build the field set.
		var fieldSet = (request.requestType == "originalNatis") ? $(".originalNatis").not(".deliveryForm").find(":input") : $(".copyNatis").find(":input");
		$.each(fieldSet, function(index, item){
			if($(item).attr("type") == "checkbox"){
				request[item.name] = "" + $(item).prop("checked");
			}
			else{
				request[item.name] = $(item).val();
			}
		});

		// add the delivery address if need be.
		if(request.requestType == "originalNatis"){
			if(request.originalnatis_deliveryMethod == 'Courier' || request.originalnatis_deliveryMethod == "Speed Mail"){
				$.each($(".deliveryForm").find(":input"), function(index, item){
					request[item.name] = $(item).val();
				});
			}
		}

		// add the delivery method for copy natis if need be:
		if(request.requestType == "copyNatis"){
			if($("[name='copynatis_bank']").val() == "SBSA"){
				request.copynatis_deliveryMethod = $("[name='copynatis_deliveryMethod']").val();
				request.deliveryEmail = $("[name='deliveryEmail']").val();
			}
			else{
				request.copynatis_deliveryMethod = "";
			}
		}

		// add the dealID.
		request.id = signio.vehicleInbox.dealID;

		// submit to backend.
		var result = null;
		signio.vehicleInbox.callAjaxAsync("../submitNatisRequest", "POST", request, function(data){
            $("#natisForm").unmask();
			if(data.state == "SUCCESS"){
                $('input[type="submit"]').attr('disabled','disabled');
                alert("Thank you, Request Submitted")
				if("copyNatis" == request.requestType && request.copynatis_deliveryMethod != null){
					if(request.copynatis_deliveryMethod == "BOTH" || request.copynatis_deliveryMethod == "EMAIL"){
						alert("Email sent to " + request.deliveryEmail);
					}

					if(request.copynatis_deliveryMethod == "BOTH" || request.copynatis_deliveryMethod == "DISPLAY"){
						// redirect directly to the copynatis view.
						window.location.assign("../view/" + data.dealID);
					}

					if(request.copynatis_deliveryMethod == "EMAIL"){
						window.close();
					}
				}
				else{
					window.location.assign("../../vehicleInbox/index");
				}
			}
			else{
				if("copyNatis" == request.requestType && request.copynatis_deliveryMethod != null){
					alert("Copy Natis Request Failed: \n" + data.message);
					self.close();
				} else if ("originalNatis" == request.requestType) {
                    alert("Original Natis Request Failed: \n" + data.message);
                    window.location.assign("../../vehicleInbox/index");
                }
				else{
					window.location.assign("../../vehicleInbox/index");
				}
			}
		});
	});

	$("#returnToVehicleInboxButton").click(function(){
		window.location.assign("../../vehicleInbox/index");
	});

    function showSpinner(){
        document.getElementById('spinner').style.display = 'inline';
        document.getElementById('spinner').style.display = 'none()';
    }

    function hideSpinner() {
        document.getElementById('spinner').style.display = 'none()';
        document.getElementById('error').style.display = 'none()';
    }

    function showError(e) {
        var errorDiv = document.getElementById('error')
        errorDiv.innerHTML = '<ul><li>'
                + e.responseText + '</li></ul>';
        errorDiv.style.display = 'block';
    }
</script>