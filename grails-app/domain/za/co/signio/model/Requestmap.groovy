package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: pieter
 * Date: 15/07/24
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
class Requestmap {

    String url
    String configAttribute

    static constraints = {
        url(nullable: false,maxSize: 255)
        configAttribute(nullable: false)
    }

    static mapping = {
        version(false)
        table('requestmap')
    }


}
