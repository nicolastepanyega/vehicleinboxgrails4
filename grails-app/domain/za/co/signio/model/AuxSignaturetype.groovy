package za.co.signio.model

class AuxSignaturetype {

    String type

    static hasMany = [auxProductoptionsetups:AuxProductoptionsetup]

    static constraints = {
        type maxSize: 100, nullable: false
    }

    static mapping = {
        version false
    }

    String toString() {
        return type
    }
}
