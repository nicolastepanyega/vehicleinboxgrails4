// Define the field actions.
$(document).ready(function(){
	
	// ********************************************** field variables. ********************************************** //
	var field_vehicleColour = $(".newVehicle").find("[name='vehicleColour']");
	var field_vehicleMake = $(".newVehicle").find("[name='make']");
	var field_vehicleYear = $(".newVehicle").find("[name='vehicleYear']");
	var field_vehicleModel = $(".newVehicle").find("[name='model']");
	var field_city = $("[name='city']");
	var field_suburb = $("[name='suburb']");
	var field_postalcode = $("[name='postalCode']");
	var field_originalNatisDeliveryMethod = $("[name='originalnatis_deliveryMethod']");
	var field_copyNatisDeliveryMethod = $("[name='copynatis_deliveryMethod']");
	var field_requestType = $("#natisRequestTypeSelection");
	var field_copyNatisBank = $("[name='copynatis_bank']");
	var field_originalNatisBank = $("[name='originalnatis_bank']");
    var field_accountNumber = $("[name='accountNumber']");
	
	// ********************************************** field actions ********************************************** //      
	// Create the File Upload Actions.
	$.each(["document_pop", "document_setl"], function(index, fieldName){
		upclick({
			element: document.getElementById(fieldName),
			action: '../../natis/uploadDocument',
			action_params: {document:fieldName, id:signio.vehicleInbox.dealID, _:$.now(), mimeType: "not set!" },
			dataname: "filedata",
			accept: {
				mime:"application/pdf,image/tiff,image/png,image/jpeg", 
				filetype:"PDF|JPEG|JPG|TIFF|PNG",
				error : function(){
					$("#" + fieldName + "_label").html("<div style='display:inline' class='error'>Only pdf and images can be uploaded</div>");
					$("#" + fieldName + "_uploaded").val("false");
				}
			},
			onstart: function(filename, form) { 
				// set the mimetype, depending on the fileName
				var fileExtension = filename.toUpperCase();
				fileExtension = fileExtension.substring(fileExtension.lastIndexOf(".") + 1);
				var mimeType = {
					JPG : "image/jpeg",
					JPEG: "image/jpeg",
					PNG : "image/png",
					TIFF: "image/tiff",
					PDF : "application/pdf"
				}[fileExtension];
				$(form).find("[name='mimeType']").val(mimeType);
				// set the response message.
				$("#" + fieldName + "_label").html("Uploading " + filename + "...");
			},
			oncomplete: function(response_data)  {
				response_data = $.parseJSON(response_data);
				if(response_data.uploaded){
					$("#" + fieldName + "_label").html("Upload Successful");
					$("#" + fieldName + "_uploaded").val("true");
					$("#" + fieldName + "_uploaded").valid();
				}
				else{
					$("#" + fieldName + "_label").html("Upload failed. Please Retry");
					$("#" + fieldName + "_uploaded").val("false");
				}
			}
		});
	});
	
	// Original Natis Bank Selection
	var selectOriginalNatisBank = function(){
		if(field_requestType.val() == "originalNatis" && (field_originalNatisBank.val() == "WESBANK" || field_originalNatisBank.val() == "SBSA")){
			$(".fileUploadForm").show();
            $("#natisForm").validate().element(field_accountNumber);
		}
		else{
			$(".fileUploadForm").hide();
		}
	};

	// Copy Natis Bank Selection
	var selectCopyNatisBank = function(){
		if(field_requestType.val() == "copyNatis" && field_copyNatisBank.val() == "SBSA"){
			$(".copyNatisDeliveryMethod").show();
		}
		else{
			$(".copyNatisDeliveryMethod").hide();
		}
	};
	
	// Natis Type selection
	var selectNatisType = function(){
		selectCopyNatisBank();
		selectCopyNatisDeliveryMethod();
		selectOriginalNatisBank();
		selectOriginalNatisDeliveryMethod();
		
		if(field_requestType.val() == "originalNatis"){
			$(".copyNatis").not(".originalNatis").hide();
			$(".originalNatis").show();
		}
		if(field_requestType.val() == "copyNatis"){
			$(".originalNatis").not(".copyNatis").hide();
			$(".copyNatis").show().removeClass("ignorevalidation");
		}
	};
	
	// Vehicle Colour Selection.
	var selectColour = function(){
		var options = "<option value='-1'>-select-</option>";
		$.each(signio.vehicleInbox.callAjax("../../vehicleDetailUtil/getVehicleColours"), function(index, item){
			options += "<option value='" + item + "'>" + item + "</option>";
		});
		field_vehicleColour.find("option").remove().end().append(options);
	};

	// Vehicle Make Selection
	var setMake = function(){		
		var options = "<option value='-1'>-select-</option>";
		$.each(signio.vehicleInbox.callAjax("../../vehicleDetailUtil/getVehicleManufacturers"), function(index, item){
			options += "<option value='" + item[0]  + "'>" + item[1] + "</option>";
		});
		field_vehicleMake.find("option").remove().end().append(options);
		field_vehicleYear.find("option").remove().end().append("<option value=-1>-select-</option>");
		field_vehicleModel.find("option").remove().end().append("<option value=-1>-select-</option>");
	};
	
	// Vehicle year selection
	var setYear = function(){
		var options = "<option value='-1'>-select-</option>";
		$.each(signio.vehicleInbox.callAjax("../../vehicleDetailUtil/getVehicleYears?id=" + field_vehicleMake.val()), function(index, item){
			options += "<option value='" + item  + "'>" + item + "</option>";
		});
		field_vehicleYear.find("option").remove().end().append(options);
		field_vehicleModel.find("option").remove().end().append("<option value=-1>-select-</option>");
	};
	
	// City Selection
	var setCity = function(){
		var options = "<option value=-1>-select-</option>";
		$.each(signio.vehicleInbox.callAjax("../../address/getCities"), function(index, option){
			options += "<option value='" + option.key + "'>" + option.value + "</option>";
		});
		field_city.find("option").remove();
		field_city.append(options)
	};
	
	// Suburb Selection
	var setSuburb = function(){
		var count = 0;
		var options = "";
		$.each(signio.vehicleInbox.callAjax("../../address/getSuburbs/" + field_city.val()), function(index, option){
			options += "<option value='" + option.key + "'>" + option.value + "</option>";
			count++;
		});

		// if there is more than one suburb, add the select field.
		if(count > 1){
			options = "<option value='-1'>-select-</option>" + options;
		}
		// if there is no suburbs, add a -no suburbs for city- message.
		if(count == 0){
			options = "<option value='-1'>-no suburbs-</option>" + options;
		}
		
		// populate the suburb field.
		field_suburb.find("option").remove();
		field_suburb.append(options);
		
		// if there is only one suburb, pre-fetch its postal codes.
		if(count == 1){
			field_suburb.change();
		}
		// if there is more or less than one suburb, clear the options.
		if(count != 1){
			field_postalcode.find("option").remove();
			field_postalcode.append("<option value=''-1'>-select suburb first-</option>");
		}
	};
	
	// Postal Code Selection
	var setPostalCode = function(){
		var count = 0;
		var options = "";
		$.each(signio.vehicleInbox.callAjax("../../address/getPostalCode/" + field_suburb.val()), function(key, value){
			options += "<option value='" + value + "'>" + value + "</option>";
			count++;
		});
		// if there is only one postal code, use it.
		if(count > 1){
			options = "<option value='-1'>-select-</option>" + options;
		}
		field_postalcode.find("option").remove();
		field_postalcode.append(options);
	};
	
	// Original Natis Delivery Method Selection
	selectOriginalNatisDeliveryMethod = function(){
		if(field_requestType.val() == "originalNatis" && field_originalNatisDeliveryMethod.val() != "Collection"){
			$(".deliveryForm").show();
		}
		else{
			$(".deliveryForm").hide();
		}
	};
	
	// Copy Natis Delivery Method Selection.
	selectCopyNatisDeliveryMethod = function(){
		if(field_copyNatisDeliveryMethod.val() == "DISPLAY"){
			$(".copyNatisEmail").hide();
		}
		else{
			$(".copyNatisEmail").show();
		}
	};
	
	// ********************************************** field action assignment ********************************************** //
	// natis type
	field_requestType.change(selectNatisType);
	// vehicle details.
	field_vehicleMake.change(setYear);
	// address details.
	field_city.change(setSuburb);
	field_suburb.change(setPostalCode);
	// delivery method details.
	field_originalNatisDeliveryMethod.change(selectOriginalNatisDeliveryMethod);
	field_copyNatisDeliveryMethod.change(selectCopyNatisDeliveryMethod);
	// bank details.
	field_copyNatisBank.change(selectCopyNatisBank);
	field_originalNatisBank.change(selectOriginalNatisBank);
	
	// ********************************************** prepopulate functions ********************************************** //
	selectNatisType();
	selectColour();
	setMake();
	setCity();
});