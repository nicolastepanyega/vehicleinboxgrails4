package za.co.signio.model

class Dealdocument {
    String filename
    Date dateAdded
    String comment
    String uri
    Boolean isCertified
    Integer dealDataVersion
    Dealstate dealstate

    static belongsTo = [Deal, Dealstate, Documenttemplategroup, Dealdocumenttype, Note, Member]

    static hasOne = [dealdocumenttype:Dealdocumenttype,
                     deal:Deal,
                     documenttemplategroup:Documenttemplategroup,
                     member:Member]

    static hasMany = [dealDataDocuments: Dealdatadocument, notes: Note]

    
    static constraints = {
        filename(maxSize: 255, nullable:false)
        dateAdded(nullable: false)
        comment nullable: true
        uri (maxSize: 255, nullable: false)
        isCertified(nullable: true)
        dealDataVersion (nullable: true)
        dealstate nullable: false
        documenttemplategroup nullable: false
        deal nullable: false
        dealdocumenttype nullable: false
        member nullable: true
    }

    static mapping = {
        version false
        notes joinTable: 'dealdocument_note'
    }

    def beforeInsert = {
        dateAdded = new Date()
    }

    String toString(){
        return filename
    }
}
