package za.co.signio.model

/**
 * Created by stephan_coetzer on 2016-06-20.
 */
class Otpaction {

    String action

    static mapping = {
        version false
    }

    static hasMany = [otps:Otp]

    static constraints = {
        action(nullable: false)
    }

}
