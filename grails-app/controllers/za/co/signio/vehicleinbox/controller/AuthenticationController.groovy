package za.co.signio.vehicleinbox.controller

import groovy.util.logging.Slf4j
import groovy.xml.StreamingMarkupBuilder

import za.co.signio.model.Institution;
import za.co.signio.model.Member;
import za.co.signio.model.Singlesignon;
import vehicleinbox.Grailsauth;
import za.co.signio.security.utils.StringEncryption;
import java.text.SimpleDateFormat
import grails.util.Environment
@Slf4j
class AuthenticationController {

	
	
	def index(){
		if (params.authNr == null) {
			chain(action:"unauthorized");
			return
		}

		// get the authentication
		String authNr = StringEncryption.getDecryptedString(URLDecoder.decode(params.authNr, "utf-8"))
		log.info(authNr);

		Grailsauth auth = Grailsauth.findByAuthenticationNumber(authNr);
	
		// if the user is authenticated.
		if(auth != null){
			log.info(params);
			session["memberID"] = Member.findByIdNumber(params.fiid).id;
			session["institutionID"] = Institution.findByThirdPartyId(params.thirdPartyID).id;

			log.info("user: " + session["memberID"] + " (institution:'" + session["institutionID"] + "') authenticated");
			// set the session variables.
			session["institutionID"] = Institution.findByThirdPartyId(params.thirdPartyID).id;
			auth.delete();
//			render('<html><head><meta http-equiv="refresh" content="0; URL=../vehicleInbox/index"></head></html>');

			log.info("Full session: ${session}")

			redirect([controller: "vehicleInbox", action: "index"])
		}
		// if the user is not authenticated.
		else{
			chain(action:"unauthorized");
		}
	}
	
	def unauthorized = {
		chain(controller: 'error', action: 'viError', model:[shortMessage: "The session is not authenticated. Please contact support."])
	}
	
	def authenticate(){
		if (params.authNr == null) {
			chain(controller: 'error', action: 'viError', model:[shortMessage:"Unauthorized to access Vehicle Inbox. Please contact support."]);
			return
		}

		// get the signon token and the
		Singlesignon signOn = Singlesignon.findByAuthKey(StringEncryption.getDecryptedString(URLDecoder.decode(params.authNr, "utf-8")));
		if(signOn == null){
			chain(controller: 'error', action: 'viError', model:[shortMessage:"Unauthorized to access Vehicle Inbox. Please contact support."]);
			return;
		}
		
		// get the parameters.
		def parameters = new XmlSlurper().parseText(signOn.getXml()).param;
		signOn.delete();
		
		// get the locator.
		Map locator = [
			"vehicleInbox":			[url:"../vehicleInbox/index",				mode:RedirectMode.REDIRECT],
			"sbsaCustomerSearch" : 	[url:"../dealSearch", 						mode:RedirectMode.REDIRECT],
			"copyNatis" : 			[url:"../natis/index/-1?natisType=copy",	mode:RedirectMode.FORMPOST, extraParams:[ bank:"SBSA"]]
		].get(parameters.find{ it.@name.text() == "Action" }.toString());
		
		// set the institution and memberID.
		session["institutionID"] = Institution.findByThirdPartyId(parameters.find{ it.@name.text() == "MerchantID" }.toString())?.id;
		session["memberID"] =  Member.findByIdNumber(parameters.find{ it.@name.text() == "IDNumber" }.toString())?.id;
		
		log.info("Authenticated memberID: " + session["memberID"] + " institutionID: " + session["institutionID"] + " Redirecting to: " + locator.url);
		
		switch(locator.mode){
			// redirect using a simple HTTP Redirect Mode.
			case RedirectMode.REDIRECT:
				renderRedirect(parameters, locator);
				render('<html><head><meta http-equiv="refresh" content="0; URL=' + locator.url + '"></head></html>');
				break;
			// redirect using a post command.
			case RedirectMode.FORMPOST:
				StringBuilder sb = new StringBuilder("<html><head></head><body><form action='" + locator.url + "' method='post' id='redirect'>");
				parameters.findAll{ !["Action", "MerchantID", "IDNumber"].contains(it.@name.text()) }.each{ param ->
					sb.append("<input type='hidden' name='" + param.@name.text()  + "' value='" + param.toString() + "'>")
				}
				if(locator.extraParams != null){
					locator.extraParams.each { key, value ->
						sb.append("<input type='hidden' name='" + key  + "' value='" + value + "'>")
					}
				}
				sb.append('</form>');
				sb.append('<script type="text/javascript">window.onload = function(){document.getElementById("redirect").submit();};</script>');
				sb.append('</body></html>');
				render sb;
				break;
		}
	}
	
	// THIS FUNCTION IS FOR TESTING PURPOSES ONLY!
	def testingAuth(){
        if ('live'.equals(Environment.currentEnvironment.name)){
            chain(controller: "error",
                    action: "viError",
                    model: [
                            shortMessage: "This URL only works in the test environments!"
                    ]
            )
        } else {
            session["institutionID"] = 807L;
            session["memberID"]  = "12";
            log.info("Test authentication used");
            render('<html><head><meta http-equiv="refresh" content="0; URL=../vehicleInbox/index"></head></html>');
        }
	}

	// THIS FUNCTION IS FOR TESTING PURPOSES ONLY!
	def generateSSO(){
		session["institutionID"] = null;
		session["memberID"]  = null;
		String authKey = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + 12;

		
		Writer writer = new StringWriter();
		def builder = new StreamingMarkupBuilder().bind(){
			params(){
				param(name:"Action", request.getParameter("page"))
				param(name:"IDNumber", "2080000000000")
				param(name:"MerchantID", "3RD00004")
				request.getParameterMap().findAll{ key, value -> key != 'page' }.each { key, value -> param(name:key, value)  }
			}
		};
		new Singlesignon(authKey:authKey, dateCreated:new Date(), xml:builder.toString()).save(flush: true, failOnError: true);

		log.info("generated authentication token: " + authKey);
		authKey = URLEncoder.encode(StringEncryption.getEncryptedString(authKey), "utf-8");
		render('<html><head>Generated Authentication Token: <a href="authenticate?authNr=' + authKey + '">' + authKey + '</head></html>');
	}
	
	public static enum RedirectMode{
		REDIRECT,
		FORMPOST
	}
}
