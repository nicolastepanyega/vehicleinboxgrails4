package za.co.signio.model

class Suburb {

    String name
    String boxCode
    String streetCode
    City city

    static belongsTo = [City]
    static hasMany = [institutions: Institution, requests:Request, members:Member]

    static mapping = {
        version false
    }

    static constraints = {
        name nullable: false, maxSize: 255
        boxCode nullable: true, maxSize: 5
        streetCode nullable: true, maxSize: 5
    }


    String toString(){
        return name
    }
}
