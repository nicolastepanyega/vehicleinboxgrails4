package za.co.signio.model

class Addendumtype {

    String type

    static hasMany = [addendums:Addendum]

    static mapping = {
        version false
    }

    static constraints = {
        type nullable: false, maxSize: 45
    }

    String toString() {
        return type
    }
}
