package za.co.signio.model

class Moduletemplate {

    String name

    static hasMany = [modules:Module,moduleactions:Moduleaction,relationshipmoduleconfigModuletemplates:RelationshipmoduleconfigModuletemplate]
    static belongsTo = [Moduleaction]

    static mapping = {
        version false
        modules joinTable: [name: "module_moduletemplate", key: "moduletemplate_id"]
        moduleactions joinTable: [name: "moduletemplate_moduleaction", key: "moduletemplate_id"]
    }

    static constraints = {
        name nullable: false, maxSize: 100
    }
}
