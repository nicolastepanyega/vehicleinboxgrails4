package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/02/12
 * Time: 1:34 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxDeliverymethodtype {

    String type

    static hasMany = [auxDeliverymethods: AuxDeliverymethod]

    static constraints = {
        type maxSize: 45, nullable: false
    }

    static mapping = {
        version false
    }
}
