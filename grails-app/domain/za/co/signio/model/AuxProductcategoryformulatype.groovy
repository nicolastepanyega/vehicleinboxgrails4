package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/05/27
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxProductcategoryformulatype {

    String type

    static hasMany = [auxProductcategoryformulas: AuxProductcategoryformula]

    static constraints = {
        type maxSize: 45, nullable: false
    }

    static mapping = {
        version false
    }
}
