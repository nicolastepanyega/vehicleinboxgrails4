package za.co.signio.vehicleinbox.controller

import groovy.util.logging.Slf4j
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder
import java.text.SimpleDateFormat
import za.co.signio.model.Singlesignon;
import za.co.signio.security.utils.StringEncryption;
@Slf4j
class RedirectController {

	def redirectToVAF2(){
		String authKey = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + session["memberID"];
		String url = "https://dev.signio.co.za/SigningBoardroom/v5integration.aspx?oAuth=" +  StringEncryption.getEncryptedString(authKey);
		Writer writer = new StringWriter();
		def builder = new StreamingMarkupBuilder().bind(){ 
			params(){
				param(name:"Action", "sbsaSettlements")
				param(name:"DealNumber", request.getParameter('dealNumber'))
			}
		};
		new Singlesignon(authKey:authKey, dateCreated:new Date(), xml:builder.toString()).save(flush: true, failOnError: true);
		render('<html><head><meta http-equiv="refresh" content="0; URL=' + url + '"></head></html>');
	}
}
