package za.co.signio.model

/**
 * Created by stephan_coetzer on 2016-06-20.
 */
class Otpdeliverymethod {

    String method

    static mapping = {
        version false
    }

    static hasMany = [otps:Otp]

    static constraints = {
        method(nullable: false)
    }

}
