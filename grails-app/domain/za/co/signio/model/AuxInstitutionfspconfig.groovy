package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/11/14
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxInstitutionfspconfig {

    Boolean isActive
    static belongsTo = [Institution, AuxFspnumber]
    static hasOne = [institution1 :Institution,
                     institution2 :Institution,
                     auxFspnumber: AuxFspnumber]

    static hasMany = [auxProductoptionInstitutionFspConfigs:AuxProductoptioninstitutionfspconfig]

    static constraints = {
        auxFspnumber(nullable: true)
        isActive(nullable: false)
        institution1(nullable: false) //Supplier
        institution2(nullable: false) //Dealer
    }

    static mapping = {
        version false
    }

    public String toString(){
        "${auxFspnumber?.fspNumber} ${institution1?.name} ${institution2?.name}"
    }
}
