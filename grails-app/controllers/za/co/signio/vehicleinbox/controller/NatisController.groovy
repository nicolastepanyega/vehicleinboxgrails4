package za.co.signio.vehicleinbox.controller

import groovy.util.logging.Slf4j
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat
import com.lowagie.text.pdf.AcroFields
import com.lowagie.text.pdf.PdfReader
import com.lowagie.text.pdf.PdfStamper

import za.co.signio.vehicleinbox.helper.NatisHelper;
import za.co.signio.dataModel.SignioDataModel;
import za.co.signio.model.Deal
import za.co.signio.model.Dealstate;
import za.co.signio.model.Dealtype;
import za.co.signio.model.Institution;
import za.co.signio.model.InstitutionInstitution;
import za.co.signio.model.Member;
import za.co.signio.webservice.ThirdParty;
import za.co.signio.model.Documenttemplate;
import za.co.signio.webservice.WebServiceResponse
import za.co.signio.webservice.WebServiceResponseState;
import grails.converters.JSON
import org.apache.commons.codec.binary.Base64
import za.co.signio.model.Dealdocument
import za.co.signio.model.Dealdocumenttype
import za.co.signio.model.Documenttemplategroup
import java.text.DateFormatSymbols
import com.sun.imageio.plugins.jpeg.JPEG
@Slf4j

class NatisController {

	//private ConfigObject config = Holders.config;
/*	private static final log log = log.getlog(this);*/
	def thirdPartyWebserviceRequestService;
	def mailService;

	def getNatisData(){
		if(params.id == null || params.id.isEmpty()){
			render ([error: "No Deal ID was set"]) as JSON;
			return;
		}

		// get the deal.
		Deal deal = (Deal) Deal.get(params.id);
		if(deal == null){
			render ([error: "No deal with ID " + params.identity + "was found"]) as JSON;
			return;
		}

		// get the model.
		SignioDataModel sdm = (SignioDataModel) deal.getSignioDataModel("originalXml");

		log.info("Member ID: ${session["memberID"]}")
		String fAndIName = ""
		String fAndIPhoneNumber = "";
		String fAndICellNumber = "";
		String fAndIEmail = ""
		Member member = (Member) Member.get((Serializable) session["memberID"]);

		if (member != null) {
			log.info("Member Found: ${member.getFirstName()} ${member.getLastName()}")
			fAndIName = "${member.getFirstName()} ${member.getLastName()}";
			fAndIPhoneNumber = (member.getContactinfo()?.getTelOffice() != null)? member.getContactinfo()?.getTelOffice() : "";
			fAndICellNumber = (member.getContactinfo()?.getCellphoneNumber() != null)? member.getContactinfo()?.getCellphoneNumber() : "";
			fAndIEmail = (member.getContactinfo()?.getEmail() != null)? member.getContactinfo()?.getEmail() : "";
		}

		if(deal.getDealtype().getType() == "VehicleInbox"){
			def res = [
				make : deal?.getVehicle()?.getVehiclemanufacturer()?.getName(),
				model : deal?.getVehicle()?.getDescription(),
				vinNumber : sdm.fields.vinNumber?.value,
				engineNumber : sdm.fields.engineNumber?.value,
				registerNumber : sdm.fields.natisRegisteredNumber?.value,
				licenseNumber : sdm.fields.registrationNumber?.value,
				// Dealer Detaill
				attention: fAndIName,
				telephoneNumber: fAndIPhoneNumber,
				cellphoneNumber: fAndICellNumber,
				emailAddress: fAndIEmail
			];
			render res as JSON;
			return;
		}

		log.error("create a vehicleInbox deal first!");
		render [:] as JSON;
	}

	def getAvailableBanks() {

		if(params.id == null || params.id.isEmpty()){
			render ([:] as JSON);
			return;
		}

		// Copy Natis Request from Front-end (For SBSA)
		if ("-1".equals(params.id)) {
			log.info("ID= ${params.id}");
			if (params.bank != null && !params.bank.isEmpty()) {
				log.info("Bank= ${params.bank}");
				NatisBank bank = NatisBank.getBankByCode(params.bank);
				if (bank != null) {
					render (["${bank.getBankCode()}": bank.getBankName()] as JSON);
					return;
				}
			}
			render ([:] as JSON);
			return;
		}

		Deal deal = (Deal) Deal.get(params.id);
		if(deal == null){
			render ([:] as JSON);
			return;
		}

		Institution dealership = deal.dealgroup.institution;
		HashMap<String, String> bankMap = new HashMap<String, String>()
		NatisBank[] banks;
		if (params.natisType != null && "original".equals(params.natisType)) {
			banks = NatisBank.getOriginalNatisBanks();
		} else {
			banks = NatisBank.values();
		}
		for (NatisBank bank : banks) {
			InstitutionInstitution ii = InstitutionInstitution
					.findByInstitution1AndInstitution2(Institution.findById(bank.getInstitutionID()), dealership);

			if (ii != null) {
				bankMap.put(bank.getBankCode(), bank.getBankName())
			}
		}
		render (bankMap as JSON);
	}

	def submitNatisRequest() {
		try {
			JSONObject json = request.JSON;
			// extract the natis Documents from the session.
			Map<String, Map<String, String>> natisDocuments = (Map<String, Map<String, String>>) session["natisDocuments"];
			if(("" + json.id).equals(session["natisDealID"])){
				log.info("set documents");
				for(Map.Entry<String, Byte[]> document : natisDocuments.entrySet()){
					json.put(document.getKey(), document.getValue());
				}
			}

			Deal deal = (Deal) Deal.get(json.id);

			// if this is not a vehicle inbox deal, we first need to create a vehicle inbox deal.
			if(deal.getDealtype().getType() != "VehicleInbox"){
				log.error("CREATE A VEHICLE INBOX DEAL FIRST");
			}

			// generates the batchRef.
			json.BatchRef = new SimpleDateFormat("yMMddHHmmssSSS").format(new Date())

			// placeholder for the serviceResponse.
			Map<String, String> serviceResponse = null;

			// if this is a copy natis request
			if(json.requestType == "copyNatis"){
				serviceResponse = submitCopyNatisRequest(deal, json);
			}

			// if this is a original natis request.
			if(json.requestType == "originalNatis"){
				serviceResponse = submitOriginalNatisRequest(deal, json);
			}

			if (serviceResponse != null) {
				render serviceResponse as JSON;
			}
			else{
				render ([
						state: "ERROR",
						message: "Request failed with Unknown Error."
					] as JSON);
			}

			// if everything went well, remove the documents.
			session["natisDocuments"] = null;
		} catch (Exception e) {
			log.error(e.getMessage(), e)
			render ([
					state: "ERROR",
					message: "${e.getMessage()}"
				] as JSON);
		}
	}

	def uploadDocument(){
		byte[] fileContents = request.getFile("filedata").inputStream.getBytes();
		String documentType = request.getParameter("document");
		String mimeType = request.getParameter("mimeType");
		String natisDealID = request.getParameter("id");

		if(session["natisDocuments"] == null || natisDealID != session["natisDealID"]){
			session["natisDealID"] = natisDealID;
			session["natisDocuments"] = new HashMap<String, Map<String, String>>();
		}
		session["natisDocuments"].put(documentType, [mimeType: mimeType, fileContents:fileContents]);

		log.info("Files uploaded for OriginalNatis dealID: " + session["natisDealID"] + " - " + session["natisDocuments"].keySet());
		String output = (([document:documentType, uploaded:"true"]) as JSON);
		render output;
	}

	def view(){
		// make sure we have a valid id...
		if(params.id == null || params.id.isEmpty()){
			render ([error:"no ID specified"]) as JSON;
			return;
		}

		// get the deal- make sure it is the right state.
		Deal deal = (Deal) Deal.get(params.id);
		Integer state = deal.getDealstate().getStateCode();
		byte[] pdf = null;
		Map<String, String> nameAndTypeMap = [fileName: "", mimeType: "application/pdf"]
		// CopyNatis - Received
		if(state == 60300){
			pdf = generateCopyNatis(deal, nameAndTypeMap);
		}
		// OriginalNatis - Received
		if(state == 61400){
			pdf = generateOriginalNatis(deal);
		}

		if(pdf!=null){
			response.setContentLength(pdf.length);
			response.contentType = nameAndTypeMap.get("mimeType")
			OutputStream out = response.getOutputStream();
			out.write(pdf)
			out.flush()
			out.close()
		}
		else{
			render ([error: "Deal was in state: '" + deal.getDealstate().getDescription() + "' and could not be rendered." ]) as JSON;
		}
	}

	private Map<String, String> submitCopyNatisRequest(Deal deal, JSONObject json){

		// Get the correct bank name.
		json.bank = [
			"ABSA" : "ABSA BANK",
			"WESBANK" : "Wesbank",
			"MFC" : "MFC",
			"SBSA" : "STANDARD BANK"
		].get(json.copynatis_bank);

		if(json.bank == null){
			log.error("No valid bank was selected!");
			return [
				state: 'ERROR',
				message: 'No valid bank was selected!'
			];
		}
		
		// Linking MFC to MFC-Fox !!!
		def originatorID = ["ABSA" : 3, "WESBANK" : 4, "MFC" : 15, "SBSA" : 2].get(json.copynatis_bank);
		
		if(originatorID == null){
			log.error("System could not find the bank selected.");
			return [
				state: 'ERROR',
				message: 'System could not find the bank selected.'
			];
		}

		Dealtype  dealType =  Dealtype.findByType("CopyNatis");
		Institution institution = null;
		InstitutionInstitution ii = null;
		Deal copyNatisDeal = null;
		
		try {
			institution = Institution.findByOriginatorId(originatorID);
		}
		catch (Exception e) {
			def errorMessage = 'System could not resolve the bank selected.';
			log.error(errorMessage);
			log.error(e.getMessage());
			return [
				state: 'ERROR',
				message: errorMessage
			];
		}

		try {
			ii = InstitutionInstitution.findByInstitution1AndInstitution2(institution, deal.dealgroup.institution);
		}
		catch (Exception e) {
			def errorMessage = 'System could not resolve the dealership\'s dealer code.' 
			log.error(errorMessage);
			log.error(e.getMessage());
			return [
				state: 'ERROR',
				message: errorMessage
			];
		}
		
		// create the XML that has to be sent.
		String originalXML = NatisHelper.composeCopyNatisXML(json, session);
		
		// save the copynatis deal.
		//If for some reason a deal of CopyNatis exists on the dealgroup that was send in from the frontend you should find the existing deal of that type and not insert a new one.
		try {
			copyNatisDeal = Deal.findByDealgroupAndParentIdAndDealtypeAndInstitution(deal.getDealgroup(),deal.getId(),dealType,institution);
			
			if (copyNatisDeal == null) {
				copyNatisDeal = new Deal(
					batchRef: json.BatchRef, originalXml: originalXML, transformedXml: "", dateCreated: new Date(),
					parentId:deal.id, vehicle: deal.vehicle, institution: institution, institutionInstitution: ii,
					dealstate: Dealstate.findByStateCode(60100), dealtype: dealType, dealgroup: deal.dealgroup,
					isResendable: false, member:(Member) Member.get(session["memberID"]));
				copyNatisDeal.save(flush: true, failOnError: true)
			} else {
				copyNatisDeal.setOriginalXml(originalXML);
				copyNatisDeal.setDateUpdated(new Date()) ;
				copyNatisDeal.save(flush: true, failOnError: true)
			}
		}
		catch (Exception e) {
			def errorMessage = 'System could not save the Copy of Natis request.'
			log.error(errorMessage);
			log.error(e.getMessage());
			return [
				state: 'ERROR',
				message: errorMessage
			];
		}
		
		// call the IVID WebService.
		WebServiceResponse serviceResponse = thirdPartyWebserviceRequestService.sendRequest(ThirdParty.IVID, ThirdParty.IVID.REGISTRATION_INFO, originalXML);

		// get the state code.
		String stateCode = [
			(WebServiceResponseState.ERROR) : "60200",
			(WebServiceResponseState.FAILED) : "60200",
			(WebServiceResponseState.SUCCESS) : "60300"
		].get(serviceResponse.getState());

		// get the state message.
		String message = serviceResponse.getMessage();

		// Check for document
		boolean createDealDocument = false;
		def registrationInformationQueryResponse = null;
		if (serviceResponse.getState() == WebServiceResponseState.SUCCESS) {
			registrationInformationQueryResponse = new XmlSlurper().parseText(serviceResponse.getResult());
			createDealDocument = (registrationInformationQueryResponse?.success?.documentImage?.originalFilename?.text() != null
					&& !"".equals(registrationInformationQueryResponse?.success?.documentImage?.originalFilename?.text()));
		}


		// get the transformed XML
		String transformedXML =
			(serviceResponse.getState() == WebServiceResponseState.SUCCESS) ? NatisHelper.composeCopyNatisSDM(serviceResponse.getResult()) :
			(serviceResponse.getState() == WebServiceResponseState.FAILED) ? serviceResponse.getResult() : serviceResponse.getMessage();

		// SPECIAL RULE: is WebServiceResponseCode is (FAILED) and message is "Authentication Failed", set state to "IVID offline" (resubmit later);
		if(serviceResponse.getState() == WebServiceResponseState.FAILED && message == "Authentication failed"){
			message = "IVID Service currently Offline."
			//stateCode = 12345 or whatever
		}

		// SPECIAL RULE: if WebserviceResponseCode is (ERROR_ and the message is "System Currently Offline", set the state to "IVID offline" (resubmit later)
		if(serviceResponse.getState() == WebServiceResponseState.FAILED && message == "System Currently Offline"){
			message = "Remote system currently Offline."
			//stateCode = 12345 or whatever
		}

		// update the Deal.
		copyNatisDeal.setTransformedXml(transformedXML);
		copyNatisDeal.setComment(message);
		copyNatisDeal.setDealstate(Dealstate.findByStateCode(stateCode));
		copyNatisDeal.save(flush: true, failOnError: true)

		if (createDealDocument) {
			try {
				Dealdocument dealDocument = Dealdocument.findByDealAndDealstateAndDealdocumenttype(copyNatisDeal,
					Dealstate.findByStateCode(60300), Dealdocumenttype.findByType('COPYOFNATIS'))

				if (dealDocument == null) {
					dealDocument = new Dealdocument()
					dealDocument.setDeal(copyNatisDeal)
					dealDocument.setDealstate(Dealstate.findByStateCode(60300))
					dealDocument.setDealdocumenttype(Dealdocumenttype.findByType('COPYOFNATIS'));
					dealDocument.setDocumenttemplategroup(Documenttemplategroup.findByName('Unassigned'));
				}

				String fileName = registrationInformationQueryResponse?.success?.documentImage?.originalFilename?.text();
				String uri = buildDocumentPath(copyNatisDeal);
				byte[] documentBytes =
					Base64.decodeBase64(((String)registrationInformationQueryResponse?.success?.documentImage?.documentContent?.text()).getBytes());

				saveFileToDocumentRepository(copyNatisDeal, documentBytes, uri, fileName);

				dealDocument.setDateAdded(new Date())
				dealDocument.setFilename(fileName)
				dealDocument.setUri(uri)
				dealDocument.save(flush: true, failOnError: true)
			} catch (e) {
				log.error("Failed to save document to repo", e);
				return [
					state: "ERROR",
					message: e.getMessage(),
					dealID: "$copyNatisDeal.id"
				];
			}

		}

		// if the call was successful, check if we dont by any chance have to email the Natis Document to someone.
		if(serviceResponse.getState() == WebServiceResponseState.SUCCESS && (json.copynatis_deliveryMethod == "EMAIL" || json.copynatis_deliveryMethod == "BOTH")){
			emailDocument(json.bank, json.deliveryEmail, json.vinNumber, copyNatisDeal)
		}

		// create the appropriate response.
		return [
			state: serviceResponse.getState().name(),
			message: serviceResponse.getMessage(),
			dealID: "$copyNatisDeal.id"
		];
	}

	private Map<String, String> submitOriginalNatisRequest(Deal deal, JSONObject json){
		// get the correct bank name.
		json.bank = [
			"ABSA" : "ABSA BANK",
			"WESBANK" : "Wesbank",
			"MFC" : "MFC",
			"SBSA" : "STANDARD BANK"
		].get(json.originalnatis_bank);

		if(json.bank == null){
			log.error("No valid bank was selected!");
			return [
				state: 'ERROR',
				message: 'No valid bank was selected!'
			];
		}

		// parse build the XML request that will be sent.
		String originalXML = NatisHelper.composeOriginalNatisXML(json, session);

		// Get the institution institution...
		Institution bank = Institution.findByName(json.bank) ;
		InstitutionInstitution ii = InstitutionInstitution.findByInstitution1AndInstitution2(bank , deal.dealgroup.institution)

		// save the original natis deal.
		Deal originalNatisDeal = new Deal(
			batchRef: json.BatchRef, originalXml: originalXML, transformedXml: "", dateCreated: new Date(), institutionInstitution:ii,
			parentId:deal.id, vehicle: deal.vehicle, institution: bank, dealstate: Dealstate.findByStateCode("61100"),
			dealtype: Dealtype.findByType("OriginalNatis"), dealgroup: deal.dealgroup, isResendable: true, member:Member.get(session["memberID"]));
		originalNatisDeal.save(flush: true, failOnError: true)

		// call the CERTRACK webservice.
		ThirdParty.CERTRACK bankService = ThirdParty.CERTRACK.valueOf(json.originalnatis_bank + "_REQUEST")
		WebServiceResponse serviceResponse = thirdPartyWebserviceRequestService.sendRequest(ThirdParty.CERTRACK, bankService, originalXML);

		// get the state code.
		String stateCode = [
			(WebServiceResponseState.ERROR) : "61300",
			(WebServiceResponseState.FAILED) : "61300",
			(WebServiceResponseState.SUCCESS) : "61200"
		].get(serviceResponse.getState());

		// get the transformed XML
		String transformedXML =
			(serviceResponse.getState() == WebServiceResponseState.SUCCESS) ? NatisHelper.composeOriginalNatisSDM(originalXML, serviceResponse.getResult()) :
			(serviceResponse.getState() == WebServiceResponseState.FAILED) ? serviceResponse.getResult() : serviceResponse.getMessage();

		// get the comment to be displayed to the user.
		String comment =
			(serviceResponse.getState() == WebServiceResponseState.SUCCESS) ? "Submitted" :
			(serviceResponse.getState() == WebServiceResponseState.FAILED) ? serviceResponse.getMessage() : "Service Currently Offline";

		// get the batchref number.
		String batchRef = (serviceResponse.getState() == WebServiceResponseState.SUCCESS) ? serviceResponse.getMessage() : originalNatisDeal.getBatchRef();

		// update the Deal.
		originalNatisDeal.setTransformedXml(transformedXML);
		originalNatisDeal.setComment(comment);
		originalNatisDeal.setDealstate(Dealstate.findByStateCode(stateCode));
		originalNatisDeal.setBatchRef(batchRef)
		// TODO: Set/Update institution_institution on deal
		originalNatisDeal.save(flush: true, failOnError: true);

		// create the appropriate response.
		return [
			state: serviceResponse.getState().name(),
			message: serviceResponse.getMessage(),
			dealID: "$originalNatisDeal.id"
		];
	}

	private void emailDocument(String bankName, String emailAddress, String vinNumber, Deal copyNatisDeal){

		//log.info("Starting Email Thread.");

		new Thread(){
			public void run() {
				log.info("Sending CopyNatis for " + vinNumber + " email to: " + emailAddress);
				Map<String, String> nameAndTypeMap = [fileName: "${vinNumber}.pdf", mimeType: "application/pdf"]
				byte[] pdf = generateCopyNatis(copyNatisDeal, nameAndTypeMap);

				if (bankName == null) {bankName = ""}
				bankName = org.apache.commons.lang.WordUtils.capitalizeFully(bankName)

				//log.error("PDF Size: " + pdf.length);

				mailService.sendMail{
					multipart true
					from '\"' + bankName +' VAF\" ' + '<no-reply@lightstoneauto.co.za>'
					to emailAddress
					subject "Copy Natis Document VIN: " + vinNumber
					body "Good Day. please find the enclosed Copy Of Natis Document as requested on " + new Date();
					attachBytes nameAndTypeMap.get("fileName"), nameAndTypeMap.get("mimeType"), pdf
				}
				log.info("CopyNatis for " + vinNumber + " sent to: " + emailAddress);
			};
		}.start();
	}

	private byte[] generateCopyNatis(Deal deal, Map<String, String> nameAndTypeMap){
		// First check if document exists in repo
		Dealdocument copyOfNatisDocument = Dealdocument.findByDealAndDealstateAndDealdocumenttype(
				deal, Dealstate.findByStateCode(60300), Dealdocumenttype.findByType('COPYOFNATIS'));

		if (copyOfNatisDocument != null) {
			// Dealdocument exists, fetch deal from repo
			try{
				String fileName = copyOfNatisDocument.getFilename();
				MimeType type = MimeType.getMimeTypeByExtension(fileName.substring(fileName.lastIndexOf('.') + 1))
				nameAndTypeMap.put("fileName", fileName)
				nameAndTypeMap.put("mimeType", (type != null? type.getMimeType() : "application/pdf"));
				String path = "${System.getenv('SIGNIO_REPO')}/${copyOfNatisDocument.getUri()}/${copyOfNatisDocument.getFilename()}"
				File file = new File(path);
				if(file.exists()){
					return file.readBytes();
				}else{
					throw new Exception("Document not found!${path}");
				}
			}catch(Exception ex){
				log.error("Error retrieving file:", ex);
				// exception occurred, continue and generate PDF
			}
		}

		// and the sdm... build a map of the fields that are to be populated on the form.
		SignioDataModel sdm = (SignioDataModel) deal.getSignioDataModel("transformedXml");
		Map<String, String> documentFieldValues = sdm.fields.collectEntries{ key, value -> [key, value.value]};

		// check if the owner ID and name should be hidden.
		if(documentFieldValues.get("ownerIdNumber")?.contains("HIDDEN")) 	{ documentFieldValues.put("ownerIdNumber", "*************"); }
		if(documentFieldValues.get("ownerName")?.contains("HIDDEN")) 		{ documentFieldValues.put("ownerName", "*************"); }

		// get the copyNatis template.
		String document = Documenttemplate.findByFileName("COPY_NATIS")?.template
		if(document != null){
			// if we got a copy of the natis template, populate it, create the PDF and render it.
			ByteArrayOutputStream pdf = new ByteArrayOutputStream();
			PdfReader reader = new PdfReader(Base64.decodeBase64(document.getBytes()));
			PdfStamper stp = new PdfStamper(reader, pdf);
			stp.getWriter().setCompressionLevel(9);
			stp.setFullCompression();

			AcroFields acroFields = stp.getAcroFields();
			documentFieldValues.each({ fieldName, fieldValue ->
					acroFields.setField(fieldName, fieldValue);
				});

			reader.close();
			//stp.formFlattening = true;
			stp.close();

			return pdf.toByteArray();
		}
		else{
			log.error("Copy Natis Template was not found in database!");
		}
		return null;
	}

	private byte[] generateOriginalNatis(Deal deal){
		return null;
	}

	private String buildDocumentPath(Deal deal) {

		Institution bank = Institution.findById(deal.institutionId);
		Calendar rightNow = Calendar.getInstance()
		StringBuilder returnPath = new StringBuilder()

		String year = (String) rightNow.get(Calendar.YEAR)
		String month = new DateFormatSymbols().getMonths()[rightNow.get(Calendar.MONTH)];

		returnPath.append(bank.name.replace(" ", "_"))
		returnPath.append("/")
		returnPath.append(year)
		returnPath.append("/")
		returnPath.append(month)
		returnPath.append("/")
		returnPath.append(deal.institutionInstitution.merchantId)
		returnPath.append("/")
		returnPath.append(deal.dealgroupId)
		returnPath.append("/")
		returnPath.append(deal.id)
		return returnPath.toString();
	}

	private void saveFileToDocumentRepository(Deal deal, byte[] documentBytes, String uri, fileName) throws Exception {
		log.info("Saving PDF file")
		String path = "${System.getenv('SIGNIO_REPO')}/${uri}"
		log.info("Path to where document will be saved: ${path}");
		def folder = new File(path);
		if(!folder.exists()){
			folder.mkdirs();
		}
		FileOutputStream fos = new FileOutputStream("${path}/${fileName}");
		fos.write(documentBytes)
		fos.close()
	}

	public enum NatisBank {
		ABSA("ABSA BANK", "ABSA", 8321, false),
		MFC("MFC", "MFC", 8353, false),
		SBSA("Standard Bank", "SBSA", 8347, true),
		WESBANK("Wesbank", "WESBANK", 8352, true);

		private String bankName, bankCode;
		private long institutionID;
		private boolean openForOriginalNatis;

		public NatisBank(String bankName, String bankCode, long institutionID, boolean openForOriginalNatis) {
			this.bankName = bankName;
			this.bankCode = bankCode;
			this.institutionID = institutionID;
			this.openForOriginalNatis = openForOriginalNatis;
		}

		public String getBankName() {return this.bankName;}

		public String getBankCode() {return this.bankCode;}

		public long getInstitutionID() {return this.institutionID;}

		public boolean isOpenForOriginalNatis() {this.openForOriginalNatis;}

		public static List<NatisBank> getOriginalNatisBanks() {
			List<NatisBank> originalNatisBankList = new ArrayList<NatisBank>();
			for (NatisBank bank : NatisController.NatisBank.values()) {
				if (bank.isOpenForOriginalNatis()) {
					originalNatisBankList.add(bank);
				}
			}
			return originalNatisBankList;
		}

		public static NatisBank getBankByCode(String bankCode) {
			for (NatisBank bank : NatisController.NatisBank.values()) {
				if (bank.getBankCode().equals(bankCode)) {
					return bank;
				}
			}
			return null;
		}

	}

	public enum MimeType {
		PDF('pdf', 'application/pdf'),
		JPG('jpg', 'image/jpeg'),
		JPEG('jpeg', 'image/jpeg'),
		PNG('png', 'image/png'),
		GIF('gif', 'image/gif'),
		TIFF('tiff', 'image/tiff'),
		TIF('tif', 'image/tiff')

		private String extension, mimeType;
		private MimeType(String extension, String mimeType) {
			this.extension = extension;
			this.mimeType = mimeType;
		}

		public String getMimeType() {
			return this.mimeType;
		}

		public static MimeType getMimeTypeByExtension(String extension) {
			for (MimeType type : NatisController.MimeType.values()) {
				if (type.extension.equalsIgnoreCase(extension)) {
					return type;
				}
			}
			return null;
		}
	}
}
