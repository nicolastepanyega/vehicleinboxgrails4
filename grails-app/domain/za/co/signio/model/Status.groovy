package za.co.signio.model


class Status {

    String description

    static hasMany = [memberInterests:Memberinterest,
                      requests:Request,
                      retentions:Retention,
                      crmEventLogs:CrmEventlog,
                      crmSharedRelationships:CrmSharedrelationship,
                      crmEmailAuditLogs:CrmEmailauditlog,
                      relationship:Relationship,
                      member:Member,
                      institution:Institution,
                      otps:Otp
    ]

    static mapping = {
        version false
    }

    static constraints = {
        description maxSize: 45, nullable: false
    }

    String toString() {
        return description
    }
}
