package za.co.signio.model

class InstitutionTemplateConfiguration {

    //Institution bank
    Date templateStartDate
    Date templateEndDate
    Boolean active
    String clientType

    DealstateParent dealstateParent

    List templates

    static hasMany = [templates: Template, requiredFields: Formfield ,  readonlyFields: Formfield ,templateMappings:TemplateMapping,queues:Queue]
    static belongsTo = [institution:Institution]

    static constraints = {
        templateEndDate(nullable: true)
    }

    static mapping = {
        version false
        table('institution_template_configuration')
        templateMappings joinTable: 'institution_template_configuration_template_mapping'
        templates joinTable: 'institution_template_configuration_template'
        queues joinTable: 'institution_template_configuration_queue'
        requiredFields joinTable: 'institution_template_configuration_formfield_required'
        readonlyFields joinTable: 'institution_template_configuration_formfield_readonly'

        //readonlyFields joinTable:[column:"institution_template_configuration_formfield_readonly", key:"formfield_id"]
    }
}
