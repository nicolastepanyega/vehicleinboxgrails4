package za.co.signio.webservice

interface ThirdParty extends WebServiceProvider{

	public static enum DUMMY_THIRDPARTY implements WebServiceProvider, WebServiceAction{
		DO_ERROR,
		DO_FAIL,
		DO_SUCCESS
	}
	
	public static enum REGTRACK implements WebServiceProvider, WebServiceAction{
		LOG_TRANSACTION,
		ENUMERATE_SUPPORTING_DOCUMENTS,
		UPLOAD_DOCUMENT,
		TRANSFER_REQUEST_TO_BANK,
		FETCH_STATUS_UPDATES,
        ACKNOWLEDGE_STATUS_UPDATE,
		FETCH_ALV_FORM;
	}
	
	public static enum CERTRACK implements WebServiceProvider, WebServiceAction {
		WESBANK_REQUEST,
		WESBANK_STATUS,
		ABSA_REQUEST,
		ABSA_STATUS,
		MFC_REQUEST,
		MFC_STATUS,
        SBSA_REQUEST,
        SBSA_STATUS
	}
	
	public static enum IVID implements WebServiceProvider, WebServiceAction{
		REGISTRATION_INFO
	}
}
