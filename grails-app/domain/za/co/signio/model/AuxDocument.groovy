package za.co.signio.model

class AuxDocument {
    String name
    String content
    Date startDate
    Date updateDate
    Date endDate
    Boolean active
    String documentHash

    static hasOne = [auxDocumenttype: AuxDocumenttype]

    static belongsTo = [AuxDocumenttype, AuxProductoption]
    static hasMany = [auxProductOptions:AuxProductoption]

    static constraints = {
        name nullable: false, maxSize: 100
        content nullable: false
        startDate nullable: false
        updateDate nullable: true
        endDate nullable: true
        active nullable: false
        auxDocumenttype nullable: false
        documentHash nullable: true
    }

    static mapping = {
        version false
        table('aux_document')
        auxProductOptions joinTable: 'aux_document_aux_productoption'

    }

    public String toString(){
        name
    }

}
