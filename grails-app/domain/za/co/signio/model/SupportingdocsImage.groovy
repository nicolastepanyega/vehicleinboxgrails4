package za.co.signio.model

class SupportingdocsImage {

    Supportingdocs supportingdocs
    Image image
    static belongsTo = [Supportingdocs, Image]

    static constraints = {
        supportingdocs(nullable: false)
        image(nullable: false)
    }

    static mapping = {
        version false
    }
}
