package za.co.signio.model

/**
 * Created with IntelliJ IDEA.
 * User: Francois Maritz
 * Date: 2015/02/12
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
class AuxProductAuxDeliverymethod implements Serializable{

    Boolean isPrimaryMethod

    static hasOne = [auxProduct: AuxProduct, auxDeliverymethod: AuxDeliverymethod]
    static belongsTo = [AuxProduct, AuxDeliverymethod]

    static constraints = {
        isPrimaryMethod(nullable: false)
        auxProduct(nullable: false)
        auxDeliverymethod(nullable: false)
    }

    static mapping = {
        version false
        id composite: ['auxProduct', 'auxDeliverymethod']
    }

}
