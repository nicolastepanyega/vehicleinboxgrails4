package vehicleinbox

class Grailsauth {

    String  authenticationNumber
    Date    timeInserted
    String  user

    static mapping = {
        version false            // do not comment out will cause production problem
        authenticationNumber column: 'authenticationNumber'
        timeInserted column: 'timeInserted'
        user column: 'user'
    }
}
