<g:javascript library="jquery" plugin="jquery"/>
<r:require module="jquery-ui"/>
<html>
<head>
	<meta name="layout" content="minimal"/>
	<title>Vehicle Inbox - V5 - Request Natis</title>
	%{-- <r:layoutResources/> --}%
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxfoundation.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'inboxapp.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'forms.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'vehicleinbox.css')}">
    <script src="${resource(dir: 'js/businessRules', file: 'vehicleinbox.js')}"></script>
</head>
<body>
	<table width="98%"  class="responsive BGColourDarkBlue">
		<tr><td colspan="100%"  class="BGColourWhite" > <g:img dir="images" file="signio_logo.png"/></td></tr>
		<tr><td colspan="100%" class="BGColoursHeading" ><h4>Request Natis</h4></td></tr>

		<script type="text/javascript">
			signio.vehicleInbox.dealID=${params.id};
		</script>


	    <g:render template="/natis/natis_type" />
	    <g:render template="/natis/natis_form" />
    </table>
</body>
</html>
