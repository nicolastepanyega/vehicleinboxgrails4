package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: SuperBoerkie
 * Date: 2/25/13
 * Time: 1:14 PM
 * To change this template use File | Settings | File Templates.
 */
class Copynatisstatus {

    String status

    static hasMany = [vehicleDetails:Vehicledetail]

    static constraints = {
        status(nullable: false, maxSize: 45)
    }

    static mapping = {
        version false
    }
}
