package za.co.signio.model

/**
 * Created by IntelliJ IDEA.
 * User: Francois
 * Date: 2014/05/06
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
class Pilot {

    String identifier
    Date dateEnabled

    static hasOne = [pilotproject: Pilotproject]

    static constraints = {
        identifier nullable: false, maxSize: 50
        dateEnabled nullable: false
    }

    static mapping = {
        version false
    }
}
