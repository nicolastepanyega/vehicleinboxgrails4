package za.co.signio.vehicleinbox.helper

import org.apache.logging.log4j.Logger
import za.co.signio.model.Clientdetail
import za.co.signio.model.Deal
import za.co.signio.model.Member
import za.co.signio.model.Dealgroup
import za.co.signio.model.Vehicle
import za.co.signio.model.Contactinfo
import za.co.signio.model.Title
import za.co.signio.model.Institution
import za.co.signio.model.InstitutionInstitution
import za.co.signio.model.Dealgroupstate
import za.co.signio.model.Dealstate
import za.co.signio.model.Dealtype
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder
import za.co.signio.model.Institutiontype
import za.co.signio.model.Companydetail
import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * User: chris.ferreira@signio.co.za
 * Date: 2013/08/23
 * Time: 9:40 AM
 */
class RegTrackHelper {

    private static final String NEW_LINE = "\n" /*System.lineSeparator() not working on stage server*/
    private static final Logger LOGGER = Logger.getLogger(RegTrackHelper.class)
    private static final String DEAL_GROUP_STATE = "Active"
    private static final String VI_DEAL_STATE = "Vehicle Inbox"
    private static final String VI_DEAL_TYPE = "VehicleInbox"
    private static final String RT_DEAL_STATE = "RegTrack - Not Requested"
    private static final String RT_REQUESTED_DEAL_STATE = "RegTrack - Requested"
    private static final String RT_FAILED_DEAL_STATE = "RegTrack - Failed"
    private static final String RT_RECEIVED_DEAL_STATE = "RegTrack - Received"
    private static final String RT_DEAL_TYPE = "RegTrack"

    private Map params
    private StringBuilder rejectionMessage
    private String xml
    private boolean isReady
    private Map<String, String> regTrackFieldMap, contactDetailMap
    private Clientdetail clientDetail
    private Companydetail companyDetail
    private Deal regTrackDeal, viDeal
    private Member member
    private Dealgroup dealGroup
    private Vehicle vehicle
    private Institution institution, bankInstitution, ubiquitechInstitution
    private InstitutionInstitution institutionInstitution


    public RegTrackHelper(Map params){
        this.params = params
        this.rejectionMessage = new StringBuilder()
        this.regTrackFieldMap = new HashMap<String, String>()
        this.contactDetailMap = new HashMap<String, String>()
        this.isReady = true
        if (dealExists(params.batchRef, params.vinNumber)) {
            this.isReady = false
            rejectionMessage.append("RegTrack has already been requested for this contract.")
        } else {
            this.validateParameters()
        }
    }

    public Institution getBankInstitution() {
        return bankInstitution
    }

    public Institution getInstitution() {
        return institution
    }

    public Vehicle getVehicle() {
        return vehicle
    }

    public Dealgroup getDealGroup() {
        return dealGroup
    }

    public Member getMember() {
        return member
    }

    public Deal getViDeal() {
        return viDeal
    }

    public Deal getRegTrackDeal() {
        return regTrackDeal
    }

    public Clientdetail getClientDetail() {
        return clientDetail
    }

    public boolean isReadyForRegTrack() {
        return this.isReady
    }

    public String getRejectionMessage() {
        return this.rejectionMessage.toString()
    }

    public String getRegTrackXML() {
        return this.xml
    }

    public void setReferenceNoInDeals(String referenceNo) {
        // RegTrack Deal
        def signioPackage = new XmlSlurper().parseText(regTrackDeal.getOriginalXml())
        signioPackage.dataBundle.appendNode {
            field(referenceNo, formFieldName:"regTrackReferenceNo", mapping:"-1")
        }

        def outputBuilder = new StreamingMarkupBuilder()
        String result = outputBuilder.bind{mkp.yield signioPackage}
        regTrackDeal.setOriginalXml(result)
        regTrackDeal.save(flush: true, failOnError: true)
        //RegTrackHelper.insertDealData(regTrackDeal)

        // VehicleInbox Deal
        def signioPackage2 = new XmlSlurper().parseText(viDeal.getOriginalXml())
        signioPackage2.dataBundle.appendNode {
            field(referenceNo, formFieldName:"regTrackReferenceNo", mapping:"-1")
        }

        def outputBuilder2 = new StreamingMarkupBuilder()
        String result2 = outputBuilder2.bind{mkp.yield signioPackage2}
        viDeal.setOriginalXml(result2)
        viDeal.save(flush: true, failOnError: true)
        //RegTrackHelper.insertDealData(viDeal)
    }

    public void updateRegTrackDealState(String dealStateDesc, String message, boolean append) throws Exception {
        if (dealStateDesc != null) {
            Dealstate state = Dealstate.findByState(dealStateDesc)
            regTrackDeal.setDealstate(state)
        }

        if (message != null) {
            String comment = regTrackDeal.getComment()
            if (comment == null) {comment = ""}
            if (append) {
                comment = comment + "\n\n" + message
            } else {
                comment = message
            }
            regTrackDeal.setComment(comment)
        }
        regTrackDeal.save(failOnError: true,flush: true)
        //RegTrackHelper.insertDealData(regTrackDeal)
    }

    private void validateParameters() {
        // Check for member
        if (!isNull(params.memberId)) {
            member = Member.findByIdNumber(params.memberId)
            if (member == null) {
                rejectionMessage.append("Member not found on ID Number: ").append(params.memberId).append(NEW_LINE)
                this.isReady = false
            }
        } else {
            rejectionMessage.append("Parameter : memberIDNo is null").append(NEW_LINE)
            this.isReady = false
        }

        // Check for vehicle
        if (!isNull(params.mmCode)) {
            vehicle = Vehicle.findByMmCode(params.mmCode)
            if (vehicle == null) {
                rejectionMessage.append("Vehicle not found on MM Code: ").append(params.mmCode).append(NEW_LINE)
                this.isReady = false
            }
        } else {
            rejectionMessage.append("Parameter : mmCode is null").append(NEW_LINE)
            this.isReady = false
        }
        final Map<String, String> addressMap = new HashMap<String, String>();
        //Check if the vehicle(s) purchased by a company or client
        if(!isNull(params.clientOrJuristic)) {
            // Build address map. Passed by reference
            buildAddressMap(addressMap);
            if("Client".equalsIgnoreCase(params.clientOrJuristic)){
                // Check Client Detail
                if (!isNull(params.customerIdNumber) && !isNull(params.customerFirstName) && !isNull(params.customerSurname)) {

                    Title title = Title.findByDescription(params.customerTitle)
                    if (title == null) {
                        title = Title.findByDescription("Mr")
                    }
                    Date dateOfBirth = getDateOfBirth(params.customerDateOfBirth)
                    createClientDetail(title, params.customerFirstName, params.customerSurname, params.customerIdNumber, dateOfBirth,
                            params.customerEmail, addressMap)

                } else {
                    if (isNull(params.customerIdNumber)) {rejectionMessage.append("Parameter : idNo is null").append(NEW_LINE)}
                    if (isNull(params.customerFirstName)) {rejectionMessage.append("Parameter : firstName is null").append(NEW_LINE)}
                    if (isNull(params.customerSurname)) {rejectionMessage.append("Parameter : surname is null").append(NEW_LINE)}
                    this.isReady = false
                }
            }else if("Juristic".equalsIgnoreCase(params.clientOrJuristic)){
                // Check Company Detail
                if (!isNull(params.businessRegistrationNumber) && !isNull(params.companyName)) {

                    createCompanyDetail( params.companyName, params.businessRegistrationNumber,params.companyEmail, addressMap)

                } else {
                    if (isNull(params.businessRegistrationNumber)) {rejectionMessage.append("Parameter : idNo is null").append(NEW_LINE)}
                    if (isNull(params.companyName)) {rejectionMessage.append("Parameter : firstName is null").append(NEW_LINE)}
                    this.isReady = false
                }
            }
        }else{
            rejectionMessage.append("Parameter : clientOrJuristic is null").append(NEW_LINE)
            this.isReady = false
        }

        ubiquitechInstitution = Institution.findByNameAndInstitutiontype('Ubiquitech', Institutiontype.findByType('Service Provider'))
        //ubiquitechInstitution = Institution.findById(16786)
        // Check Institution
        if (!isNull(params.institutionID) && !isNull(params.dealerCode) && !isNull(params.thirdPartyID)) {
            bankInstitution = (Institution) Institution.get(params.institutionID)
            if (bankInstitution != null) {
                log.info("Logging see what is saved in institution: DEALERCODE: ${params.dealerCode} BANK: ${bankInstitution} 3RDParty: ${params.thirdPartyID}")
                InstitutionInstitution ii = InstitutionInstitution.findByMerchantIdAndInstitution1AndThirdPartyId(
                        params.dealerCode, bankInstitution, params.thirdPartyID)
                if (ii != null) {
                    institution = (Institution) Institution.get(ii.getInstitution2()?.getId())
                } else {
                    rejectionMessage.append("Failed to retrieve Institution on id: " + params.institutionID)
                            .append(", dealerCode: ").append(params.dealerCode)
                            .append(", thirdPartyID: ").append(params.thirdPartyID).append(NEW_LINE)
                    this.isReady = false
                }
            } else {
                rejectionMessage.append("Failed to retrieve Bank Institution on id: " + params.institutionID)
                this.isReady = false
            }
        } else {
            if (isNull(params.institutionID)) {rejectionMessage.append("Parameter : institutionID is null").append(NEW_LINE)}
            if (isNull(params.dealerCode)) {rejectionMessage.append("Parameter : dealerCode is null").append(NEW_LINE)}
            if (isNull(params.thirdPartyID)) {rejectionMessage.append("Parameter : thirdPartyID is null").append(NEW_LINE)}
            this.isReady = false
        }

        // Check Deal Group
        if (!isNull(params.batchRef) && this.isReady) {
            Dealgroupstate dealGroupState = Dealgroupstate.findByState(DEAL_GROUP_STATE)

            // VAF-9677
            if("Juristic".equalsIgnoreCase(params.clientOrJuristic)){
                dealGroup = Dealgroup.findByCompanydetailAndDealgroupstateAndInstitution(companyDetail, dealGroupState, institution)
            }else {
                dealGroup = Dealgroup.findByClientdetailAndDealgroupstateAndInstitution(clientDetail, dealGroupState, institution)
            }


            if (dealGroup == null) {
                createDealGroup(dealGroupState)
            }
        } else {
            if (this.isReady) {
                rejectionMessage.append("Parameter : batchRef is null").append(NEW_LINE)
                this.isReady = false
            } else {
                rejectionMessage.append("Deal Group can't be created because of previous errors").append(NEW_LINE)
            }
        }

        if (this.isReady) {
            institutionInstitution = InstitutionInstitution.findByInstitution1AndInstitution2(ubiquitechInstitution, institution)

            if (institutionInstitution == null) {
                this.isReady = false
                this.rejectionMessage.append("Institutions: ${ubiquitechInstitution.getName()} and ${institution.getName()} not linked in Institution_Institution table.")
            }
        }

        // Check Vehicle Deal
        if (this.isReady) {
            Dealstate viDealState = Dealstate.findByState(VI_DEAL_STATE)
            Dealtype viDealType = Dealtype.findByType(VI_DEAL_TYPE)
            if (viDealState != null && viDealType!= null) {
                createVIDeal(viDealState, viDealType, params.batchRef, addressMap)
            } else {
                rejectionMessage.append("VehicleInbox dealstate and/or dealtype not found.").append(NEW_LINE)
                this.isReady = false
            }
        } else {
            rejectionMessage.append("VehicleInbox Deal can't be created because of previous errors").append(NEW_LINE)
        }

        // Check RegTrack Deal
        if (this.isReady) {
            Dealstate regTrackDealState = Dealstate.findByState(RT_DEAL_STATE)
            Dealtype regTrackDealType = Dealtype.findByType(RT_DEAL_TYPE)
            log.info("regTrackDealState: ${regTrackDealState.id} ****regTrackDealType: ${regTrackDealType.id}")
            if (regTrackDealState != null && regTrackDealType != null) {
                createRegTrackDeal(regTrackDealState, regTrackDealType, (Integer)viDeal.getId(), params.batchRef, addressMap)
                log.info("After createRegTrackDeal()")
            }else {
                rejectionMessage.append("RegTrack dealstate and/or dealtype not found.").append(NEW_LINE)
                this.isReady = false
            }
        } else {
            rejectionMessage.append("RegTrack Deal can't be created because of previous errors").append(NEW_LINE)
        }

        // Check remaining parameters for RegTrack
        if (this.isReady) {
            if (isNull(params.customerBankAccountNumber) && isNull(params.companyBankAccountNumber)) {
                rejectionMessage.append("Parameter : accountNo is null").append(NEW_LINE)
                this.isReady = false
            }

            if (isNull(params.vinNumber)){
                rejectionMessage.append("Parameter : vinNo is null").append(NEW_LINE)
                this.isReady = false
            }

            if (isNull(params.newOrUsed)){
                rejectionMessage.append("Parameter : newUsed is null").append(NEW_LINE)
                this.isReady = false
            }
        }

        // If all check passed and all objects created build RegTrack XML.
        if (this.isReady) {
            buildParameterXML(addressMap)
        }
    }

    public static boolean dealExists(String batchRef, String vinNo) {
        if (isNull(batchRef) || isNull(vinNo)) {
            return false
        }
        Dealstate requestedDealState = Dealstate.findByState(RT_REQUESTED_DEAL_STATE)
        Deal deal = Deal.findByBatchRefAndDealstate(batchRef, requestedDealState)
        if (deal == null) {
            Dealstate receivedDealState = Dealstate.findByState(RT_RECEIVED_DEAL_STATE)
            deal = Deal.findByBatchRefAndDealstate(batchRef, receivedDealState)
            if (deal == null) {
                return false
            }
        }
        String xml = deal.getOriginalXml()
        if (!xml.contains(vinNo)) {
            return false
        }
        return true
    }

    private void createClientDetail(Title title, String firstName,
                                       String lastName, String idNo, Date dateOfBirth,
                                       String email, HashMap<String, String> addressMap) {
        try {
            Contactinfo contactInfo;
            clientDetail = Clientdetail.findByIdNumber(params.customerIdNumber)
            if (clientDetail == null) {
                clientDetail = new Clientdetail()
                contactInfo = new Contactinfo()
            } else {
                contactInfo = clientDetail.getContactinfo()
            }
            contactInfo.setEmail(email)
            contactInfo.setPhysicalAddress_1(addressMap.physicalAddressLine1 != null? addressMap.physicalAddressLine1 : "")
            contactInfo.setPhysicalAddress_2(addressMap.physicalAddressLine2 != null? addressMap.physicalAddressLine2 : "")
            contactInfo.setPhysicalAddress_3(addressMap.physicalAddressLine3 != null? addressMap.physicalAddressLine3 : "")
            contactInfo.setPhysicalAddressCode(addressMap.physicalAddressPostalCode != null? addressMap.physicalAddressPostalCode : "")
            contactInfo.setPostalAddress_1(addressMap.postalAddressLine1 != null? addressMap.postalAddressLine1 : "")
            contactInfo.setPostalAddress_2(addressMap.postalAddressLine2 != null? addressMap.postalAddressLine2 : "")
            contactInfo.setPostalAddress_3(addressMap.postalAddressLine3 != null? addressMap.postalAddressLine3 : "")
            contactInfo.setPostalAddressCode(addressMap.postalAddressPostalCode != null? addressMap.postalAddressPostalCode : "")
            contactInfo.save(flush: true, failOnError: true);
            //
            clientDetail.setIdNumber(idNo)
            clientDetail.setFirstName(firstName)
            clientDetail.setLastName(lastName)
            clientDetail.setDateOfBirth(dateOfBirth)
            clientDetail.setTitle(title)
            clientDetail.setContactinfo(contactInfo)
            clientDetail.save(flush: true, failOnError: true)
        } catch (ex) {
            LOGGER.error("Failed to save Client Detail", ex)
            rejectionMessage.append("Failed to save Client Detail. Error: ").append(ex.getMessage()).append(NEW_LINE)
            isReady = false
        }
     }

    private void createCompanyDetail(String companyName, String registrationNumber,
                                    String email, HashMap<String, String> addressMap) {
        try {
            Contactinfo contactInfo;
            companyDetail = Companydetail.findByRegistrationNumber(params.businessRegistrationNumber)
            if (companyDetail == null) {
                companyDetail = new Companydetail()
                contactInfo = new Contactinfo()
            } else {
                contactInfo = companyDetail.getContactinfo()
            }
            contactInfo.setPhysicalAddress_1(addressMap.physicalAddressLine1 != null? addressMap.physicalAddressLine1 : "")
            contactInfo.setPhysicalAddress_2(addressMap.physicalAddressLine2 != null? addressMap.physicalAddressLine2 : "")
            contactInfo.setPhysicalAddress_3(addressMap.physicalAddressLine3 != null? addressMap.physicalAddressLine3 : "")
            contactInfo.setPhysicalAddressCode(addressMap.physicalAddressPostalCode != null? addressMap.physicalAddressPostalCode : "")
            contactInfo.setPostalAddress_1(addressMap.postalAddressLine1 != null? addressMap.postalAddressLine1 : "")
            contactInfo.setPostalAddress_2(addressMap.postalAddressLine2 != null? addressMap.postalAddressLine2 : "")
            contactInfo.setPostalAddress_3(addressMap.postalAddressLine3 != null? addressMap.postalAddressLine3 : "")
            contactInfo.setPostalAddressCode(addressMap.postalAddressPostalCode != null? addressMap.postalAddressPostalCode : "")
            contactInfo.save(flush: true, failOnError: true);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            Date date = new Date()
            String strDate = dateFormat.format(date)
            Date creationDate = dateFormat.parse(strDate)

            companyDetail.setCompanyName(companyName)
            companyDetail.setRegistrationNumber(registrationNumber)
            companyDetail.setContactinfo(contactInfo)
            companyDetail.setDateCreated(creationDate)
            companyDetail.save(flush: true, failOnError: true)
        } catch (ex) {
            LOGGER.error("Failed to save Company Detail", ex)
            rejectionMessage.append("Failed to save Company Detail. Error: ").append(ex.getMessage()).append(NEW_LINE)
            isReady = false
        }
    }

    private void createDealGroup(Dealgroupstate dealGroupState) {
        try {
            dealGroup = new Dealgroup()

            // VAF-9677
            if(clientDetail == null){
                dealGroup.setCompanydetail(companyDetail)
            }else {
                dealGroup.setClientdetail(clientDetail)
            }

            dealGroup.setMember(member)
            dealGroup.setReferenceNumber(params.batchRef)
            dealGroup.setDealgroupstate(dealGroupState)
            dealGroup.setInstitution(institution)
            dealGroup.save(failOnError: true,flush: true)
        } catch (ex) {
            LOGGER.error("Failed to save Deal Group", ex)
            rejectionMessage.append("Failed to save Deal Group. Error: ").append(ex.getMessage()).append(NEW_LINE)
            isReady = false
        }
    }

    private void createVIDeal(Dealstate dealState, Dealtype dealType, String batchRef, HashMap<String, String> addressMap) {
        try {
            log.info("ubiquitechInstitution: ${ubiquitechInstitution.id}")
            viDeal = Deal.findByBatchRefAndDealstateAndInstitution(batchRef, dealState, ubiquitechInstitution);
            if (viDeal == null) {
                viDeal = new Deal()
                viDeal.setBatchRef(batchRef)
                viDeal.setMember(member)
                viDeal.setDealgroup(dealGroup)
                viDeal.setVehicle(vehicle)
                viDeal.setInstitution(ubiquitechInstitution)
                viDeal.setIsResendable(true)
                viDeal.setDealstate(dealState)
                viDeal.setDealtype(dealType);
                viDeal.setOriginalXml(this.createSDM(addressMap))
                viDeal.setInstitutionInstitution(institutionInstitution)
                if(!viDeal.save(flush: true,failOnError: true)) {
                    viDeal.errors.each {
                        rejectionMessage.append("Failed to save Deal. Error: ").append(it).append(NEW_LINE)
                    }
                    LOGGER.error("Failed to save VIDeal: " + rejectionMessage.toString())
                    isReady = false
                } //else {RegTrackHelper.insertDealData(viDeal)}
            }
        } catch (ex) {
            LOGGER.error("Failed to save Deal", ex)
            rejectionMessage.append("Failed to save VehicleInbox Deal. Error: ").append(ex.getMessage()).append(NEW_LINE)
            isReady = false
        }
    }

    private void createRegTrackDeal(Dealstate dealState, Dealtype dealType, int parentID, String batchRef, HashMap<String, String> addressMap) {
        try {
            log.info("Create Regtrack Deal: BANK ID: ${bankInstitution.id} OTHER INSTITUTION: ${institution.id} VALUE FOUND: ${InstitutionInstitution.findByInstitution1AndInstitution2(bankInstitution, institution).id}")
            Dealstate failedState = Dealstate.findByState(RT_FAILED_DEAL_STATE)
            regTrackDeal = Deal.findByBatchRefAndDealstateAndVehicleAndInstitution(batchRef, failedState, vehicle, bankInstitution)
            if (regTrackDeal == null) {
                regTrackDeal = new Deal()
                regTrackDeal.setBatchRef(batchRef)
                regTrackDeal.setVehicle(vehicle)
                regTrackDeal.setInstitution(bankInstitution)
                regTrackDeal.setInstitutionInstitution(institutionInstitution)
            }
            regTrackDeal.setIsResendable(true)
            regTrackDeal.setMember(member)
            regTrackDeal.setDealgroup(dealGroup)
            regTrackDeal.setIsResendable(true)
            regTrackDeal.setDealstate(dealState)
            regTrackDeal.setDealtype(dealType);
            regTrackDeal.setParentId(parentID)
            regTrackDeal.setOriginalXml(this.createSDM(addressMap))
            if(!regTrackDeal.save(flush: true,failOnError: true)) {
                regTrackDeal.errors.each {
                    rejectionMessage.append("Failed to save Deal. Error: ").append(it).append(NEW_LINE)
                }
                LOGGER.error("Failed to save Deal: " + rejectionMessage.toString())
                isReady = false
            }// else {RegTrackHelper.insertDealData(regTrackDeal)};
        } catch (ex) {
            LOGGER.error("Failed to save RegTrackDeal", ex)
            rejectionMessage.append("Failed to save RegTrack Deal. Error: ").append(ex.getMessage()).append(NEW_LINE)
            isReady = false
        }
    }

    private String createSDM(HashMap<String, String> addressMap) {

        StringWriter writer = new StringWriter()
        def signioDataModel = new MarkupBuilder(writer)

        signioDataModel.signioPackage {
            dataBundle{
                if ("Juristic".equals(params.clientOrJuristic)) {
                    field(params.companyName, formFieldName:"companyName", mapping:"442")
                    field(params.customerBankAccountNumber, formFieldName:"customerBankAccountNumber", mapping:"139")
                    field(params.businessRegistrationNumber, formFieldName:"businessRegistrationNumber", mapping:"793")
                    field(params.customerEmail, formFieldName:"customerEmail", mapping:"59")
                    field(params.companyPhoneNumber, formFieldName:"phoneNumber", mapping:"130")
                    field(params.natisBusinessRegisteredNumber, formFieldName:"natisBusinessRegisteredNumber", mapping:"0")
                    field(params.natisControlNumber, formFieldName:"natisControlNumber", mapping:"0")
                } else {
                    field(params.customerFirstName, formFieldName:"customerFirstName", mapping:"50")
                    field(params.customerSurname, formFieldName:"customerSurname", mapping:"52")
                    field(params.customerTitle, formFieldName:"customerTitle", mapping:"42")
                    field(params.customerBankAccountNumber, formFieldName:"customerBankAccountNumber", mapping:"139")
                    field(params.customerIdNumber, formFieldName:"customerIdNumber", mapping:"53")
                    field(params.customerEmail, formFieldName:"customerEmail", mapping:"59")
                    field(params.customerDateOfBirth, formFieldName:"customerDateOfBirth", mapping:"54")
                    field(params.customerPhoneNumber, formFieldName:"phoneNumber", mapping:"130")
                }
                // Physical Address
                field(addressMap.physicalAddressLine1 != null? addressMap.physicalAddressLine1 : "", formFieldName:"customerResidentialAddressLine1", mapping:"131")
                field(addressMap.physicalAddressLine2 != null? addressMap.physicalAddressLine2 : "", formFieldName:"customerResidentialAddressLine2", mapping:"132")
                field(addressMap.physicalAddressSuburb != null? addressMap.physicalAddressSuburb : "", formFieldName:"customerResidentialAddressSuburb", mapping:"133")
                field(addressMap.physicalAddressCity != null? addressMap.physicalAddressCity : "", formFieldName:"customerResidentialAddressCity", mapping:"815")
                field(addressMap.physicalAddressPostalCode != null? addressMap.physicalAddressPostalCode : "", formFieldName:"customerResidentialAddressPostalCode", mapping:"134")
                // Postal Address
                field(addressMap.postalAddressLine1 != null? addressMap.postalAddressLine1 : "", formFieldName:"customerPostalAddressLine1", mapping:"160")
                field(addressMap.postalAddressLine2 != null? addressMap.postalAddressLine2 : "", formFieldName:"customerPostalAddressLine2", mapping:"161")
                field(addressMap.postalAddressSuburb != null? addressMap.postalAddressSuburb : "", formFieldName:"customerPostalAddressSuburb", mapping:"163")
                field(addressMap.postalAddressCity != null? addressMap.postalAddressCity : "", formFieldName:"customerPostalAddressCity", mapping:"819")
                field(addressMap.postalAddressPostalCode != null? addressMap.postalAddressPostalCode : "", formFieldName:"customerPostalAddressPostalCode", mapping:"162")
                //
                field(params.batchRef, formFieldName:"batchRef", mapping:"159")
                field(params.dealerCode, formFieldName:"dealerCode", mapping:"149")
                field(params.memberId, formFieldName:"memberId", mapping:"1055")
                field(params.vinNumber, formFieldName:"vinNumber", mapping:"68")
                field(params.registrationNumber, formFieldName:"registrationNumber", mapping:"66")
                field(params.yearOfFirstRegistration, formFieldName:"yearOfFirstRegistration", mapping:"60")
                field(bankInstitution.getOriginatorId(), formFieldName:"originatorID", mapping:"-1")
                field(params.economicSector, formFieldName: "regtrackEconomicSector", mapping:"1546")
                field(params.vehicleUsage, formFieldName: "regtrackVehicleUsage", mapping:"1547")
                field(params.reasonForRegistration, formFieldName: "regtrackReasonForRegistration", mapping:"1549")
                field(params.natureOfOwnership, formFieldName: "regtrackNatureOfOwnership", mapping:"1548")
                field(params.usedOnPublicRoad, formFieldName: "regtrackUsedOnPublicRoad", mapping:"1545")
                field(params.bank, formFieldName: "bank", mapping:"-1")
                field(params.newOrUsed, formFieldName: "newOrUsed", mapping: "220")
            }
        }

        return  writer.toString()
    }

    private Date getDateOfBirth(String dob) {
        if (isNull(dob)) {
            String dateOfBirth = params.customerIdNumber
            dateOfBirth = dateOfBirth.substring(0,5)
            try {
                Date date = Date.parse("yyMMdd", dateOfBirth)
                return date
            } catch (ex){
                LOGGER.warn("Failed to parse date from id " + params.customerIdNumber, ex)
                return null
            }
        }
        return null
    }

    private static boolean isNull(Object o) {
        if (o instanceof String) {
            String s = (String) o
            if (s == null) {return true}
            if ("".equals(s.trim())) {return true}
            if ("null".equals(s.toLowerCase().trim())) {return true}
        } else {
            if (o == null) {return true}
        }
        return false
    }

    private void buildParameterXML(HashMap<String, String> addressMap) {

        // F&I Details
        String memberName = "${member.getFirstName()} ${member.getLastName()}"
        String memberEmail = member.getContactinfo()?.getEmail()
        if (isNull(memberEmail)) {memberEmail = institution.getContactinfo()?.getEmail()}
        String memberTel = member.getContactinfo()?.getTelOffice()
        if (isNull(memberTel)) {memberTel = institution.getContactinfo()?.getTelOffice()}
        if (isNull(memberTel)) {memberTel = member.getContactinfo()?.getCellphoneNumber()}

        contactDetailMap = [
                'contactEmail': truncateString(memberEmail),
                'contactName': truncateString(memberName),
                'contactTelephone': memberTel,
                'BATCH_REF':params.batchRef
        ]

        String accountNo;
        if (!RegTrackHelper.isNull(params.customerBankAccountNumber)) {
            accountNo =  params.customerBankAccountNumber
        } else {
            accountNo = params.batchRef
        }

        regTrackFieldMap = [
                'ACCOUNT_NO':accountNo,
                'DEALER_CODE':params.dealerCode,
                'VEHICLE_VIN':params.vinNumber,
                'OWNER_PHYSICAL_ADDRESS_1':truncateString(addressMap.physicalAddressLine1),
                'OWNER_PHYSICAL_ADDRESS_2':truncateString(addressMap.physicalAddressLine2),
                'OWNER_PHYSICAL_ADDRESS_3':truncateString(addressMap.physicalAddressLine3),
                'OWNER_PHYSICAL_ADDRESS_SUBURB':truncateString(addressMap.physicalAddressSuburb),
                'OWNER_PHYSICAL_ADDRESS_CITY':truncateString(addressMap.physicalAddressCity),
                'OWNER_PHYSICAL_ADDRESS_POSTCODE':truncateString(addressMap.physicalAddressPostalCode),
                'OWNER_POSTAL_ADDRESS_1':truncateString(addressMap.postalAddressLine1),
                'OWNER_POSTAL_ADDRESS_2':truncateString(addressMap.postalAddressLine2),
                'OWNER_POSTAL_ADDRESS_3':truncateString(addressMap.postalAddressLine3),
                'OWNER_POSTAL_ADDRESS_SUBURB':truncateString(addressMap.postalAddressSuburb),
                'OWNER_POSTAL_ADDRESS_CITY':truncateString(addressMap.postalAddressCity),
                'OWNER_POSTAL_ADDRESS_POSTCODE':truncateString(addressMap.postalAddressPostalCode),
                'VEHICLE_USAGE':truncateString(params.vehicleUsage),
                'ECONOMIC_SECTOR':truncateString(params.economicSector),
                'NATURE_OF_OWNERSHIP':truncateString(params.natureOfOwnership),
                'USED_ON_PUBLIC_ROAD':truncateString(params.usedOnPublicRoad),
                'REASON_FOR_REGISTRATION':truncateString(params.reasonForRegistration)
        ]

        // Add Juristic or Client Fields
        if ("Juristic".equals(params.clientOrJuristic)) {
            regTrackFieldMap.put('OWNER_NAME', truncateString(params.companyName));
            regTrackFieldMap.put('OWNER_ID_NO', truncateString(params.natisBusinessRegistrationNumber));
            regTrackFieldMap.put('OWNER_ID_TYPE', '04');
            regTrackFieldMap.put('OWNER_EMAIL', truncateString(params.companyEmail));
            regTrackFieldMap.put('OWNER_TELEPHONE', truncateString(params.companyPhoneNumber));
            regTrackFieldMap.put('NATIS_CONTROL_NO', truncateString(params.natisControlNumber));
        } else {
            regTrackFieldMap.put('OWNER_NAME', truncateString(params.customerFirstName + " " + params.customerSurname));
            regTrackFieldMap.put('OWNER_ID_NO', truncateString(params.customerIdNumber));
            if ("Foreigner".equals(params.clientOrJuristic)) {
                regTrackFieldMap.put('OWNER_ID_TYPE', '01');
            } else {
                regTrackFieldMap.put('OWNER_ID_TYPE', '02');
            }
            regTrackFieldMap.put('OWNER_DATE_OF_BIRTH', truncateString(params.customerDateOfBirth));
            regTrackFieldMap.put('OWNER_EMAIL', truncateString(params.customerEmail));
            regTrackFieldMap.put('OWNER_TELEPHONE', truncateString(params.customerPhoneNumber));
        }

        this.xml = parseMapsToXML("logTransaction")
        regTrackDeal.setTransformedXml(this.xml)
        regTrackDeal.save(failOnError: true, flush: true)
    }

    private String parseMapsToXML(String requestAction)
            throws Exception{

        StringBuilder sb = new StringBuilder();
        sb.append("<").append(requestAction).append(">").append("\n")
        for (String key : contactDetailMap.keySet()) {
            sb.append("<").append(key).append(">")
            sb.append(contactDetailMap.get(key).replaceAll("&", "&amp;"))
            sb.append("</").append(key).append(">\n")
        }
        sb.append("<fieldList>")
        for (String key : regTrackFieldMap.keySet()) {
            sb.append("<").append(key).append(">")
            sb.append(regTrackFieldMap.get(key).replaceAll("&", "&amp;"))
            sb.append("</").append(key).append(">\n")
        }
        sb.append("</fieldList>")
        sb.append("</").append(requestAction).append(">")

        return sb.toString()
    }

    private void buildAddressMap(HashMap<String, String> addressMap) {

        String physicalAddressString = ("Juristic".equals(params.clientOrJuristic))? params.companyResidentialAddress : params.customerResidentialAddress;
        // Add Residential fields to map, can be between 3 to 5 fields. Postal code can be in either 3, 4 or 5.
        if (!isNull(physicalAddressString)) {
            String[] physicalAddress = (physicalAddressString).split(",");
            for(int i = 0; i < physicalAddress.length; i++) {
                switch (i+1) {
                    case 1:
                        addressMap.put("physicalAddressLine1", physicalAddress[i]?.trim());
                        break;
                    case 2:
                        addressMap.put("physicalAddressLine2", physicalAddress[i]?.trim());
                        if (physicalAddress.length == 4) {
                            addressMap.put("physicalAddressSuburb", physicalAddress[i]?.trim());
                        }
                        break;
                    case 3:
                        addressMap.put("physicalAddressLine3", physicalAddress[i]?.trim());
                        if (physicalAddress[i].isNumber()) {
                            addressMap.put("physicalAddressPostalCode", physicalAddress[i]?.trim());
                        } else if (physicalAddress.length == 4) {
                            addressMap.put("physicalAddressCity", physicalAddress[i]?.trim());
                        } else if (physicalAddress.length == 5) {
                            addressMap.put("physicalAddressSuburb", physicalAddress[i]?.trim());
                        }
                        break;
                    case 4:
                        if (physicalAddress[i].isNumber()) {
                            addressMap.put("physicalAddressPostalCode", physicalAddress[i]?.trim());
                        } else {
                            addressMap.put("physicalAddressCity", physicalAddress[i]?.trim());
                        }
                        break;
                    case 5:
                        if (physicalAddress[i].isNumber()) {
                            addressMap.put("physicalAddressPostalCode", physicalAddress[i]?.trim());
                        }
                        break;
                    default:
                        if (physicalAddress[i].isNumber()) {
                            addressMap.put("physicalAddressPostalCode", physicalAddress[i]?.trim());
                        }
                        break;
                }
            }
        }

        String postalAddressString = ("Juristic".equals(params.clientOrJuristic))? params.companyPostalAddress : params.customerPostalAddress;
        // Add Postal fields to map, can be between 3 to 5 fields. Postal code can be in either 3, 4 or 5;
        if (!isNull(postalAddressString)) {
            String[] postalAddress = postalAddressString.split(",");
            for(int i = 0; i < postalAddress.length; i++) {
                switch (i+1) {
                    case 1:
                        addressMap.put("postalAddressLine1", postalAddress[i]?.trim());
                        break;
                    case 2:
                        addressMap.put("postalAddressLine2", postalAddress[i]?.trim());
                        if (postalAddress.length == 4) {
                            addressMap.put("postalAddressSuburb", postalAddress[i]?.trim());
                        }
                        break;
                    case 3:
                        addressMap.put("postalAddressLine3", postalAddress[i]?.trim());
                        if (postalAddress[i].isNumber()) {
                            addressMap.put("postalAddressPostalCode", postalAddress[i]?.trim());
                        } else if (postalAddress.length == 4) {
                            addressMap.put("postalAddressCity", postalAddress[i]?.trim());
                        } else if (postalAddress.length == 5) {
                            addressMap.put("postalAddressSuburb", postalAddress[i]?.trim());
                        }
                        break;
                    case 4:
                        if (postalAddress[i].isNumber()) {
                            addressMap.put("postalAddressPostalCode", postalAddress[i]?.trim());
                        } else {
                            addressMap.put("postalAddressCity", postalAddress[i]?.trim());
                        }
                        break;
                    case 5:
                        if (postalAddress[i].isNumber()) {
                            addressMap.put("postalAddressPostalCode", postalAddress[i]?.trim());
                        }
                        break;
                    default:
                        if (postalAddress[i].isNumber()) {
                            addressMap.put("postalAddressPostalCode", postalAddress[i]?.trim());
                        }
                        break;
                }
            }
        }
    }

    private String truncateString(String s) {
        if (isNull(s)) {
            return "";
        }

        if (s.length() > 40) {
            return s.substring(0, 40);
        }

        return s;
    }

    enum Bank {
        WESBANK("Wesbank", "WESB", true),
        MFC("MFC", "7001", true),
        SA_TAXI("SA Taxi Finance", "SATF", true),
        STANDARD_BANK("STANDARD BANK", "SBVAF", true),
        ABSA("ABSA BANK", "ABSAREG", false);

        private String bankName
        private String bankCode
        private boolean enabled

        private Bank(String bankName, String bankCode, boolean enabled){
            this.bankName = bankName
            this.bankCode = bankCode
            this.enabled = enabled
        }

        public String getBankName() {
            return this.bankName
        }

        public String getBankCode() {
            return this.bankCode
        }

        public boolean isEnabled() {
            return this.enabled
        }

        public static Bank getBankByName(String name) {
            for (RegTrackHelper.Bank bank : RegTrackHelper.Bank.values()) {
                if (bank.getBankName().equalsIgnoreCase(name)) {
                    return bank
                }
            }
            return null
        }

        public static Bank getBankByCode(String bankCode) {
            for (Bank bank : RegTrackHelper.Bank.values()) {
                if (bank.getBankCode().equals(bankCode)) {
                    return bank
                }
            }
            return null
        }

        public List<Bank> getEnabledBanks() {
            List<Bank> bankList = new ArrayList<Bank>()
            for (Bank bank : Bank.values()) {
                if (bank.isEnabled()) {
                    bankList.add(bank)
                }
            }
            return bankList
        }
    }

    enum RedirectionFlow {
        ERROR, INDEX, INBOX
    }
}